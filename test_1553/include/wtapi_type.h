#ifndef __WTAPI_TYPE_H
#define __WTAPI_TYPE_H

#ifdef __cplusplus 
extern "C" {
#endif

typedef  int  (_stdcall *WT_UserHandle)(unsigned int device); 

#define  WT_SUCCESS									0
#define  WT_BAD_DEVICE_ID							183
#define  WT_UNSUCCESS								-1
#define  DL_OVERLOAD								-2

#define  ENABLE										1
#define  DISABLE									0

#define  nMaxDevice									8
#define  nMaxHandle									64

#define START_BOARD_TYPE
#define WT_PCI642_NO_BOARD							0x00
#define WT_PCI9054_NO_BOARD							0x00

#define WT_PCI642_ARINC429_16TR						0x01
#define WT_CPCI642_1553B							0x02
#define WT_PCI642_DATABUS							0x03
#define WT_PCI642_MULTI_IO_S04						0x04
#define WT_PCI642_1553B								0x05
#define WT_PCI642_485_S01							0x06
#define WT_PCI642_SCDR								0x07
#define WT_PCI642_ARINC453							0x08
#define WT_PCI642_LVDS								0x09
#define WT_PCI642_ARINC429_RS422					0x0A
#define WT_PCI642_ARINC708							0x0B
#define WT_PMC642_HDLC								0x0C
#define WT_PCI642_ARINC429_M						0x0D
#define WT_PXI642_1553B								0x0E
#define WT_PCI642_RS422_16TR						0x0F
#define WT_PCI642_RS422_32R							0x10
#define WT_PCI642_G950_IO							0x11
#define WT_PCI642_G950_PRES							0x12
#define WT_PCI642_G950_DA							0x13
#define WT_PCI642_ARINC429_32R						0x14
#define WT_PXI642_ARINC429_16TR						0x15




#define WT_CPCI642_Multi_Func						0x80
#define WT_CPCI642_1553B_CAN						0x81
#define WT_CPCI642_DA180							0x82
#define WT_CPCI642_RS422_96R_44T					0x83
#define WT_CPCI642_Pulse_width_Measurement			0x84
#define WT_CPCI9054_batterypower                    0x85
#define WT_CPCI9054_elec_curr						0x86
#define WT_CPCI642_LVDS_96R_44T						0x87
#define WT_CPCI642_AD96								0x88

#define END_BOARD_TYPE


UINT Wtapi_Get_Board_ID(UINT device);


#ifdef __cplusplus 
}
#endif

#endif
