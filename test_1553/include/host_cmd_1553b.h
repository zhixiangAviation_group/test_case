#ifndef __HOST_CMD_1553B_H
#define __HOST_CMD_1553B_H


#ifdef WT_DSP_1553B


	#define BT1553_BUFCOUNT  33  /* Length of one Data Buffer, in WORDS.  */
	#define PCI1553_BUFCOUNT 40  /* The PCI-1553 rounds this number up    */
    	                         /*  to 40 words; it requires an address  */
        	                     /*  which is a multiple of 8 words.      */


	/****************************************************************************
	*  Remote Terminal Definitions
	****************************************************************************/

	#define RT_ADDRESS_COUNT      32
	#define RT_SUBADDR_COUNT      32
	#define RT_TRANREC_COUNT      2
	#define RT_SUBUNIT_COUNT      2048
	#define RT_ABUF_ITF           0x0004   // Set this bit to inhibit terminal flag
	#define RT_ABUF_DBC_ENA       0x4000   // Set this bit to enable Dynamic Bus Ccceptance
	#define RT_ABUF_DBC_RT_OFF    0x8000   // Set this bit to shut down RT on valid DBA

	/* Define the message type */
	#define BC_CONTROL_NOP        0x0000   // no-op command (jump .+1)
	#define BC_CONTROL_MESSAGE    0x0001   // data message base value
	#define BC_CONTROL_BRANCH     0x0002   // branch message type (jump to specified)
	#define BC_CONTROL_CONDITION  0x0003   // conditional branch on previous msg
	#define BC_CONTROL_CONDITION2 0x0004   // conditional branch on specific addr
	#define BC_CONTROL_CONDITION3 0x0006   // cond branch on specified msg and word
	#define BC_CONTROL_LAST       0x0005   // last logical message in list; stops the BC
	#define BC_CONTROL_TYPEMASK   0x0007   // mask for message type

	/* These bits are defined for messages */
	#define BC_CONTROL_MFRAME_BEG 0x0008   // beginning of a minor frame
	#define BC_CONTROL_MFRAME_END 0x0010   // end of a minor frame
	#define BC_CONTROL_RTRTFORMAT 0x0020   // rt to rt format 1553 message
	#define BC_CONTROL_RETRY      0x0040   // retry enable: Conditions for retrying
    	                                   //  are setup in the BusTools_BC_Init()
        	                               //  function call defined below.
	#define BC_CONTROL_INTERRUPT  0x0080   // interrupt enable on this message
	#define BC_CONTROL_INTQ_ONLY  0x1000   // do not generate H/W int but add event to queue

	#define BC_CONTROL_BUFFERA    0x0100   // buffer a is active
	#define BC_CONTROL_BUFFERB    0x0200   // buffer b is active
	#define BC_CONTROL_CHANNELA   0x0400   // output message on channel a
	#define BC_CONTROL_CHANNELB   0x0800   // output message on channel b
	#define BC_CONTROL_BUSA       0x0400   // output message on channel a
	#define BC_CONTROL_BUSB       0x0800   // output message on channel b

	/************************************************
	*  External BC Triggering options for the
	*   BusTools_BC_Trigger() function.
	************************************************/
	#define BC_TRIGGER_IMMEDIATE  0  /* BC starts running immediately      */
	#define BC_TRIGGER_ONESHOT    1  /* BC is triggered by external source */
    	                             /*   and free runs after trigger.     */
	#define BC_TRIGGER_REPETITIVE -1 /* Each minor frame is started by the */
    	                             /*   external trigger.                */
	/************************************************
	* BC Retry options
	************************************************/
	#define RETRY_ALTERNATE_BUS  2
	#define RETRY_SAME_BUS       1
	#define RETRY_END            0

	/********************************************************
	*  Retry Enable bit definitions for BusTools_BC_Init()
	*   wRetry parameter.  These bits enable the conditons
	*   for BC_CONTROL_RETRY retries in the API_BC_MBUF.
	********************************************************/
	#define BC_RETRY_ALTB   0x0001   /* Register 78 Bit  9, Alternate Bus  */
	#define BC_RETRY_NRSP   0x0002   /* Register 78 Bit 10, No Response    */
	#define BC_RETRY_ME     0x0004   /* Register 45 Bit 10, Message Error  */
	#define BC_RETRY_BUSY   0x0008   /* Register 45 Bit  3, Busy Bit Set   */
	#define BC_RETRY_TF     0x0010   /* Register 45 Bit  0, Terminal Flag  */
	#define BC_RETRY_SSF    0x0020   /* Register 45 Bit  2, SubSystem Flag */
	#define BC_RETRY_INSTR  0x0040   /* Register 45 Bit  9, Instrumentation*/
	#define BC_RETRY_SRQ    0x0080   /* Register 45 Bit  8, Service Request*/




	/*****************************************************************************
	*  Event filter structure event specification values.  When specified event
	*   detected the API will place it in the FIFO and call the user function.
	*****************************************************************************/
	#define EVENT_TIMER_WRAP  0x0002  /* Tag Timer overflow or discrete input   */
	#define EVENT_RT_MESSAGE  0x0004  /* RT message transacted                  */
	#define EVENT_BM_MESSAGE  0x0008  /* BM message transacted                  */

	#define EVENT_BC_MESSAGE  0x0010  /* BC message transacted                  */
	#define EVENT_BM_TRIG     0x0020  /* BM trigger event (start/stop)          */
	#define EVENT_BM_START    0x0040  /* BM started (BusTools_BM_StartStop)V4.01*/
	#define EVENT_BM_STOP     0x0080  /* BM stopped (BusTools_BM_StartStop)     */

	#define EVENT_BC_START    0x0100  /* BC started (BusTools_BC_StartStop)     */
	#define EVENT_BC_STOP     0x0200  /* BC stopped (BusTools_BC_StartStop)     */
	#define EVENT_RT_START    0x0400  /* BC started (BusTools_RT_StartStop)     */
	#define EVENT_RT_STOP     0x0800  /* BC stopped (BusTools_RT_StartStop)     */

	#define EVENT_RECORDER    0x1000  /* BM recorder buffer has 64K or timeout  */
	#define EVENT_MF_OVERFLO  0x2000  /* Minor frame timing overflow            */
	#define EVENT_API_OVERFLO 0x4000  /* BM API Recorder buffer overflowed      */
	#define EVENT_HW_OVERFLO  0x8000  /* BM HW Recorder buffer overflowed       */



	#define BT1553_INT_HIGH_WORD       0x00000001L // high word error
	#define BT1553_INT_BIT_COUNT_DATA  0x00000001  // bit cnt err, data wrd **
	#define BT1553_INT_INVALID_WORD    0x00000002L // invalid word error    **
	#define BT1553_INT_LOW_WORD        0x00000004L // low word error
	#define BT1553_INT_INVERTED_SYNC   0x00000008L // inverted sync         **
	#define BT1553_INT_MID_BIT         0x00000010L // mid bit error         **
	#define BT1553_INT_TWO_BUS         0x00000020L // Two bus error V4.25
	#define BT1553_INT_PARITY          0x00000040L // parity error          **
	#define BT1553_INT_NON_CONT_DATA   0x00000080L // non-contiguous data   **
	#define BT1553_INT_EARLY_RESP      0x00000100L // early response
	#define BT1553_INT_LATE_RESP       0x00000200L // late response
	#define BT1553_INT_BAD_RTADDR      0x00000400L // incorrect rt address
	#define BT1553_INT_CHANNEL         0x00000800L // channel (0=A, 1=B)
	#define BT1553_INT_WRONG_BUS       0x00002000L // response on wrong bus
	#define BT1553_INT_BIT_COUNT       0x00004000L // bit count error       **
	#define BT1553_INT_NO_IMSG_GAP     0x00008000L // no intermessage gap
	//      Note: In the above list, "**" means the bit can be set for a
	//             data word (vs. a command or status word, or a message),
	//            BT1553_INT_HIGH_WORD_DATA is used for BM data words, and
	//            is a copy of BT1553_INT_BIT_COUNT.
	#define BT1553_INT_END_OF_MESS     0x00010000L // end of message
	#define BT1553_INT_BROADCAST       0x00020000L // broadcast message
	#define BT1553_INT_RT_RT_FORMAT    0x00040000L // rt-to-rt message format
	#define BT1553_INT_RESET_RT        0x00080000L // reset rt
	#define BT1553_INT_SELF_TEST       0x00100000L // self test
	#define BT1553_INT_MODE_CODE       0x00200000L // message is a Mode Code (wcs > 3.07, was BIT FAIL)
	#define BT1553_INT_NOCMD           0x00400000L // No command seen by decoder
	#define BT1553_INT_RT_RTB0         0x00800000L // RT microcode updated buffer (set when entered, clear when complete)
	#define BT1553_INT_RT_RTB1         0x01000000L // RT microcode updated buffer (set when entered, set when complete)
	#define BT1553_INT_RETRY           0x02000000L // retry                 NI for BM
	#define BT1553_INT_NO_RESP         0x04000000L // no response (for RT-RT, set if EITHER was no response)
	#define BT1553_INT_ME_BIT          0x08000000L // 1553 status wd message error bit
	#define BT1553_INT_ALT_BUS         0x80000000L // retry on alternate bus.

	// *****  SOFTWARE ONLY BITS ***** (not set by the hardware)

	#define BT1553_INT_TRIG_BEGIN      0x10000000L // message with trigger begin
	#define BT1553_INT_TRIG_END        0x20000000L // message with trigger end
	#define BT1553_INT_BM_OVERFLOW     0x40000000L // message at buffer overflow

#endif


//消息类型定义：
#define Non_Massage								0x0000
#define Non_Broadcast_BC_To_RT					0x0001
#define Non_Broadcast_RT_To_BC					0x0002
#define Non_Broadcast_RT_To_RT					0x0003
#define Non_Broadcast_Mode_With_Data_Recv		0x0004
#define Non_Broadcast_Mode_With_Data_Send		0x0005
#define Non_Broadcast_Mode_Non_Data				0x0006
#define Broadcast_BC_To_RT						0x0007
#define Broadcast_RT_To_RT						0x0008
#define Broadcast_Mode_With_Data_Recv			0x0009
#define Broadcast_Mode_Non_Data					0x000a
#define Massage_Error							0x8000

#ifdef WT_DSP_1553B
	#define __int64 long  long
#endif
//#pragma pack(push)
#pragma pack(8)
struct  WT1553_Msg
{
    unsigned short int cmd1; 
    unsigned short int cmd2;
    unsigned short int status1;
    unsigned short int status2;
    unsigned short int msgtype; 
    unsigned short int msgflag;		//		"00000" & transout & respout1 & respout2 & bus_error & cmd1_ready & cmd2_ready & status1_ready & status2_ready & data_ready & busb_flag & busa_flag;

	unsigned short int data[32]; /*winie org = 32*/
    
    unsigned  	   int msgno;
	long long   		   tag;    		//		1/24 us unit;
	unsigned short respcnt1; 		//		1/24 us unit;
	unsigned short respcnt2; 		//		1/24
};
#pragma pack()
struct BM_Message
{
	struct WT1553_Msg msg;
	float resp_time1; //1us unit;
	float resp_time2; 
};


//#define BC_Control_MSG611_NORMAL			0x0000		//简单1553消息	
//#define BC_Control_MSG611_UPDATE			0x0001		//如果消息是ISBC MESSAGE，对于BC->RT消息，先检查该位更新标志，如果有数据更新，则发送

//#define BC_Control_MSG611_PRD1				0x0010		//周期消息
//#define BC_Control_MSG611_UPE				0x0020		//UPE消息
//#define BC_Control_MSG611_SUPE				0x0040		//SUPE消息
//#define BC_Control_MSG611_PRD2				0x0080		//周期消息

//#define BC_Control_MSG611_MANT				0x0100		//管理消息

//#define BC_Control_MSG611_ISBC				0x8000		//表示这是ISBC消息

struct WT_API_BC_MBUF
{
	unsigned short int messno;
	unsigned short int control;
	unsigned short int messno_next;
	unsigned short int messno_prev;

	unsigned short int mess_cmd1;
	unsigned short int mess_cmd2;

	unsigned short int errorid;
	unsigned short int gap_time;

	unsigned short int mess_status1;
	unsigned short int mess_status2;
	
	unsigned int status;
	
	unsigned short int data[2][32];

	unsigned short int data_value;
	unsigned short int data_mask;
	unsigned short int address;
	unsigned short int messno_branch;
	unsigned short int messno_compare;

	unsigned short int	spare;
	unsigned int		test_address;

	unsigned short int cond_count_val;
	unsigned short int cond_count;

	unsigned short int data_buf_num1;
	unsigned short int data_buf_num2;

	__int64  tag;//		1/24 us unit;

	//unsigned short control_611;

	unsigned int   msg_type;

	//unsigned int   sysMode;
	
	//unsigned char  CycleActiveFlag[96];

	//auto increments
	unsigned short	auto_inc_flag;
	unsigned short	auto_inc_req;	
	unsigned short	start_value;
	unsigned short	inc_value;
	unsigned short	max_value;
	unsigned short	word_idx;
	unsigned int	inc_rate;		//每多少次，数据内容增加一次
	unsigned int	inc_idx;		
};

struct WT_IRQ_Fifo_Info
{
      unsigned int  event_type;  // EVENT_ definitions below.
      unsigned int  buffer_off;  // Byte offset of message buffer which caused event
      unsigned int  rtaddress;   // Terminal address of message.  If > 31, error code.
      unsigned int  transrec;    // Non-zero if transmit message, zero for receive
      unsigned int  subaddress;  // Subaddress of message
      unsigned int  wordcount;   // Word count of message; 0-31; 0 indicates 32 words
                           		 //  unless mode code (then indicates mode code number)
      unsigned int  bufferID;    // Buffer ID number or message ID number.
      unsigned int  reserved;    // Reserved for API.
};

struct WT_API_RT_ABUF
{
	unsigned short int enable_a;             // enable channel a; 1=enable
	unsigned short int enable_b;             // enable channel b; 1=enable
	unsigned short int inhibit_term_flag;    // inhibit terminal flag plus misc (see below)
	unsigned short int status;               // latest RT status word
	unsigned short int command;              // latest command word.V4.27
	unsigned short int bit_word;             // latest built in test (BIT) word
};

struct WT_API_RT_CBUF
{
	unsigned int legal_wordcount;      // legal word count bit mask
};

struct WT_API_RT_CBUFBROAD
{
	unsigned int legal_wordcount[32]; 	// legal word count bits for each RT address,
      		                            //   for RT addresses 0 - 31.
	unsigned int mbuf_count;          	// Number of message buffers to allocate,
};                              		//  or zero to use the default.V4.39

struct WT_API_RT_MBUF
{
    unsigned short int  mess_command;    // 1553 command word

    unsigned short int  mess_status;     // 1553 status word

    unsigned int  time_L;            	// 64-bit time
    unsigned int  time_H;            	// 64-bit time

	unsigned int  status;          		// interrupt status

    unsigned int  spare;           		// spare data word for word count errors

    unsigned short int  mess_data[32];  // data words

   	unsigned int  enable;          		// interrupt enable bits
   	unsigned int  error_inj_id;    		// id of error injection buffer
};

struct WT_API_BM_CBUF
{
	unsigned int wcount;           		// enabled word counts (bit field)
	unsigned int modecode;         		// enabled mode codes
   	unsigned int pass_count;          	// number of passes before interrupt
};

struct WT_OUT_Msg
{
	unsigned short int  	BusB;
	unsigned short int  	tx_type;
	unsigned short int  	cmd1_status;
	unsigned short int  	cmd2;
	unsigned short int  	datacnt;
	unsigned short int  	error_inj_id;
	unsigned short int  	data[32];
};

#define nMax_CH1553				2

#define IrqNum					1024
#define IrqMask					(IrqNum-1)

#define BmNum					1024
#define	BmMask					(BmNum-1)

#define BCMsgNum				8192

#pragma pack(8)

struct WT_1553B_Data
{
	unsigned int 				RESET;

	unsigned int 				RT_Enable;
	unsigned int 				RT_Start;
	unsigned int 				RT_Stop;

	unsigned int  				RT_Mbuf_WR_Addr;
	unsigned int  				RT_Mbuf_WR_SubAddr;
	unsigned int  				RT_Mbuf_WR_TR;
	unsigned int  				RT_Mbuf_WR_ID;
	unsigned int  				RT_Mbuf_WR;
	struct WT_API_RT_MBUF		RT_Mbuf_WR_Param;
	unsigned int  				RT_Mbuf_RD_Addr;
	unsigned int  				RT_Mbuf_RD_SubAddr;
	unsigned int  				RT_Mbuf_RD_TR;
	unsigned int  				RT_Mbuf_RD_ID;
	unsigned int  				RT_Mbuf_RD;
	struct WT_API_RT_MBUF		RT_Mbuf_RD_Param;

	//unsigned short int  		RT_Vector_Value[RT_ADDRESS_COUNT];
	//unsigned short int  		RT_BITWord[RT_ADDRESS_COUNT];

	unsigned int  				RT_Abuf_WR_Addr;
	unsigned int  				RT_Abuf_WR;
	struct WT_API_RT_ABUF		RT_Abuf[RT_ADDRESS_COUNT];
	
	unsigned int  				RT_Cbuf_WR_Addr;
	unsigned int  				RT_Cbuf_WR_SubAddr;
	unsigned int  				RT_Cbuf_WR_TR;
	unsigned int				RT_Cbuf_WR_Count;
	unsigned int  				RT_Cbuf_WR;
	unsigned int  				RT_Cbuf_WR_Rst;
	unsigned int  				RT_Cbuf_RD_Addr;
	unsigned int  				RT_Cbuf_RD_SubAddr;
	unsigned int  				RT_Cbuf_RD_TR;
	unsigned int				RT_Cbuf_RD_Count;
	unsigned int  				RT_Cbuf_RD;
	struct WT_API_RT_CBUF		RT_Cbuf[RT_ADDRESS_COUNT][RT_SUBADDR_COUNT][RT_TRANREC_COUNT];

	unsigned int  				RT_Cbufbroad_WR_SubAddr;
	unsigned int  				RT_Cbufbroad_WR_TR;
	unsigned int  				RT_Cbufbroad_WR;
	struct WT_API_RT_CBUFBROAD	RT_Cbufbroad[RT_SUBADDR_COUNT][RT_TRANREC_COUNT];

	unsigned int 				BC_Run_Mode;		//=0 GE Mode   =1 611 Mode  ==2 804 Mode
	unsigned int 				BC_Enable;
	unsigned int 				BC_Retry;
	unsigned int				BC_Multiple_Retry[8];
	struct WT_API_BC_MBUF 		BC_Msg[BCMsgNum];
	//unsigned short int 			BC_Vector_Message_Flag[RT_ADDRESS_COUNT];

	unsigned int				mFrame_Run_Start;	//启动BC帧周期消息
	unsigned int				mFrame_Run_Stop;	//停止BC帧周期消息

	unsigned int				mFrame_Run_Flag;	//BC帧周期消息运行标志
													//=0 停止发送帧周期消息
													//=1 正在发送帧周期消息
													//=2 暂停发送帧周期消息
													//只能由上位机控制
															
	unsigned int				mFrame_Run_Freq;	//BC消息帧运行周期(us)
	unsigned int				mFrame_Start_MsgID; //BC周期消息初始ID号
	unsigned int				mFrame_Is_Run;		//当前帧正在运行标志
													//=0 停止发送周期消息
													//=1 正在发送周期消息
	unsigned int				mFrame_Curr_MsgID;	//BC周期消息当前ID号


	unsigned int				Async_Run_Start;	//启动BC非周期消息
	unsigned int				Async_Start_MsgID;	//BC非周期消息初始ID号
	unsigned int				Async_Priority;		//BC非周期消息优先级
													//=0 低优先级
													//=1 高优先级
	unsigned int				Async_Is_Run;		//当前帧正在运行标志
													//=0 停止发送非周期消息
													//=1 正在发送周期消息
	unsigned int				Async_Curr_MsgID;	//BC非周期消息当前ID号

	unsigned int				SA31;
	unsigned int				Set_SA31;

	unsigned int				Voltage;
	unsigned int				Set_Voltage;

	unsigned int 				irqfifo_wridx;
	struct WT_IRQ_Fifo_Info		irqfifo[IrqNum];	

	unsigned int 				BM_Enable;
	unsigned int 				BM_Msg_WrIdx;
	//unsigned int 				BM_Msg_RdIdx;
	unsigned int				BM_Irq_Enable;
	unsigned int				BM_Record_Enable;
	struct WT_API_BM_CBUF		BM_Cbuf[RT_ADDRESS_COUNT][RT_SUBADDR_COUNT][RT_TRANREC_COUNT];
	struct WT1553_Msg			BM_Msg[BmNum];

	unsigned int				buffer_offset;
	unsigned short	int			buffer[65536];	
};
#pragma pack()
struct WT_1553B_Board
{
	unsigned int	M1553B_Init_Req;
	unsigned int	M1553B_Enable;
	unsigned int	hM1553B_Chans;
	unsigned int	hM1553B;
};


#endif
