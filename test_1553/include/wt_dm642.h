#ifndef __WT_DM642_H
#define __WT_DM642_H

#ifdef __cplusplus 
extern "C" {
#endif


UINT	__stdcall WT_DM642_Init(UINT device,UINT sdram_test,UINT sram_test,PCHAR prog_file,PCHAR rbf_file_a,PCHAR rbf_file_b);
UINT	__stdcall WT_DM642_ReInit(UINT device,UINT sdram_test,UINT sram_test,PCHAR prog_file,PCHAR rbf_file_a,PCHAR rbf_file_b);
void	__stdcall WT_DM642_Exit(UINT device);
void	__stdcall WT_DM642_Test_DSP_INT_PCI(UINT device);
void	__stdcall WT_DM642_GetIrqCounter(UINT device,UINT *irq);
void	__stdcall WT_DM642_PCI_Config(UINT device,USHORT VendorID,USHORT DeviceID,USHORT RvID,UINT SubsystemID);
void	__stdcall WT_DM642_Set_Board_Config(UINT device,struct WT_BOARD_CONFIG *bd_config);
void	__stdcall WT_DM642_Get_Board_Config(UINT device,struct WT_BOARD_CONFIG *bd_config);
void	__stdcall WT_DM642_Get_Board_Type(UINT device,UINT *board_type_a,UINT *board_type_b);
void	__stdcall WT_DM642_Get_FPGA_ID(UINT device,UINT *fpga_id_a,UINT *fpga_id_b);
void	__stdcall WT_DM642_Get_FPGA_CLK(UINT device,UINT *fpga_clk0_a,UINT *fpga_clk1_a,UINT *fpga_clk0_b,UINT *fpga_clk1_b);
void	__stdcall WT_DM642_MapToFPGA_A(UINT device);
void	__stdcall WT_DM642_MapToFPGA_B(UINT device);
PVOID	__stdcall WT_DM642_Get_pHsotCmd(UINT device);

#ifdef __cplusplus 
}
#endif

#endif
