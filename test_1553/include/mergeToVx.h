
#include "vxWorks.h"
#include "string.h"
#include "string.h"
#include "taskLib.h"
#include "logLib.h"
#include "semLib.h"

#include "../../target/h/drv/pci/pciIntLib.h"
#include "../../target/h/drv/pci/pciConfigLib.h"
#include "iv.h"
#include "intLib.h"
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "sysLib.h"
#include "vxLib.h"
#define log
#define _stdcall
#define __stdcall
#define VXW_PCI_X86
#define _VER524
//#define GUID int
#define INVALID_HANDLE_VALUE -1
#define PCI_VENID 0X5754
#define PCI_DEVID 0X0642
#define Command_ADDR          0X02 /*WORD ADDR*/
#define Status_ADDR           0X03 /*WORD ADDR*/
#define RevisionID_ADDR       0X08
#define ProgIf_ADDR           0X09
#define SubClass_ADDR         0X0A
#define BaseClass_ADDR        0X0B
#define CacheLineSize_ADDR    0X0C
#define LatencyTimer_ADDR     0X0D
#define HeaderType_ADDR       0X0E
#define BIST_ADDR             0X0F
#define PCI_ADDRBASE          0X04  /*LONG ADDR*/
#define CardBusCISPtr_ADDR    0X0A  /*LONG ADDR*/
#define SubsystemVendorID_ADDR     0X16  /*WORD ADDR*/
#define SubsystemID_ADDR      0X17 /*WORD ADDR*/
#define ROMBaseAddress_ADDR   0X0C  /*LONG ADDR*/
#define InterruptLine_ADDR    0X3C
#define InterruptPin_ADDR     0X3D
#define MinimumGrant_ADDR     0X3E
#define MaximumLatency_ADDR   0X3F


typedef int BT_U32BIT ;
typedef short BT_U16BIT;
typedef char* PCHAR;
typedef void* PVOID;
typedef unsigned int* PUINT ;
//typedef unsigned long * PDWORD;
struct pciBoardIndex
{
	int 	index;			/*��?o?*/
	char	intNum;			/*?D??o?*/
	int 	intTimes;		/*��??-2��������??D??��?��y*/
	PCHAR 	pcimem[6];	/*cpci?��?t����??��?memory0 ��??����??��*/
	SEM_ID	semPci;		/*��?2?��?D?o?��?*/
	char 	semIntFlag; /*initial sem flag*/
};
