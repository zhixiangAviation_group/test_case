
#include <stdlib.h>
#include <stdio.h>
#include "../include/mergeToVx.h"/*winie*/
#include "../include/busapi.h"
#include "../include/host_cmd.h"
#include "../include/host_cmd_1553b.h"

#include "../include/wt_1553b.h"

API_INT_FIFO UserFIFO[64];

NOMANGLE BT_INT CCONV BusTools_API_Close(
   BT_UINT cardnum )         // (i) card number
{
	return WT_1553B_Close(cardnum);
}

#if 0
NOMANGLE BT_INT CCONV BusTools_API_Reset(BT_UINT cardnum, BT_UINT reset_flag) 
{
   return API_SUCCESS;
}
#endif

NOMANGLE BT_INT CCONV BusTools_API_Init(
   BT_UINT   cardnum,       // (i) card number (0 - 12)
   BT_U32BIT base_addr,     // (i) base address of memory area or WinRT device
   BT_UINT   ioaddr,        // (i) io address
   BT_UINT * flag)      // (io) 0 -> software driver, 1 -> hardware driver,
                            //      2 -> HW interrupt enable
{
	return WT_1553B_API_Init(cardnum,base_addr);
}

NOMANGLE BT_INT CCONV BusTools_API_InitExtended(
  BT_UINT   cardnum,  // (i) card number (0 - 12) (device number in 32-bit land)
  BT_U32BIT base_addr,// (i) linear base address of board/carrier memory area or WinRT device number
  BT_UINT   ioaddr,   // (i) board or carrier I/O address
  BT_UINT * flag, // (io) 0 -> software driver, 1 -> hardware driver
                      //      2 -> HW interrupt enable
  BT_UINT   platform, // (i) execution platform: PLATFORM_PC, PLATFORM_VMIC, PLATFORM_VME
  BT_UINT   cardType, // (i) IP1553, IP1553MF, IP1553SF, or PCI1553
  BT_UINT   carrier,  // (i) NATIVE, or ....
  BT_UINT   slot,     // (i) SLOT_A, SLOT_B, SLOT_C, SLOT_D.
  BT_UINT   mapping)  // (i) carrier memory map: CARRIER_MAP_DEFAULT, CARRIER_MAP_A24,
                      //     CARRIER_MAP_A32,
                      //     or the address of the IP ID space (VME "NATIVE" only).
{
	return WT_1553B_API_InitExtended(cardnum,base_addr,slot);
}

NOMANGLE BT_INT CCONV BusTools_API_OpenChannel(
  BT_UINT   *chnd,       // (o) card number (0 - 12) (device number in 32-bit land)
  BT_UINT   mode,        // (i) operational mode
  BT_INT    devid,       // (i) linear base address of board/carrier memory area or WinRT device number
  BT_UINT   channel)     // (i) channel 1 - 4.
{
	int state;
	//int erronum=0;
	state=WT_1553B_OpenChannel(chnd,mode,devid,channel);
	//while((state!=API_SUCCESS)&&(erronum<4))
	//{
	//	state=WT_1553B_OpenChannel((PULONG)chnd,mode,devid,channel);
	//	erronum++;
	//}
	return state;
}

NOMANGLE BT_INT CCONV BusTools_FindDevice(
  BT_INT   device_type,  // (i) device number (0 - 15)
  BT_INT   instance)     // (i) device instance (1 - 16)
{
   return WT_1553B_FindDevice(device_type,instance);
}

NOMANGLE BT_INT CCONV BusTools_ListDevices(DeviceList *dlist)     // (i) channel 1 - 4.
{
   return API_SUCCESS;              
}

NOMANGLE BT_INT CCONV BusTools_GetCardnum(void)  // (o)cardnum
{
    return 0;
}

NOMANGLE BT_INT CCONV BusTools_DumpMemory(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_U32BIT region_mask,   // (i) mask of memory regions to dump (bit 1 = region 1)       
   char * file_name,        // (i) pointer to name of output file to create             
   char * heading)          // (i) pointer to message to display in file                      
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_GetAddr(
   BT_UINT cardnum,         // (i) card number (0 - 3)
   BT_UINT memtype,         // (i) memory block type (e.g. API_MEM_REGISTERS)
   BT_U32BIT * start,   // (o) returned address of beginning of block
   BT_U32BIT * end)     // (o) returned address of end of block
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_Checksum1760(BT_U16BIT *mbuf, BT_INT wdcnt)
{
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_ExtTriggerOut(
   BT_UINT cardnum,        // (i) card number
   BT_U16BIT pwidth)       // (i) Trigger pulse width      
{  
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_GetChannelStatus(
   BT_UINT cardnum,         // (i) card number
   API_CHANNEL_STATUS * cstat)          // (o) pointer to heart beat count
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_GetRevision(
   BT_UINT cardnum,         // (i) card number
   BT_UINT * mrev,          // (o) pointer to microcode revision number
   BT_UINT * arev)          // (o) pointer to API revision number
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_GetFWRevision(
   BT_UINT cardnum,         // (i) card number
   float * wrev,          // (o) pointer to microcode revision number
   float * lrev)          // (o) pointer to lpu revision number
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_GetPulse(
   BT_UINT cardnum,         // (i) card number
   BT_U16BIT * beat)          // (o) pointer to heart beat count
{
   return API_SUCCESS;
}

UINT __stdcall WT_1553B_MemoryWrite(UINT cardnum, UINT addr,UINT bcount,VOID * buff);
UINT __stdcall WT_1553B_MemoryRead(UINT cardnum, UINT addr,UINT bcount,VOID * buff);

NOMANGLE BT_INT CCONV BusTools_MemoryAlloc(
   BT_UINT cardnum,         // (i) card number
   BT_UINT segnum,          // (i) Segment to allocate memory in (must be zero)
   BT_U32BIT bcount,        // (i) number of bytes to be allocated (even)
   BT_U32BIT * addr)		// (o) Allocated buffer address in BT hardware
{
	return WT_1553B_MemoryAlloc(cardnum,bcount,(UINT *)addr);
}

NOMANGLE BT_INT CCONV BusTools_MemoryRead(
   BT_UINT cardnum,         // (i) card number
   BT_U32BIT addr,          // (i) BYTE address in BT hardware to begin reading
   BT_U32BIT bcount,        // (i) EVEN number of bytes to read
   VOID * buff)         // (o) Pointer to user's buffer
{
   return WT_1553B_MemoryRead(cardnum,addr,bcount,buff);
}

NOMANGLE BT_INT CCONV BusTools_MemoryWrite(
   BT_UINT cardnum,         // (i) card number
   BT_U32BIT addr,          // (i) BYTE address in BT hardware to begin reading
   BT_U32BIT bcount,        // (i) EVEN number of bytes to read
   VOID * buff)         // (i) Pointer to user's buffer
{
   return WT_1553B_MemoryWrite(cardnum,addr,bcount,buff);
}

NOMANGLE BT_INT CCONV BusTools_RegisterFunction(BT_UINT cardnum,
                                                API_INT_FIFO *sIntFIFO,
                                                BT_UINT wFlag)
{
	memcpy(&UserFIFO[cardnum], sIntFIFO, sizeof(API_INT_FIFO));
	if(wFlag==1)
	{
		UserFIFO[cardnum].mask_index =MAX_FIFO_LEN-1 ;
		return  WT_1553B_SetUserHandle(cardnum, &UserFIFO[cardnum]);
	}
	else
	{
		return  WT_1553B_UnSetUserHandle(cardnum, &UserFIFO[cardnum]);
	}
}

NOMANGLE BT_INT CCONV BusTools_SetBroadcast(
   BT_UINT cardnum,        // (i) card number
   BT_UINT bcast)          // (i) broadcast flag (0 -> no broadcast)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_SetExternalSync(BT_UINT cardnum,
   BT_UINT flag)        // (i) 0 -> disable external sync, 1-> enable sync
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_SetInternalBus(
   BT_UINT cardnum,        // (i) card number
   BT_UINT busflag)        // (i) 0 -> external bus, 1-> internal bus
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_SetTestBus(
   BT_UINT cardnum,        // (i) card number
   BT_UINT busflag)        // (i) TEST_BUS_ENABLE enables the test bus
                           //     TEST_BUS_DISABLE disalbe the test bus
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiscreteSetIO(
   BT_UINT cardnum,         // (i) card number
   BT_U32BIT disSet,        // (i) discrete flags
   BT_U32BIT mask)          // (i) mask
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiscreteGetIO(
   BT_UINT cardnum,        // (i) card number
   BT_U32BIT *disDirValue)    // (o) discrete values
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiscreteRead(
   BT_UINT cardnum,        // (i) card number
   BT_INT disSel,          // (i) Discrete select = 0 all
   BT_U32BIT *disValue)    // (o) discrete values
{
   return API_SUCCESS;
}


NOMANGLE BT_INT CCONV BusTools_DiscreteReadRegister(
   BT_UINT cardnum,        // (i) card number
   BT_INT regnum,          // (i) Discrete select = 0 all
   BT_U32BIT *disValue)    // (o) discrete values
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiscreteWrite(
   BT_UINT cardnum,        // (i) card number
   BT_INT  disSel,         // (i) Discrete select 0 = all
   BT_UINT disFlag)        // (i) 0 reset the discrete 1 set the discrete
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiscreteTriggerOut(
   BT_UINT cardnum,         // (i) card number
   BT_INT disflag)          // (i) TRIGGER_OUT_DIS_7 - use Discrete 7 as trigger out
                            //     TRIGGER_OUT_DIS_8 - use Discrete 8 as trigger out
                            //     TRIGGER_OUT_DIS_NONE - use neither
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiscreteTriggerIn(
   BT_UINT cardnum,         // (i) card number
   BT_UINT trigflag)         // (i) 0=none, 1=485, 2=dis7 3=dis8 
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_DiffTriggerOut(
   BT_UINT cardnum,         // (i) card number
   BT_INT  chflag,          // (i) 0 = disconnect 1 = connect channel
   BT_INT  diffen)          // (i) 0 = disable 1 = enable differential output 
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_SetOptions(
   BT_UINT cardnum,        // (i) card number
   BT_UINT intflag,        // (i) 0x0001 -> suppress MF OFlow message
                           //     0x0004 -> Dump on BM Stop
                           //     0x0008 -> BM trigger on message
   BT_UINT resettimer,     // (i) Reset TT on Sync Mode code
   BT_UINT trig_on_sync,   // (i) Trigger output on Sync Mode Code enable = 1
                           //     disable = 0;
   BT_UINT enable_rt)      // (i) RT start on trigger input enable = 1
                           //     Disalbe = 0;
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_SetPolling(BT_UINT polling)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_SetSa31(
   BT_UINT cardnum,        // (i) card number
   BT_UINT sa31)           // (i) 1=SA31 is a mode code, 0=SA31 NOT a mode code
{
   return WT_1553B_SetSa31(cardnum,sa31);
}

NOMANGLE BT_INT CCONV BusTools_SetVoltage(
   BT_UINT cardnum,        // (i) card number
   BT_UINT  voltage,       // (i) volts peak-to-peak * 100
   BT_UINT  coupling)      // (i) 0 -> direct, 1 -> transformer coupling
{
   return WT_1553B_Set_Voltage(cardnum,voltage,coupling);
}

NOMANGLE char * CCONV BusTools_StatusGetString(
   BT_INT status)        // (i) API Status code
{
   static char static_buf[100];

   switch(status)
   {
   case API_SUCCESS             : return "Successful completion of routine";

   case API_CONTINUE            : return "API function should continue execution normally";
   case API_RETURN_SUCCESS      : return "API function should return immediately with API_SUCCESS";
   case API_NEVER_CALL_AGAIN    : return "User function is never to be called again";
   case API_BUSTOOLS_INT_USED   : return "Board Interrupt already in use";
   case API_INIT_NO_SUPPORT     : return "Cannot Initialize board type with this function";
   case API_NULL_PTR            : return "NULL Pointer passed to function";
   case API_MAX_CHANNELS_INUSE  : return "Maximum 1553 channels already in use";
   case API_CARDNUM_INUSE       : return "cardnum in already in use";
   case API_BAD_PRODUCT_LIST    : return "Unable to build the Condor Engineering Product list";
   case API_BAD_DEVICE_ID       : return "Bad device ID";
   case API_INSTALL_INIT_FAIL   : return "CEI_INSTALL init failure";

   case API_BUSTOOLS_INITED     : return "Board has already been initialized";
   case API_BUSTOOLS_NOTINITED  : return "BusTools API has not been initialized";
   case API_BUSTOOLS_BADCARDNUM : return "Bad card number specified";
   case API_BUSTOOLS_BADINTTYPE : return "Bad interrupt type or window handle specified";
   case API_BUSTOOLS_BADBUFSIZE : return "Bad buffer size specified";
   case API_BUSTOOLS_BADCOUPLING: return "Bad coupling specified in BusTools_SetVoltage";
   case API_BUSTOOLS_BADVOLTAGE : return "Bad voltage specified in BusTools_SetVoltage";
   case API_BUSTOOLS_BADSEGNUM  : return "Segment number must be 0";
   case API_BUSTOOLS_EVENBCOUNT : return "Even byte counts always required";
   case API_BUSTOOLS_BADMEMORY  : return "BusTools Board Dual-Port Memory Self-Test Failed";
   case API_BUSTOOLS_TOO_MANY   : return "Too many user interrupt functions registered";
   case API_BUSTOOLS_FIFO_BAD   : return "User API_INT_FIFO structure corrupted or bad entry";
   case API_BUSTOOLS_NO_OBJECT  : return "Error creating event object or thread";
   case API_BUSTOOLS_BAD_REGS   : return "Register File Self-Test failed";
   case API_BUSTOOLS_NO_FILE    : return "Could not open the specified file";
   case API_BUSTOOLS_NO_MEMORY  : return "BusTools_MemoryAlloc request overflows first 64 Kw";
   case API_HW_IQPTR_ERROR      : return "Hardware Interrupt Pointer register error";
   case API_BIT_BC_RT_FAIL_PRI  : return "BIT failure/data error detected BC-RT on primary bus";
   case API_BIT_BC_RT_FAIL_SEC  : return "BIT failure/data error detected BC-RT on secondary bus";
   case API_BIT_BM_RT_FAIL_PRI  : return "BIT failure/data error detected BM-RT on primary bus";
   case API_BIT_BM_RT_FAIL_SEC  : return "BIT failure/data error detected BM-RT on secondary bus";

   case API_BUSTOOLS_FIFO_DUP   : return "Specified API_INT_FIFO structure already in use";

   case API_HARDWARE_NOSUPPORT  : return "Function not supported by current hardware";
   case API_OUTDATED_FIRMWARE   : return "Firmware version not supported by this software, contact factory for upgrade";
   case API_NO_OS_SUPPORT       : return "Function not supported by underlying Operating System";
   case API_NO_BUILD_SUPPORT    : return "Function not supported by API as built";
   case API_CHANNEL_OPEN_OTHER  : return "Board or channel already opened as another cardnum";
   case API_BUSTOOLS_BAD_ALIGN  : return "API structures are not properly aligned";
   case API_SINGLE_FUNCTION_ERR : return "Attempt to run multiple functions on single function board";
   case API_CANT_LOAD_USER_DLL  : return "Cannot load specified user DLL";
   case API_REGISTERFUNCTION_OFF: return "RegisterFunction operations not enabled";

   case API_BAD_PARAM			: return "Bad parameter for the function call";

   case API_EI_BADMSGTYPE       : return "Bad message type specified in LoadErrors";
   case API_EI_ILLERRORNO       : return "Error injection buffer number > number of buffers available";
   case API_EI_ILLERRORADDR     : return "Illegal error buffer address";

   case API_BAD_ADDR_TYPE       : return "Bad address type for BusTools_GetAddr()";

   case API_BAD_DISCRETE        : return "Attempting to confirm invalid discrete";
   case API_OUTPUT_DISCRETE     : return "Attempting to read from an output";
   case API_INPUT_DISCRETE      : return "Attempting to write to an input";
   case API_MEM_ALLOC_ERR       : return "Error allocating memory";
   case API_BADDATATYPE	        : return "Bad data type for EU conversion"; 
   case API_BADBCDDATA          : return "Bad data for BCD EU conversion";
   case API_BADTRANSLATE        : return "Bad translation table data, for translate EU conversion";
   case API_BADFACTORTYPE       : return  "Bad factor type for scaled EU conversion";

   case API_BC_NOTINITED        : return "BC has not been initialized";
   case API_BC_INITED           : return "BC has already been initialized";
   case API_BC_RUNNING          : return "BC is currently running";
   case API_BC_NOTRUNNING       : return "B not currently running";
   case API_BC_MEMORY_OFLOW     : return "BC memory overflow";
   case API_BC_ILLEGAL_MBLOCK   : return "BC illegal memory block number specified";
   case API_BC_MBLOCK_NOMATCH   : return "BC specified addr is not a BC message block";
   case API_BC_MBUF_NOT_ALLOC   : return "BC message buffers have not been allocated";
   case API_BC_MBUF_ALLOCD      : return "BC the number of messages cannot be increased after allocation";
   case API_BC_ILLEGAL_NEXT     : return "BC illegal next message number specified";
   case API_BC_ILLEGAL_BRANCH   : return "BC illegal branch message number specified";
   case API_BC_MESS1_COND       : return "BC first message in buffer is conditional";
   case API_BC_BAD_COND_ADDR    : return "BC bad address value in conditional message";
   case API_BC_BADTIMEOUT1      : return "BC illegal \"No Response\" timeout";
   case API_BC_BADTIMEOUT2      : return "BC illegal \"Late Response\" timeout";
   case API_BC_BADFREQUENCY     : return "BC illegal minor frame frequency";
   case API_BC_HALTERROR        : return "BC error detected during stop, bus is probably unterminated";
   case API_BC_BOTHBUFFERS      : return "BC message cannot specify both buffers";
   case API_BC_BOTHBUSES        : return "BC message cannot specify both buses";
   case API_BC_UPDATEMESSTYPE   : return "BC MessageUpdate cannot operate on branch or conditional messages";
   case API_BC_ILLEGALMESSAGE   : return "BC message in memory is not legal";
   case API_BC_ILLEGALTARGET    : return "BC branch data message number not legal";
   case API_BC_NOTMESSAGE       : return "BC msg is not a proper 1553-type message";
   case API_BC_NOTNOOP          : return "BC msg is not a proper nop-type message";
   case API_BC_APERIODIC_RUNNING: return "BC Aperiodics still running, cannot start new msg list";
   case API_BC_APERIODIC_TIMEOUT: return "BC Aperiodic messages did not complete in time";
   case API_BC_CANT_NOOP        : return "BC cannot noop or un-noop a noop message";
   case API_BC_MINOR_OFLOW      : return "BC message processing has overflowed specified Minor Frame time.  "
                                         "Bus operation may not be accurate.  Ensure that the bus is properly terminated.";
   case API_BC_READ_TIMEOUT     : return "BC Timeout on buffer read";
   case API_BC_READ_NODATA      : return "No BC data in int queue";
   case API_BC_AUTOINC_INUSE    : return "Auto-Increment in use for message";


   case API_BM_NOTINITED        : return "BM Init or MessageAlloc has not been called";
   case API_BM_INITED           : return "BM has already been initialized";
   case API_BM_RUNNING          : return "BM is currently running";
   case API_BM_NOTRUNNING       : return "BM not currently running";
   case API_BM_MEMORY_OFLOW     : return "BM memory overflow";
   case API_BM_CBUF_EXISTS      : return "BM specified control buffer already exists";
   case API_BM_CBUF_NOTDEFINED  : return "BM specified control buffer does not exist";
   case API_BM_ILLEGAL_ADDR     : return "BM illegal RT address specified";
   case API_BM_ILLEGAL_SUBADDR  : return "BM illegal RT sub-address specified";
   case API_BM_ILLEGAL_TRANREC  : return "BM illegal RT transmit/receive flag specified";
   case API_BM_ILLEGAL_MBUFID   : return "BM illegal message buffer number specified";
   case API_BM_MBUF_NOMATCH     : return "BM specified message number not allocated";
   case API_BM_WRAP_AROUND      : return "BM API message buffer has overflowed, data has been lost";
   case API_BM_ILLEGAL_MESSAGE  : return "BM illegal command word detected in HW message";
   case API_BM_MSG_ALLOC_CALLED : return "BM_MessageAlloc has already been called";
   case API_BM_HW_WRAP_AROUND   : return "BM HW message buffer has overflowed, data has been lost";
   case API_BM_POINTER_REG_BAD  : return "BM HW pointer register contents invalid";
   case API_BM_READ_TIMEOUT     : return "BM Timeout on buffer read";
   case API_BM_READ_NODATA      : return "No BM data in int queue";
   case API_BM_1760_ERROR       : return "Checksum error on MIL-STD-1760 message.";

   case API_RT_NOTINITED        : return "RT has not been initialized";
   case API_RT_INITED           : return "RT has already been initialized";
   case API_RT_RUNNING          : return "RT is currently running";
   case API_RT_NOTRUNNING       : return "RT not currently running";
   case API_RT_MEMORY_OFLOW     : return "RT memory overflow";
   case API_RT_CBUF_EXISTS      : return "RT subunit MBUFs already allocated";
   case API_RT_CBUF_NOTDEFINED  : return "RT subunit not yet defined";
   case API_RT_ILLEGAL_ADDR     : return "RT illegal RT address specified";
   case API_RT_ILLEGAL_SUBADDR  : return "RT illegal RT sub-address specified";
   case API_RT_ILLEGAL_TRANREC  : return "RT illegal RT transmit/receive flag specified";
   case API_RT_ILLEGAL_MBUFID   : return "RT specified message buffer ID is larger than the number of buffers created";
   case API_RT_TOOMANY_MBUFS    : return "not used";
   case API_RT_CBUF_BROAD       : return "RT address 31 is broadcast only";
   case API_RT_CBUF_NOTBROAD    : return "RT specified RT address is not broadcast";
   case API_RT_MBUF_NOMATCH     : return "RT message buffer not found at specified address";
   case API_RT_BROADCAST_DISABLE: return "RT 31 Broadcast is disabled";
   case API_RT_SELF_TEST_MODE   : return "RT Self Test Wrap-Around Mode selected, normal operation inhibited";
   case API_RT_READ_TIMEOUT     : return "RT Timeout on buffer read";
   case API_RT_READ_NODATA      : return "No RT data in int queue";
   case API_NO_HARDWIRE_RT      : return "RT hardwired address not enabled.";

   case API_LV_BADARRAY         : return "LabView array/structure size not correctly setup";
   case API_NO_LV_SUPPORT       : return "Function not supported in LabView";

   case API_PLAYBACK_INIT_ERROR : return "Error initializing Playback";
   case API_PLAYBACK_BAD_THREAD : return "Attempt to create Playback thread failed";
   case API_PLAYBACK_BAD_FILE   : return "Playback file open failed";
   case API_PLAYBACK_BAD_EVENT  : return "Playback event creation error";
   case API_PLAYBACK_BUF_EMPTY  : return "Playback Buffer empty";
   case API_PLAYBACK_BAD_EXIT   : return "Unexpected Exit from Playback";
   case API_PLAYBACK_BAD_MEMORY : return "Unable to allocate Playback memory on Host";
   case API_PLAYBACK_DISK_READ  : return "Disk read Error during Playback";
   case API_PLAYBACK_RUNNING    : return "Playback is already running";
   case API_PLAYBACK_BAD_ALLOC  : return "Failure to allocate enough BusTools Memory for Playback";

   case API_TIMETAG_BAD_DISPLAY : return "Unknown or unsupported Time Tag display format";
   case API_TIMETAG_BAD_INIT    : return "Unknown Time Tag Initialization method";
   case API_TIMETAG_BAD_MODE    : return "Unknown Time Tag Operating Mode";
   case API_TIMETAG_NO_DLL      : return "DLL containing BusTools_TimeTagSet() could not be loaded";
   case API_TIMETAG_NO_FUNCTION : return "Could not get the address of the BusTools_TimeTagSet() function";
   case API_TIMETAG_USER_ERROR  : return "User function BusTools_TimeTagSet() returned an error";
   case API_TIMETAG_WRITE_ERROR : return "Cannot write to time tag load register when in API_TM_IRIG mode";
   case API_IRIG_NO_SIGNAL  : return "No IRIG signal present";

   case BTD_ERR_PARAM           : return "Driver-invalid parameter: type, slot, addr, etc.";
   case BTD_ERR_NOACCESS        : return "Driver-unable to map/access adapter";
   case BTD_ERR_INUSE           : return "Driver-adapter already in use";
   case BTD_ERR_BADADDR         : return "Driver-invalid address";
   case BTD_ERR_NODETECT        : return "Board detect failure (IO or Config register ID invalid)";
   case BTD_ERR_BADCFG          : return "Board jumper configuration invalid or not supported";
   case BTD_ERR_NOTSETUP        : return "Driver-adapter has not been setup";
   case BTD_ERR_FPGALOAD        : return "Driver-FPGA load failure";
   case BTD_ERR_WRITEFAIL       : return "Driver-not used";
   case BTD_ERR_NOMEMORY        : return "Driver-error allocating host memory for BM buffers";
   case BTD_ERR_BADADDRMAP      : return "Driver-bad initial mapping of address";
   case BTD_ERR_BADEXTMEM       : return "Driver-bad extended memory mapping";
   case BTD_ERR_BADBOARDTYPE    : return "Driver-unknown board type";
   case BTD_ERR_BADWCS          : return "Driver-read back failure of Writeable Control Store";
   case BTD_NO_PCI_SERVICES     : return "PCI services not available/not installed";
   case BTD_NO_PCI_CARRIER      : return "Cannot locate IP-D1553 PCI carrier";
   case BTD_NO_PLATFORM         : return "Platform specified unknown or not supported";
   case BTD_BAD_MANUFACTURER    : return "IP ID PROM Manufacturer code not 0x79";
   case BTD_BAD_MODEL           : return "IP ID PROM Model number not 0x05 or 0x08";
   case BTD_BAD_SERIAL_PROM     : return "IP Serial PROM needs update, contact Condor Engineering";
   case BTD_CHAN_NOT_PRESENT    : return "Channel not present (on multi-channel board)";
   //case BTD_NON_SUPPORT         : return "Bus/Carrier/OS combination not supported by API";
   case BTD_BAD_HW_INTERRUPT    : return "Hardware interrupt number bad or not defined in registry";
   case BTD_FPGA_NOT_CLEAR      : return "The FPGA configuration failed to clear";

   case BTD_ERR_NOWINRT         : return "WinRT driver not loaded/started.  Device Conflict or board not installed?";
   case BTD_ERR_BADREGISTER     : return "WinRT parameters don't match registry";
   case BTD_ERR_BADOPEN         : return "WinRT device open/map failed";
   case BTD_UNKNOWN_BUS         : return "Bus is not PCI, ISA or VME";
   case BTD_BAD_LL_VERSION      : return "Unsupported lowlevel driver installed";
   case BTD_BAD_INT_EVENT       : return "Unable to create interrupt event";
   case BTD_ISR_SETUP_ERROR     : return "Error setting up the Interrupt Service Routine";
   case BTD_CREATE_ISR_THREAD   : return "Error creating the Interrupt Service Routine thread";
   case BTD_NO_REGIONS_TO_MAP   : return "No regions requested in call to vbtMapBoardAddresses";
   case BTD_NEW_PCCARD_FW       : return "PCC-1553 firmware is too new to be used with this version of the API";
   case BTD_OLD_PCCARD_FW       : return "PCC-1553 firmware is too old for this version of the API, use the JAM Player to update it";
   case BTD_BAD_CONF_FILE       : return "Unable to open ceidev.conf";
   case BTD_NO_DRV_MOD          : return "No Driver Module found";
   case BTD_IOCTL_DEV_ERR       : return "Error in ioctl get device";
   case BTD_IOCTL_SET_REG       : return "Error in ioctl set region";
   case BTD_IOCTL_REG_SIZE      : return "Error in getting ioclt region size";
   case BTD_IOCTL_GET_REG       : return "Error in ioctl get region";
   case BTD_BAD_SIZE            : return "Region size is 0";
   case BTD_BAD_PROC_ID         : return "Unable to set process ID";
   case BTD_HASH_ERR            : return "Unable to setup hash table";
   case BTD_NO_HASH_ENTRY       : return "No hash table entry found";
   case BTD_WRONG_BOARD         : return "Wrong Board Type for this command";
   case BTD_MODE_MISMATCH       : return "Mode Mis-Match on IP-D1553";
   case BTD_IRIG_NO_LOW_PEAK    : return "No lower peak on IRIG DAC calibration";
   case BTD_IRIG_NO_HIGH_PEAK   : return "No upper peak on IRIG DAC calibration";
   case BTD_IRIG_LEVEL_ERR      : return "IRIG-B Signal level too low";
   case BTD_IRIG_NO_SIGNAL      : return "No IRIG Signal Detected";
   case BTD_RTADDR_PARITY       : return "Parity Error on Hardwired RT address lines";

   }
   sprintf(static_buf,"Unknown error code: %d (0x%X).",status,status);
   return static_buf;
}

NOMANGLE BT_U32BIT CCONV BusTools_InterMessageGap(
   API_BM_MBUF * first,
   API_BM_MBUF * second)
{
	return 0L;            // If both messages are @ same time, return 0.
}

NOMANGLE BT_INT CCONV BusTools_ErrorCountClear(
   BT_UINT   cardnum)       // (i) card number (0 based)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_ErrorCountGet(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_U32BIT * msgcount,
   BT_U32BIT * buf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_API_LoadUserDLL(
   BT_UINT cardnum,                // (i) - Card number to open/close
   const char * szDLLName)     // (i) - Pointer to the comma-delimited list of DLL names
{
   return API_SUCCESS;
}
#ifdef _PLAYBACK_ 
NOMANGLE BT_INT CCONV BusTools_Playback(
   BT_UINT cardnum,           // (i) card number (0 - based)
   API_PLAYBACK playbackData)  // (io) Control and Status structures
{
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_Playback_Stop(
   BT_UINT cardnum)         // (i) card number (0 - based)
{
   return API_SUCCESS;
}
#endif
NOMANGLE void CCONV BusTools_TimeGetString(
   BT1553_TIME * curtime,   // (i) Timer to be converted to string value.
   char        * string)    // (o) Pointer, store resulting time string.
{
}

NOMANGLE BT_INT CCONV BusTools_TimeTagInit(
   BT_UINT  cardnum)              // (i) Card number.
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_TimeTagMode(
   BT_UINT   cardnum,       // (i) card number (0 - based)
   BT_INT   TTDisplay,      // (i) time tag display format
   BT_INT   TTInit,         // (i) time tag counter initialization mode
   BT_INT   TTMode,         // (i) time tag timer operation mode
   char     *DLLname,       // (i) name of the DLL containing time function
   BT_U32BIT TTPeriod,      // (i) period of external TTL sync input
   BT_U32BIT lParm1,        // (i) spare parm1
   BT_U32BIT lParm2)        // (i) spare parm2
{
      return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_TimeTagRead(
   BT_UINT cardnum,               // (i) board number
   BT1553_TIME * timetag)     // (o) resulting 48-bit time value
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_TimeTagWrite(
   BT_UINT cardnum,           // (i) card number (0 - based)
   BT1553_TIME * timetag, // (i) pointer to time tag structure
   BT_INT            flag)    // (i) flag->0 just load the TT Register,
                              //     flag->1 load the TT Register into the counter
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_ReadVMEConfig(
   BT_UINT cardnum,         // (i) card number
   BT_U16BIT *vdata)       // (o) pointer to users buffer
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_WriteVMEConfig(
   BT_INT cardnum,         // (i) card number
   BT_UINT offset,         // (i) offset to base A16 address
   BT_U16BIT vdata)        // (i) data written to offset
{
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_API_GetBaseAddr(BT_UINT cardnum, BT_U32BIT * baseAddress) 
{
   return API_SUCCESS;
}

NOMANGLE char * CCONV BusTools_DataGetString(DATA_CONVERT *cdat)
{
   return cdat->string;
}

NOMANGLE BT_INT CCONV BusTools_PLX_Dump(BT_INT cardnum)
{
   return 0;
}

NOMANGLE BT_INT CCONV BusTools_PLXRead(
   BT_UINT cardnum,         // (i) card number
   BT_U32BIT addr,          // (i) BYTE address in BT hardware to begin reading
   BT_U16BIT *rData)         // (o) Pointer to user's data
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_PLXWrite(
   BT_UINT cardnum,         // (i) card number
   BT_U32BIT addr,          // (i) BYTE address in BT hardware to begin reading
   BT_U16BIT rData)         // (o) Pointer to user's data
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_PLX_View(BT_UINT cardnum)
{
   return API_SUCCESS;
}
NOMANGLE BT_INT CCONV BusTools_IRIG_Calibration(BT_UINT cardnum, 
                                                BT_INT flag)        // (i) card number
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_IRIG_Config(BT_UINT cardnum,   // (i) card number
                                           BT_UINT intFlag,   // Internal IRIG or External
                                           BT_UINT outFlag)   // Output Internal IRIG    
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_IRIG_SetTime(BT_UINT cardnum,   // (i) card number
                                            long timedate,     // (i) timedate to set -1 use system time
                                            BT_U32BIT flag)    // (i) 0 = gmtime 1 = localtime
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_IRIG_Valid(BT_UINT cardnum)    
{
	return API_SUCCESS;
}

#if 0
NOMANGLE BT_INT CCONV BusTools_IRIG_SetBias(BT_UINT cardnum,   // (i) card number
                                            BT_INT bias)    
{
   return API_SUCCESS;
}
#endif
NOMANGLE BT_INT CCONV BusTools_PutData(
   BT_UINT   cardnum,       // (i) card number (0 - based)
   BT_U32BIT addr,          // (i) card address of data to write (must be even)
   BT_U32BIT bcount,        // (i) number of bytes to write (must be even)
   VOID * buff)
{
	return 0;
}
