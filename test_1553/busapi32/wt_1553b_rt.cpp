
#include <stdlib.h>
#include <stdio.h>
#include "../include/mergeToVx.h"/*winie*/
#include "../include/busapi.h"
#include "../include/host_cmd.h"
#include "../include/host_cmd_1553b.h"

#include "../include/wt_1553b.h"

#include "../include/wtapi_type.h"
#include "../include/wtapi_lowlevel.h"


NOMANGLE BT_INT CCONV BusTools_RT_AbufRead(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddress,         // (i) RT Terminal Address
   API_RT_ABUF * abuf)    // (o) Pointer to buffer to receive abuf data
{
    WT_API_RT_ABUF pAbuf;
	UINT rv=WT_1553B_RT_AbufRead(cardnum,rtaddress,&pAbuf);
	abuf->enable_a = pAbuf.enable_a;
	abuf->enable_b = pAbuf.enable_b; 
	abuf->command = pAbuf.command;
    abuf->bit_word = pAbuf.bit_word;
    abuf->inhibit_term_flag = pAbuf.inhibit_term_flag;
    abuf->status = pAbuf.status;
	if(rv==WT_SUCCESS) return API_SUCCESS;
	  else  return API_BUSTOOLS_BADCARDNUM;
}

NOMANGLE BT_INT CCONV BusTools_RT_AbufWrite(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddress,         // (i) RT Terminal Address
   API_RT_ABUF * abuf)    // (i) Pointer to user's abuf structure
{
    WT_API_RT_ABUF pAbuf;
	pAbuf.enable_a =abuf->enable_a ;
	pAbuf.enable_b =abuf->enable_b ; 
	pAbuf.command = abuf->command ;
    pAbuf.bit_word = abuf->bit_word;
    pAbuf.inhibit_term_flag = abuf->inhibit_term_flag;
    pAbuf.status = abuf->status;
	UINT rv=WT_1553B_RT_AbufWrite(cardnum,rtaddress,&pAbuf);
	if(rv==WT_SUCCESS) return API_SUCCESS;
	  else  return API_BUSTOOLS_BADCARDNUM;
}

NOMANGLE BT_INT CCONV BusTools_RT_MonitorEnable(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddress,         // (i) RT Terminal Address
   BT_UINT mode)              // (i) rt monitor mode 0 = disable; 1 = enable
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_CbufbroadRead(
   BT_UINT cardnum,           // (i) card number
   BT_UINT subaddr,           // (i) RT Subaddress
   BT_UINT tr,                // (i) Transmit=1, Receive=0
   API_RT_CBUFBROAD * apicbuf)  // (o) Pointer to user's RT control buffer
{
    struct WT_API_RT_CBUF SetCbuf;
    UINT  ReadBufCount;
    WT_1553B_RT_CbufRead(cardnum,31,subaddr,tr,&ReadBufCount,&SetCbuf);
	apicbuf->mbuf_count=ReadBufCount;
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_CbufbroadWrite(
   BT_UINT cardnum,           // (i) card number
   BT_UINT subaddr,           // (i) Subaddress to write
   BT_UINT tr,                // (i) Non-zero indicates Transmit, zero Receive
   API_RT_CBUFBROAD * apicbuf) // (i) Buffer containing Control Buffer
{
    WT_API_RT_ABUF pAbuf;
 	pAbuf.enable_a =1;
	pAbuf.enable_b =1 ; 
//	pAbuf.command = abuf->command ;
    pAbuf.bit_word = 0;
    pAbuf.inhibit_term_flag =0;
    pAbuf.status = 0;
	UINT rv=WT_1553B_RT_AbufWrite(cardnum,31,&pAbuf);
	if(rv!=WT_SUCCESS) return API_BUSTOOLS_BADCARDNUM;
    struct WT_API_RT_CBUF SetCbuf;
    UINT  ReadBufCount;
    SetCbuf.legal_wordcount =0xffffffff;
    WT_1553B_RT_CbufWrite(cardnum,31,subaddr,tr,apicbuf->mbuf_count,&SetCbuf);
    WT_1553B_RT_CbufRead(cardnum,31,subaddr,tr,&ReadBufCount,&SetCbuf);

   if(ReadBufCount==(UINT)apicbuf->mbuf_count)
    	return API_SUCCESS;
   else
        return API_BUSTOOLS_BADCARDNUM;

}

NOMANGLE BT_INT CCONV BusTools_RT_CbufRead(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddr,            // (i) Remote Terminal address to be setup
   BT_UINT subaddr,           // (i) Subaddress to read
   BT_UINT tr,                // (i) Non-zero indicates Transmit, zero Receive
   BT_UINT * mbuf_count,  // (o) Number of Message Buffers created
   API_RT_CBUF * apicbuf) // (o) User's buffer to receive Control Buffer
{
   struct WT_API_RT_CBUF SetCbuf;
   UINT  ReadBufCount;
   UINT  rv=WT_1553B_RT_CbufRead(cardnum,rtaddr,subaddr,tr,&ReadBufCount,&SetCbuf);
   *mbuf_count=ReadBufCount;
   apicbuf->legal_wordcount =SetCbuf.legal_wordcount ;
   if(rv==WT_SUCCESS) return API_SUCCESS;
	  else  return API_BUSTOOLS_BADCARDNUM;
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_CbufWrite(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddr,            // (i) Remote Terminal address to be setup
   BT_UINT subaddr,           // (i) Subaddress to be setup
   BT_UINT tr,                // (i) Non-zero indicates Transmit, zero Receive
   BT_INT  mbuf_count,        // (i) Num buffers, If negative, one pass through only.
                              //     Current buffer number if second call.
   API_RT_CBUF * apicbuf) // (i) User-supplied CBUF; legal word count mask
{
   struct WT_API_RT_CBUF SetCbuf;
   UINT  ReadBufCount;
      SetCbuf.legal_wordcount =apicbuf->legal_wordcount;
      WT_1553B_RT_CbufWrite(cardnum,rtaddr,subaddr,tr,mbuf_count,&SetCbuf);
      WT_1553B_RT_CbufRead(cardnum,rtaddr,subaddr,tr,&ReadBufCount,&SetCbuf);

   if(ReadBufCount==(UINT)mbuf_count)
    	return API_SUCCESS;
   else
        return API_BUSTOOLS_BADCARDNUM;
}

NOMANGLE BT_INT CCONV BusTools_RT_Init(
    BT_UINT cardnum,          // (i) card number
    BT_UINT testflag)         // (i) flag; ignored!
{
  API_RT_ABUF  	Abuf_RT;	  // RT address buffer structure.
  API_RT_CBUF	Cbuf_RT;          // RT control buffer structures.
  int rt;
  // return WT_1553B_RT_Init(cardnum);
   if(WT_1553B_RT_Init(cardnum)==WT_SUCCESS)
   {
	BusTools_SetVoltage(cardnum, 1980, 0);
     for ( rt=1;rt<31;rt++)
	 {
       Abuf_RT.enable_a = 0;			// Respond on bus A
       Abuf_RT.enable_b = 0;			// Respond on bus B
       Abuf_RT.inhibit_term_flag = 0;	        // Inhibit terminal flag in status word
       Abuf_RT.status =0;     // Set status word
       Abuf_RT.bit_word = 0x0000;		// Set BIT word (for mode code 19)
       BusTools_RT_AbufWrite(cardnum, rt, &Abuf_RT);
	 };
     for (int rt=1;rt<31;rt++)
     for(int sub=1;sub<31;sub++)
	 {
        Cbuf_RT.legal_wordcount = 0xffffffff;  // any word count is legal.
        BusTools_RT_CbufWrite(cardnum, rt, sub, 0, 1, &Cbuf_RT);
        BusTools_RT_CbufWrite(cardnum, rt, sub, 1, 1, &Cbuf_RT);
	  }
      return API_SUCCESS;
   }else
   {
      return API_BUSTOOLS_BADCARDNUM;
   };

}

NOMANGLE BT_INT CCONV BusTools_RT_MessageGetaddr(
   BT_UINT cardnum,           // (i) Card number
   BT_UINT rtaddr,            // (i) RT Address
   BT_UINT subaddr,           // (i) RT Subaddress
   BT_UINT tr,                // (i) Transmit or Receive
   BT_UINT mbuf_id,           // (i) Message Buffer ID (0-based)
   BT_U32BIT * mbuf_offset)  // (o) Offset to message buffer
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_MessageGetid(
   BT_UINT cardnum,           // (i) Card number
   BT_U32BIT     addr,        // (i) Address (byte address) of message to locate
   BT_UINT * rtaddr,      // (o) RT Terminal address
   BT_UINT * subaddr,     // (o) RT Subaddress
   BT_UINT * tr,          // (o) Transmit or Receive
   BT_UINT * mbuf_id)     // (o) Message buffer number (0-based)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_MessageRead(
   BT_UINT cardnum,         // (i) card number
   BT_UINT rtaddr,          // (i) RT Terminal Address of message to read
   BT_UINT subaddr,         // (i) RT Subaddress of message to read
   BT_UINT tr,              // (i) Receive or Transmit buffer is to be read
   BT_UINT mbuf_id,         // (i) ID number of message buffer (0-based)
   API_RT_MBUF_READ * apimbuf) // (o) User's buffer to receive data
{
    	WT_API_RT_MBUF pmbuf;
		__int64   tag;

	unsigned short int cmd1, cmd2;

		BT_UINT rv =WT_1553B_RT_MessageRead(cardnum,rtaddr,subaddr,tr,mbuf_id, &pmbuf);
    	if(rv==WT_SUCCESS)
		{
	    	apimbuf->mess_command.rtaddr =(pmbuf.mess_command>>11)&0x1f;
            apimbuf->mess_command.subaddr =(pmbuf.mess_command>>5)&0x1f;
            apimbuf->mess_command.tran_rec =(pmbuf.mess_command>>10)&1;
            apimbuf->mess_command.wcount =pmbuf.mess_command&0x1f;
               
               cmd1 = pmbuf.spare & 0xffff;
               cmd2 = (pmbuf.spare >> 16) & 0xffff;
               if(((cmd1 >> 10) & 1) == apimbuf->mess_command.tran_rec)
               		{
               			apimbuf->spare = cmd2;
               		}
               	else
               		{
               			apimbuf->spare = cmd1;
               		}       
            
            apimbuf->mess_status.busy =(pmbuf.mess_status>>3)&1;   
            apimbuf->mess_status.bcr=(pmbuf.mess_status>>4)&1;   
            apimbuf->mess_status.dba  =(pmbuf.mess_status>>1)&1;   
            apimbuf->mess_status.me =(pmbuf.mess_status>>10)&1;   
            apimbuf->mess_status.rtaddr =(pmbuf.mess_status>>11)&0x1f;   
            apimbuf->mess_status.res =(pmbuf.mess_status>>5)&7;   
            apimbuf->mess_status.sf  =(pmbuf.mess_status>>2)&1;   
            apimbuf->mess_status.sr =(pmbuf.mess_status>>8)&1;
            apimbuf->mess_status.inst =(pmbuf.mess_status>>9)&1;
            apimbuf->mess_status.tf =pmbuf.mess_status&1; 
            for(int k=0;k<32;k++)
                apimbuf->mess_data[k]= pmbuf.mess_data[k]; 
			
			tag=(__int64)pmbuf.time_H;
            tag=((tag<<32)&0XFFFFFFFF00000000LL)+(__int64)pmbuf.time_L;
		    //tag=pmbuf.time;
			float	floattag;
	    	floattag=(float)(tag>>3);
	    	floattag=floattag/3;
	    	tag=(__int64)floattag;
    		apimbuf->time.microseconds=(BT_U32BIT)((tag)&0xffffffff);
	    	apimbuf->time.topuseconds=(BT_U16BIT)((tag>>32)&0xffff);
            apimbuf->status=pmbuf.status;//BT1553_INT_END_OF_MESS; 

			if (apimbuf->mess_command.subaddr == 0)
			{
				apimbuf->status |= BT1553_INT_MODE_CODE;
			}

			if (apimbuf->mess_command.rtaddr == 31)
			{
				apimbuf->status |= BT1553_INT_BROADCAST;
			}

	        return API_SUCCESS;
		}else
		{
            return API_BUSTOOLS_BADCARDNUM;
		};
}

NOMANGLE BT_INT CCONV BusTools_RT_MessageWrite(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddr,            // (i) RT Terminal Address
   BT_UINT subaddr,           // (i) Subaddress
   BT_UINT tr,                // (i) Transmit=1, Receive=0
   BT_UINT mbuf_id,           // (i) RT Message Buffer number
   API_RT_MBUF_WRITE * apimbuf)  // (i) Pointer to user's RT mbuf structure
{
	struct WT_API_RT_MBUF rt_mbuf;
	
	rt_mbuf.enable = apimbuf->enable;
	rt_mbuf.error_inj_id = apimbuf->error_inj_id;
	memcpy(rt_mbuf.mess_data,apimbuf->mess_data,32*sizeof(unsigned short int));
	
	WT_1553B_RT_MessageWrite(cardnum,rtaddr,subaddr,tr,mbuf_id,&rt_mbuf);
	
	return API_SUCCESS;
}


NOMANGLE BT_INT CCONV BusTools_RT_AutoIncrMessageData(BT_INT cardnum,BT_INT rtaddr,BT_INT subaddr,
									                  BT_INT data_wrd,BT_U16BIT start, BT_U16BIT incr, 
                                                      BT_INT rate, BT_U16BIT max, BT_INT sflag)
{
   	if(cardnum==-1)return API_SUCCESS;

	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_MessageWriteStatusWord(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddr,            // (i) RT Terminal Address
   BT_UINT subaddr,           // (i) Subaddress
   BT_UINT tr,                // (i) Transmit=1, Receive=0
   BT_UINT mbuf_id,           // (i) RT Message Buffer number
   BT_U16BIT wStatusWord,     // (i) 1553 RT Status word
   BT_UINT wFlag)             // (i) Flag=RT_SET or RT_NOCHANGE
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_MessageWriteDef(
   BT_UINT cardnum,           // (i) card number
   BT_UINT rtaddr,            // (i) RT Terminal Address
   API_RT_MBUF_WRITE * apimbuf)  // (i) Pointer to user's RT Message Buffer
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_StartStop(
   BT_UINT cardnum,           // (i) card number
   BT_UINT startflag)         // (i) Flag=1 to start the RT, =0 to stop it
{
	if(startflag==1)
    {
		WT_1553B_RT_Start(cardnum);
	}else
    {
		WT_1553B_RT_Stop(cardnum); 
	};
    return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_GetRTAddr(
    BT_UINT cardnum,          // (i) card number
    BT_INT *rtaddr)           // (o) hardwire RT address lines value
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_ReadNextMessage(int cardnum,BT_UINT timeout,BT_INT rt_addr,
									   BT_INT subaddress,BT_INT tr, API_RT_MBUF_READ *pRT_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_ReadLastMessage(int cardnum,BT_INT rt_addr,
									   BT_INT subaddress,BT_INT tr, API_RT_MBUF_READ *pRT_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_ReadLastMessageBlock(int cardnum,BT_INT rt_addr_mask,
									   BT_INT subaddr_mask,BT_INT tr, BT_UINT *mcount,API_RT_MBUF_READ *pRT_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_RT_Checksum1760(API_RT_MBUF_WRITE *mbuf, BT_U16BIT *cksum, int wdcnt)
{
	return API_SUCCESS;
}


/*
NOMANGLE BT_INT CCONV BusTools_RT_SetVectorWord(UINT cardnum,UINT rtaddr,unsigned short VectorWord)
{
	return WT_1553B_RT_SetVectorWord(cardnum,rtaddr,VectorWord);
}

NOMANGLE BT_INT CCONV BusTools_RT_GetVectorWord(UINT cardnum,UINT rtaddr,unsigned short *VectorWord)
{
	return WT_1553B_RT_GetVectorWord(cardnum,rtaddr,VectorWord);
}

NOMANGLE BT_INT CCONV BusTools_RT_SetBITWord(UINT cardnum,UINT rtaddr,unsigned short BitWord)
{
	return WT_1553B_RT_SetBITWord(cardnum,rtaddr,BitWord);
}

NOMANGLE BT_INT CCONV BusTools_RT_SetTransmitData(UINT cardnum,UINT rtaddr,UINT subaddr, UINT DataLen, unsigned short Data[32])
{
	return WT_1553B_RT_SetTransmitData(cardnum,rtaddr,subaddr,DataLen,Data);
}

NOMANGLE BT_INT CCONV BusTools_RT_UpdateTransmitData(UINT cardnum,UINT rtaddr,UINT subaddr, UINT DataLen, unsigned short Data[32])
{
	return WT_1553B_RT_UpdateTransmitData(cardnum,rtaddr,subaddr,DataLen,Data);
}
*/
