#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <taskLib.h>
#include "SimUdpFunc.h"
/*#include "../coreip/arpa/inet.h"*/


#define SEND_PORT_1	(8602)
#define SEND_PORT_2	(8603)

#define RCV_PORT_1	(8604)
#define RCV_PORT_2	(8605)

#define SEND_IP		"192.168.0.114"
#define RCV_IP		"192.168.0.88"

#define MESS_COUNT	(4)
#define RCV_BUF_SIZE	(40)
#define SEND_BUF_SIZE	(100)


int 			g_iTimerTaskId;
unsigned int		g_drtA;
unsigned int		g_drtB;
UDP_MESSAGE_T   	g_udpInf[MESS_COUNT];

void SendDataHandleA(void)
{
	int i;

	for(i = 0; i < SEND_BUF_SIZE; i++)
	{
		g_udpInf[0].buff[i] = i+g_drtA;
	}
	g_drtA++;	
}

void SendDataHandleB(void)
{
	int i;

	for(i = 0; i < SEND_BUF_SIZE; i++)
	{
		g_udpInf[1].buff[i] = i+g_drtB;
	}
	g_drtB++;	
}

void DisplayData(UDP_MESSAGE_T *message)
{
	int i;

	printf("port:%d,len:%d\n",message->port,message->status);
	for(i = 0; i < message->status;i++)
	{
		printf("%d ",message->buff[i]);
		if(0 == (i+1)%8)
		{
			putchar('\n');
		}
	}

}

void TimerHandlerA(timer_t timerId, int arg)
{
	WriteUdpFuncImplement(&g_udpInf[0]);
	SendDataHandleA();
}
void TimerHandlerB(timer_t timerId, int arg)
{
	WriteUdpFuncImplement(&g_udpInf[1]);
	SendDataHandleB();
}

void TimerHandlerC(timer_t timerId, int arg)
{
	ReadUdpFuncImplement(&g_udpInf[2]);
	DisplayData(&g_udpInf[2]);
}

void TimerHandlerD(timer_t timerId, int arg)
{
	ReadUdpFuncImplement(&g_udpInf[3]);
	DisplayData(&g_udpInf[3]);
}


int InitTimer(void)
{
	struct itimerspec timerSpec;
	int iRetval1 = 0,iRetval2 = 0,iRetval3 = 0,iRetval4 = 0;
	timer_t timer1Id = 0, timer2Id = 0,timer3Id = 0,timer4Id = 0;

	iRetval1 = timer_create(CLOCK_REALTIME, NULL, &timer1Id);
	iRetval2 = timer_create(CLOCK_REALTIME, NULL, &timer2Id);
	iRetval3 = timer_create(CLOCK_REALTIME, NULL, &timer3Id);
	iRetval4 = timer_create(CLOCK_REALTIME, NULL, &timer4Id);
	if ((ERROR == iRetval1) ||(ERROR == iRetval2) ||(ERROR == iRetval3) ||(ERROR == iRetval4))
	{
		printf("timer create failed\n");
		return -1;
	}

	iRetval1 = timer_connect(timer1Id, TimerHandlerA, 4567);
	iRetval2 = timer_connect(timer2Id, TimerHandlerB, 789);
	iRetval3 = timer_connect(timer3Id, TimerHandlerC, 1234);
	iRetval4 = timer_connect(timer4Id, TimerHandlerD, 987);
	if ((ERROR == iRetval1) ||(ERROR == iRetval2) ||(ERROR == iRetval3) ||(ERROR == iRetval4))
	{
		printf("timer connect failed\n");
		return -1;
	}

	/*timer1 set 40ms*/
	timerSpec.it_value.tv_sec = 2;
	timerSpec.it_value.tv_nsec = 0;
	timerSpec.it_interval.tv_sec = 0;
	timerSpec.it_interval.tv_nsec = 40 * 1000 * 1000;
	iRetval1 = timer_settime(timer1Id, 0, &timerSpec, NULL);
	if (iRetval1 == ERROR)
	{
		printf("set timer failed\n");
		return -1;
	}

	/*timer2 set 20ms*/
	timerSpec.it_value.tv_sec = 2;
	timerSpec.it_value.tv_nsec = 0;
	timerSpec.it_interval.tv_sec = 0;
	timerSpec.it_interval.tv_nsec = 20 * 1000 * 1000;
	iRetval2 = timer_settime(timer2Id, 0, &timerSpec, NULL);
	if (iRetval2 == ERROR)
	{
		printf("set timer failed\n");
		return -1;
	}

	/*timer3 set 40*/
	timerSpec.it_value.tv_sec = 2;
	timerSpec.it_value.tv_nsec = 0;
	timerSpec.it_interval.tv_sec = 0;
	timerSpec.it_interval.tv_nsec = 40 * 1000 * 1000;
	iRetval3 = timer_settime(timer3Id, 0, &timerSpec, NULL);
	if (iRetval3 == ERROR)
	{
		printf("set timer failed\n");
		return -1;
	}

	/*timer4 set 40ms*/
	timerSpec.it_value.tv_sec = 2;
	timerSpec.it_value.tv_nsec = 0;
	timerSpec.it_interval.tv_sec = 0;
	timerSpec.it_interval.tv_nsec = 40 * 1000 * 1000;
	iRetval4 = timer_settime(timer4Id, 0, &timerSpec, NULL);
	if (iRetval4 == ERROR)
	{
		printf("set timer failed\n");
		return -1;
	}

	return 0;
}


void TimerTask(void)
{
	
	printf("timer task tid = %X\n", taskIdSelf());
	InitTimer();
	while (1)
	{
		taskDelay(60);
	}
}
int TestMain(void)
{
	int i = 0;
 
	g_drtA = 1;
	g_drtB = 10;

	memset(g_udpInf,0,sizeof(g_udpInf));

	InitializeUdp();
	
	/*udp send config*/
	g_udpInf[0].port = SEND_PORT_1;
	g_udpInf[0].ip = inet_addr(SEND_IP);
	g_udpInf[0].buffLength = SEND_BUF_SIZE;
	g_udpInf[0].buff = malloc(SEND_BUF_SIZE);
	
	g_udpInf[1].port = SEND_PORT_2;
	g_udpInf[1].ip = inet_addr(SEND_IP);
	g_udpInf[1].buffLength = SEND_BUF_SIZE;
	g_udpInf[1].buff = malloc(SEND_BUF_SIZE);

	/*udp rcv config*/
	g_udpInf[2].port = RCV_PORT_1;
	g_udpInf[2].ip = 0;
	g_udpInf[2].buffLength = RCV_BUF_SIZE;
	g_udpInf[2].buff = malloc(RCV_BUF_SIZE);
	
	g_udpInf[3].port = RCV_PORT_2;
	g_udpInf[3].ip = 0;
	g_udpInf[3].buffLength = RCV_BUF_SIZE;
	g_udpInf[3].buff = malloc(RCV_BUF_SIZE);

	OpenUdpFuncImplement(&g_udpInf[2]);
	OpenUdpFuncImplement(&g_udpInf[3]);
	
	SendDataHandleA();
	SendDataHandleB();

	g_iTimerTaskId = taskSpawn("TimerOpr", 210, 0, 4096, (FUNCPTR)TimerTask, 0,0,0,0,0,0,0,0,0,0);
	
	return 0;
}
