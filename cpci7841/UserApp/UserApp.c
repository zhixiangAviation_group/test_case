
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "Api7841.h"
#include "Commdef.h"

#define	PATTERNS	(32)

int g_Handle[4];			//using for Port g_Handle
int	g_RcvPatterns[2];
CAN_PACKET g_RcvMsg0[PATTERNS], g_RcvMsg1[PATTERNS],g_SndMsg[PATTERNS];

int CanOpr(void)
{
	int ret;
	int errorFlag = 0;
//	PORT_STRUCT setPort;
	int count, c2, cout1, cout2;    

	if ((g_Handle[0] = CanOpenDriver(0, 0)) == -1)
	{
		printf("\n Open Port 0 Failed!!!\n");
		return -1;
	}
	if ((g_Handle[1] = CanOpenDriver(0, 1)) == -1)
	{
		printf("\n Open Port 1 Failed!!!\n");
		return -1;
	}
	
	if ((g_Handle[2] = CanOpenDriver(1, 0)) == -1)
	{
		printf("\n Open Port 0 Failed!!!\n");
		return -1;
	}
	if ((g_Handle[3] = CanOpenDriver(1, 1)) == -1)
	{
		printf("\n Open Port 1 Failed!!!\n");
		return -1;
	}

	/*CanClearRxBuffer(0);*/
	CanClearTxBuffer(0);
	CanClearRxBuffer(1);
/*	CanClearTxBuffer(1);*/

/* 	setPort.mode     = 0;       //  0 : 11-bit ;  1 : 29-bit CAN network
	setPort.accCode  = 0;	    //	Only ID = accCode can pass the filter
	setPort.accMask  = 0x7ff;   //  Don't care bit
	setPort.baudrate = 0;       //	0: 125kBps; 1:250kBps;	2:500kBps; 3:1MBps

	CanConfigPort(g_Handle[1], &setPort);
	CanConfigPort(g_Handle[0], &setPort);*/

	g_RcvPatterns[0] = 0;
	g_RcvPatterns[1] = 0;

	//Initial send buffer
	for(count = 0; count < PATTERNS; count++)
	{
		g_SndMsg[count].canId = count;
		g_SndMsg[count].rtr = 0; //data frame:0,remote frame:1
		g_SndMsg[count].len = 8;
		for(c2 = 0; c2 < 8; c2++)
		{
			g_SndMsg[count].data[c2] = c2;
		}
	}

	printf("Start Transmit %d messages Through PORT1 and PORT2\n", PATTERNS);
	for(count = 0; count < PATTERNS; count++)
	{
		if((ret = CanSendMsg(g_Handle[0], &g_SndMsg[count])) != ERR_NOERROR)
		{
			printf(" Port0: CanSendMsg failed @ count = %d\n", count);	
		}
	}

/*	for(count = 0; count < PATTERNS; count++)
	{
		if((ret = CanSendMsg(g_Handle[1], &g_SndMsg[count])) != ERR_NOERROR)
		{
			printf(" Port1: CanSendMsg failed @ count = %d\n", count);	
		}
	}*/

	Nsleep(5000);
	cout1 = CanGetRcvCnt(g_Handle[0]);
	cout2 = CanGetRcvCnt(g_Handle[1]);
	printf("Get Receive Message PORT1 %d  PORT2 %d\n", cout1, cout2);

/*	for(count = 0; count < cout1; count++) 
	{
		if(count < PATTERNS)
		{
			CanRcvMsg(g_Handle[0], &g_RcvMsg0[g_RcvPatterns[0]]);
			g_RcvPatterns[0]++;
		}
	}*/

	for(count = 0; count < cout2; count++)
	{
		CanRcvMsg(g_Handle[1], &g_RcvMsg1[g_RcvPatterns[1]]);

		printf("rec:%d",g_RcvPatterns[1]);
		for(c2 = 0; c2 < 8; c2++)
		{
			
			printf("%d ",g_RcvMsg1[g_RcvPatterns[1]].data[c2]);
		}
		printf("\n");
		g_RcvPatterns[1]++;
	}

	/*for(count = 0;count < PATTERNS; count++)
	{
		printf("\r examine %d patterns\n", count+1);
		if(g_RcvMsg0[count].canId != g_SndMsg[count].canId)
		{
			errorFlag = 1;
			break;
		}
		
		if(g_RcvMsg0[count].rtr != g_SndMsg[count].rtr)
		{
			errorFlag = 1;
			break;
		}
		if(g_RcvMsg0[count].len != g_SndMsg[count].len)
		{
			errorFlag = 1;
			break;
		}
		for(c2 = 0;c2 < 8; c2++)
		{
			if(g_RcvMsg0[count].data[c2] != g_SndMsg[count].data[c2])
			{

				errorFlag = 1;
				break;
			}
		}
		
		if(g_RcvMsg1[count].canId != g_SndMsg[count].canId)
		{
			errorFlag = 1;
			break;
		}
		
		if(g_RcvMsg1[count].rtr != g_SndMsg[count].rtr)
		{
			errorFlag = 1;
			break;
		}
		
		if(g_RcvMsg1[count].len != g_SndMsg[count].len)
		{
			errorFlag = 1;
			break;
		}

		for(c2 = 0;c2 < 8; c2++)
		{
			if(g_RcvMsg1[count].data[c2] != g_SndMsg[count].data[c2])
			{
				errorFlag = 1;
				break;
			}
		}
	}
	
	if(errorFlag == 1)
	{
		printf("\ntest failed with following CAN message:\n");
		printf("Send Msg: id=%d\n",(int)g_SndMsg[count].canId);
		printf("Rcv0 Msg: id=%d\n",(int)g_RcvMsg0[count].canId);
		printf("Rcv1 Msg: id=%d\n",(int)g_RcvMsg1[count].canId);
	}
	else
	{
		printf("\ncan test ok!!\n");
	}*/

    CanCloseDriver(g_Handle[0]);
 	CanCloseDriver(g_Handle[1]);
	printf("Press any key to exit the program...\n");

	return 0;
}
