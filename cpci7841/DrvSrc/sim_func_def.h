#ifndef SIM_FUNC_DEF_H
#define SIM_FUNC_DEF_H

#include <vxWorks.h>

#ifdef __cplusplus
extern "C"
{
#endif 

typedef struct UdpTimeIfo
{
	uint64_t	ip;
	uint32_t	port;
	int32_t		socket;
	int32_t		status;
	double      currentTim;
	uint32_t	buffLength;
	uint8_t		*buff;	
}_udpTimeMessage_t;

typedef void (*OpenTimeUdp)(_udpTimeMessage_t * message);
typedef void (*CloseTimeUdp)(_udpTimeMessage_t * message);
typedef void (*WriteTimeUdp)(_udpTimeMessage_t * message);

typedef struct UdpMessage
{
	uint64_t	ip;
	uint32_t	port;
	int32_t		socket;
	int32_t		status;
	uint32_t	buffLength;
	uint8_t		*buff;
}_udpMessage_t;

typedef void (*OpenUdp)(_udpMessage_t * message);
typedef void (*CloseUdp)(_udpMessage_t * message);
typedef void (*WriteUdp)(_udpMessage_t* message);
typedef void (*ReadUdp)(_udpMessage_t* message);


typedef struct RT1553Message
{
	uint32_t	rtid;
	uint32_t	said;
	uint16_t	modle;
	uint16_t	channelId;
	uint32_t	buffLength;
	uint16_t	*buff;
} _rt1553Message_t;

typedef void (*Open1553)(_rt1553Message_t* message); 
typedef void (*Close1553)(_rt1553Message_t* message);
typedef void (*Write1553)(_rt1553Message_t* message); 
typedef void (*Read1553)(_rt1553Message_t* message);

typedef struct Bm1553Message
{	
	double      currentTime;
	uint64_t	ip;
	uint32_t	port;
}_bm1553Message_t;

typedef void (*Open1553Bm)(_bm1553Message_t* message); 
typedef void (*Close1553Bm)();
typedef void (*Read1553Bm)(_bm1553Message_t* message);

typedef struct RS422Message
{
	uint64_t	ip;
	uint32_t	port;
	int32_t		socket;
	int32_t		status;
	uint32_t	buffLength;
	uint8_t		*buff;
} _rs422Message_t;

typedef void (*Open422)(_rs422Message_t *message);
typedef void (*Close422)(_rs422Message_t *message);
typedef void (*Write422)(_rs422Message_t *message);
typedef void (*Read422)(_rs422Message_t *message);

typedef struct DIOMessage
{
	uint8_t		*channelArr;
	uint32_t	sampleRate;
	uint32_t	buffLength;
	uint8_t		*buff;
} _DIOMessage_t;

typedef void (*OpenDIO)(_DIOMessage_t *message);
typedef void (*CloseDIO)();
typedef void (*WriteDIO)(_DIOMessage_t *message);
typedef void (*ReadDIO)(_DIOMessage_t *message);

typedef struct ADDAMessage
{
	uint8_t		*channelArr;
	uint32_t	sampleRate;
	uint32_t	scanRate;
	uint32_t	scanCnt;
	uint32_t	buffLength;
	float		*buff;
} _ADDAMessage_t;

typedef void (*OpenDA)(_ADDAMessage_t  *message);
typedef void (*OpenAD)(_ADDAMessage_t  *message);
typedef void (*CloesDA)();
typedef void (*CloesAD)();
typedef void (*WriteDA)(_ADDAMessage_t  *message);
typedef void (*ReadAD)(_ADDAMessage_t  *message);

typedef struct PWMMessage
{
	uint32_t	frtCounterNum;
	uint32_t	sndCounterNum;
	float		frequency;
	float		pulseWidth;
	uint8_t		pwmId;
} _PWMMessage_t;


typedef void (*OpenPWM)();
typedef void (*ClosePWM)();
typedef void (*WritePWM)(_PWMMessage_t  *message);
typedef void (*ReadPWM)(_PWMMessage_t  *message);

typedef struct Rfm2gMessage
{
	uint32_t  offsetAddress;
	uint32_t  buffLength;
	uint8_t   *buff;
}_Rfm2gMessage_t;

typedef void (*OpenRfm2g)();
typedef void (*CloseRfm2g)();
typedef void (*WriteRfm2g)(_Rfm2gMessage_t  *message);
typedef void (*ReadRfm2g)(_Rfm2gMessage_t  *message);

typedef struct Bc1553Message
{
	uint8_t   messageId;
	uint32_t  buffLength;
	uint8_t   *buff;
}_Bc1553Message_t;

typedef void (*OpenBc1553)();
typedef void (*CloseBc1553)();
typedef void (*WriteBc1553)(_Bc1553Message_t *message);
typedef void (*ReadBc1553)(_Bc1553Message_t *message);

typedef struct ModleInitData
{
	uint32_t  buffLength;
	uint8_t   *buff;
}_ModleInitData_t;

typedef void(* ReadInitData)(_ModleInitData_t *message);

typedef struct CanMessage
{
	uint8_t carNum;
	uint8_t portNum;
	uint8_t messageCnt;
	uint32_t messageId;
	uint32_t  buffLength;
	uint8_t   *buff;
}_CanMessage_t;

typedef void (*OpenCan)();
typedef void (*CloseCan)();
typedef void (*WriteCan)(_CanMessage_t *message);
typedef void (*ReadCan)(_CanMessage_t *message);

typedef struct SimFunc
{
    Open1553	Open1553Func;
    Close1553	Close1553Func;
    Write1553	Write1553Func;
    Read1553	Read1553Func;

	Open1553Bm  OpenBmFunc;
	Close1553Bm CloseBmFunc;
	Read1553Bm  ReadBmFunc;

    Open422		Open422Func;
    Close422	Close422Func;
    Write422	Write422Func;
    Read422		Read422Func;

    OpenDIO		OpenDIOFunc;
    CloseDIO	CloseDIOFunc;
    WriteDIO	WriteDIOFunc;
    ReadDIO		ReadDIOFunc;

    OpenDA		OpenDAFunc;
    OpenAD		OpenADFunc;
    CloesDA		CloesDAFunc;
    CloesAD		CloesADFunc;
    WriteDA		WriteDAFunc;
    ReadAD		ReadADFunc;

    OpenPWM		OpenPWMFunc;
    ClosePWM	ClosePWMFunc;
    WritePWM	WritePWMFunc;
    ReadPWM		ReadPWMFunc;

    OpenUdp		OpenUdpFunc;
    CloseUdp	CloseUdpFunc;
    WriteUdp	WriteUdpFunc;
    ReadUdp		ReadUdpFunc; 
    
    OpenRfm2g   OpenRfm2gFunc;
    CloseRfm2g  CloseRfm2gFunc;
    WriteRfm2g  WriteRfm2gFunc;
    ReadRfm2g   ReadRfm2gFunc;
    
    OpenBc1553  OpenBcFunc;
    CloseBc1553 CloseBcFunc;
    WriteBc1553 WriteBCFunc;
    ReadBc1553  ReadBCFunc;
	
	/*带时间参数的udp信息*/
	OpenTimeUdp  OpenTimeUdpFunc;
	CloseTimeUdp CloseTimeUdpFunc;
	WriteTimeUdp WriteTimeUdpFunc;
    
    ReadInitData ReadModleDataFunc;
    
    OpenCan   OpenCanFunc;
    CloseCan  CloseCanFunc;
    WriteCan  WriteCanFunc;
    ReadCan   ReadCanFunc;
} _simFunc_t;

#ifdef __cplusplus
}
#endif 

#endif

