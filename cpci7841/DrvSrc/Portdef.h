#ifndef PORTING_DEF_H
#define PORTING_DEF_H

#include <spinLockLib.h>

#define	KERN_NOTICE
#define	KERN_ALERT

#define	GFP_ATOMIC		(0) 
#define	GFP_KERNEL		(0)
#define IRQ_NONE		(-1)
#define IRQ_HANDLED		(0)

#define printk printf

typedef int irqreturn_t;
typedef spinlockIsr_t spinlock_t;

uint8_t Inbyte(uint16_t p);
void kfree(void *mem_ptr);
void Outbyte(uint8_t v, uint16_t p);
void SpinLockInit(spinlockIsr_t *pLock);
void *kmalloc(unsigned int mem_size, int not_used);
void SpinLockIrqsave(spinlockIsr_t *pLock, int flags);
void SpinUnlockIrqrestore(spinlockIsr_t *pLock, int flags);


unsigned long copy_from_user(void *dst, const void *src, size_t mem_size);
unsigned long copy_to_user(void *dst, const void *src, size_t mem_size);

typedef irqreturn_t (*irq_handler_t)(int);
int RequestIrq(unsigned int irq, irq_handler_t handler,int boardNo);
void FreeIrq(unsigned int irqNo, void *param);

#endif
