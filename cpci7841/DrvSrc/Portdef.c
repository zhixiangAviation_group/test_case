
#include <vxWorks.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysLib.h>
#include <ioLib.h>
#include <taskLib.h>
#include <math.h>
#include <string.h>
#include <logLib.h>
#include <intLib.h>
#include <iv.h>
#include "drv/pci/pciConfigLib.h"
#include "drv/pci/pciIntLib.h"
#include "Portdef.h"
#include <pci.h>

void *kmalloc(unsigned int mem_size, int not_used)
{
	return malloc(mem_size);
}

void kfree(void *mem_ptr)
{
	free(mem_ptr);
}

void SpinUnlockIrqrestore(spinlockIsr_t *pLock, int flags)
{
	spinLockIsrGive(pLock);
}

void SpinLockIrqsave(spinlockIsr_t *pLock, int flags)
{
	spinLockIsrTake(pLock); 
}

void SpinLockInit(spinlockIsr_t *pLock)
{
	spinLockIsrInit(pLock, 0);
}

uint8_t Inbyte(uint16_t p)
{
	return sysInByte(p);
}

void Outbyte(uint8_t v, uint16_t p)
{
	sysOutByte(p, v);
}
unsigned long copy_from_user(void *dst, const void *src, size_t mem_size)
{
	memcpy(dst, src, mem_size);
	return 0;
}
unsigned long copy_to_user(void *dst, const void *src, size_t mem_size)
{
	memcpy(dst, src, mem_size);
	return 0;
}

int RequestIrq(unsigned int irq, irq_handler_t handler,int boardNo)
{
	if(OK == pciIntConnect(INUM_TO_IVEC(0x20 + irq), (VOIDFUNCPTR)handler, boardNo)) 
	{
		return OK;	
	}
	else
	{
		return ERROR;
	}
}
void FreeIrq(unsigned int irqNo, void *param)
{
    //pciIntDisconnect(INUM_TO_IVEC(INT_NUM_GET(irq)), (VOIDFUNCPTR)handler);
}
