#ifndef _ISR_DPC_H_
#define _ISR_DPC_H_

#include "Portdef.h"

irqreturn_t Cpci7841InterruptHandler(int boardNo);

#endif
