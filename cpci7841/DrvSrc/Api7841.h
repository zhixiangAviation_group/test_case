#ifndef PCI_7841_H
#define PCI_7841_H

#include "Commdef.h"

#define	MAXSUPPORT					(4)
#define	PCI_INIT_OK					(0x00)
#define	EEPROM_WR_OK				(0x00)
#define	EEPROM_WR_ERR				(-3)
#define	PCI_INIT_ERROR				(-1)
#define	PCI_INIT_NotFound			(-2)

//error infor
#define	ERR_NOERROR					(0)
#define	ERR_INVALIDCARDNUMBER		(-1)
#define	ERR_OPENFILEFAILED			(-2)
#define ERR_OPENEVENTFAILED			(-3)
#define ERR_BOARDNOINIT				(-4)
#define ERR_ALLOCATEMEMORY			(-5)
#define ERR_OWNERSET				(-6)
#define	ERR_SIGNALHANDLE			(-7)
#define	ERR_SIGNALNOTIFY			(-8)
#define	ERR_INPUTAXISERROR			(-9)
#define	ERR_SETVELERROR				(-10)
#define ERR_CLRPLSERROR				(-11)
#define ERR_RUNMODE					(-12)
#define ERR_MOVEERROR				(-13)

//Define for can port struct
typedef struct
{
	int mode;				//  0 for 11-bit;1 for 29-bit
	DWORD accCode, accMask;
	int baudrate;			//	0 : 125KBps, 1 : 250KBps, 2 : 500KBps, 3 : 1MBps,4 : Self-Defined
	BYTE brp, tseg1, tseg2;	//	Used only if baudrate = 4
	BYTE sjw, sam;          //  Used only if baudrate = 4
}PORT_STRUCT;

// Define CAN status register
typedef struct
{
    unsigned char rxBuffer      : 1;
    unsigned char dataOverrun   : 1;
    unsigned char txBuffer      : 1;
    unsigned char txEnd         : 1;
    unsigned char rxStatus      : 1;
    unsigned char txStatus      : 1;
    unsigned char errorStatus   : 1;
    unsigned char busStatus     : 1;
    //unsigned short reserved      : 8;
} PORTREG_BIT;

typedef union 
{
	PORTREG_BIT bit;
	unsigned char reg;
}PORT_REG;

typedef struct 
{
	PORT_STRUCT port;
	PORT_REG status;
}PORT_STATUS;

void Nsleep(unsigned long ns);
WORD  GetDriverVersion(void);
int  CanGetRcvCnt(int handle);
int  CanCloseDriver(int handle);
int  CanStopReceiveEvent(int handle);
int  CanOpenDriver(int card, int port);
int  CanDetectBaudrate(int handle, int miliSecs);
int  CanConfigPort(int handle, PORT_STRUCT *PortStruct);
int  CanGetPortStatus(int handle, PORT_STATUS *PortStruct);
int  CanGetReceiveEvent(int handle,void (*event_handler)(int));

int  CanStopReceiveWakeUpEvent(int handle);
int  CanStopReceiveBusErroreEvent(int handle);
int  CanStopReceiveDataOverrunEvent(int handle);
int  CanStopReceiveErrorPassiveEvent(int handle);
int  CanStopReceiveErrorWarningEvent(int handle);
int  CanStopReceiveArbitrationLostEvent(int handle);
int  CanGetReceiveWakeUpEvent(int handle,void (*event_handler)(int));
int  CanGetReceiveBusErrorEvent(int handle,void (*event_handler)(int));
int  CanGetReceiveErrorWarningEvent(int handle,void (*event_handler)(int));
int  CanGetReceiveDataOverrunEvent(int handle,void (*event_handler)(int));
int  CanGetReceiveErrorPassiveEvent(int handle,void (*event_handler)(int));
int  CanGetReceiveArbitrationLostEvent(int handle,void (*event_handler)(int));


BYTE  Read7841(int handle, int offset);
void  Write7841(int handle, int offset, BYTE data);
//use for cPCI-7841
BYTE   CanGetLedStatus(int handle, int index);
void  CanSetLedStatus(int handle, int index, int mode);

//CAN layer functions
void  CanEnableReceive(int handle);
void  CanDisableReceive(int handle);
int  CanSendMsg(int handle, CAN_PACKET *packet);
int  CanRcvMsg(int handle, CAN_PACKET *packet);

//CAN layer status control
BYTE  CanGetErrorCode(int handle);
void  CanClearOverrun(int handle);
void  CanClearRxBuffer(int handle);
void  CanClearTxBuffer(int handle);
BYTE  CanGetRxErrorCount(int handle);
BYTE  CanGetTxErrorCount(int handle);
BYTE  CanGetIntStatusReg(int handle);

BYTE  CanGetErrorWarningLimit(int handle);
BYTE  CanGetArbitrationLostBit(int handle);
void  CanSetTxErrorCount(int handle, BYTE value);
void  CanSetErrorWarningLimit(int handle, BYTE limit);
int   CanGetPortStatus(int handle, PORT_STATUS *PortStatus);

#endif

