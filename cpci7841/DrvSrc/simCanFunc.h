#ifndef SIM_CAN_FUNC_H
#define SIM_CAN_FUNC_H

#include "sim_func_def.h"

void ReleaseCan(void);
void InitializeCan(void);
void OpenCanFuncImplement(_CanMessage_t *message);
void CloseCanFuncImplement(_CanMessage_t *message);
void WriteCanFuncImplement(_CanMessage_t *message);
void ReadCanFuncImplement(_CanMessage_t *message);

#endif
