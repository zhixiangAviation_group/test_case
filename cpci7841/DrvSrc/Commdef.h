#ifndef	_COMMDEF_H_
#define	_COMMDEF_H_

#include <sys/ioctl.h>

/*************************************/
/*      Constant Definitions         */
/*************************************/
#define	PCI_7841	(0x7841)
#define	PCL_CS0		(0x00)
#define	PCL_CS1		(0x80)

/*************************************/
/*      Typedef  Definitions         */
/*************************************/
typedef unsigned char   U8;
typedef short           I16;
typedef unsigned short  U16;
typedef long            I32;
typedef unsigned long   U32;
typedef float           F32;
typedef double          F64;
typedef int 			BOOLEAN;
typedef unsigned long   DWORD;
typedef unsigned short  WORD;
typedef unsigned char   BYTE;
typedef unsigned char   UCHAR;
typedef unsigned long   ULONG;
typedef long   			LONG;
typedef unsigned short  USHORT;

#define	MAX_PCI_CARDS	(4)
#define	SIGEVENT1		(63)
#define	SIGEVENT2		(62)

#define FIRSTBYTE(VALUE)	(VALUE & 0x00FF)
#define SECONDBYTE(VALUE)	((VALUE >> 8) & 0x00FF)
#define THIRDBYTE(VALUE)	((VALUE >> 8) & 0x00FF)
#define FOURTHBYTE(VALUE)	((VALUE >> 8) & 0x00FF)

#define MAGIC_NUM 'S'
#define IOCTL_7841_SIGNAL1_SET _IO(MAGIC_NUM, 5)
#define IOCTL_7841_SIGNAL2_SET _IO(MAGIC_NUM, 6)
#define IOCTL_7841_CLOSE_PORT _IOW(MAGIC_NUM, 19, REG_RW)
#define IOCTL_7841_CLOSE_CARD _IOW(MAGIC_NUM, 20, REG_RW)
#define IOCTL_7841_SET_PORT_MODE _IOW(MAGIC_NUM, 21, RETINFO)
#define IOCTL_7841_REG_READ _IOR(MAGIC_NUM, 22, REG_RW)
#define IOCTL_7841_REG_WRITE _IOW(MAGIC_NUM, 23, REG_RW)
#define IOCTL_7841_SET_BUFFER_DATA _IOW(MAGIC_NUM, 24, CAN_MSG)
#define IOCTL_7841_GET_BUFFER_DATA _IOR(MAGIC_NUM, 25, CAN_MSG)
#define IOCTL_7841_GET_UNRD_COUNT _IOR(MAGIC_NUM, 26, REC_CN)
#define IOCTL_7841_CLEAR_RX_BUFFER _IOW(MAGIC_NUM, 27, int)
#define IOCTL_7841_CLEAR_TX_BUFFER _IOW(MAGIC_NUM, 28, int)

//  Define can packet struct
typedef struct 
{
    DWORD canId;        //  CAN id
    BYTE rtr;           //  RTR bit
    BYTE len;           //  Data length
    BYTE data[8];       //  Data
    DWORD time;         //  Occur time (non use)
    BYTE reserved;      //  future use
}CAN_PACKET;

typedef struct
{
  U16 retVal;
  int offset;
  BYTE data;
} REG_RW;

typedef struct
{
  DWORD handle;
  DWORD mode;
} RETINFO;

typedef struct
{
  DWORD handle;
  U16 retVal;
  CAN_PACKET msg;
} CAN_MSG;

typedef struct
{
  DWORD handle;
  U16 retVal;
} REC_CN;


#endif








