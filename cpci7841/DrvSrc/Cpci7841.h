#ifndef _CPCI_7841_
#define _CPCI_7841_

#include "Commdef.h"
#include "Portdef.h"


#define	PCI_PORT_MAX		(4)
#define	FIFOSIZE			(128)

#define	ENABLE				(1)
#define	DISABLE				(0)

#define	HIBYTE(ax)			(ax >> 8)
#define	LOBYTE(ax)			(ax & 0x00ff)
#define	HIWORD(ax)			(ax >> 16)
#define	LOWORD(ax)			(ax & 0x0000ffff)

// Constant for structures
// Port report from PCI bios

#define	PCI_CTRL_S593		(0)
#define	PCI_CTRL_PLX90		(1)
#define	PCI_CTRL_P9080		(2)
#define	PCI_CTRL_HSL_P9050	(3)

#define	ADL_OP_LAS0RR		(0x00)
#define	ADL_OP_LAS1RR		(0x04)
#define	ADL_OP_LAS2RR		(0x08)
#define	ADL_OP_LAS3RR		(0x0C)
#define	ADL_OP_EROMRR		(0x10)
#define	ADL_OP_LAS0BA		(0x14)
#define	ADL_OP_LAS1BA		(0x18)
#define	ADL_OP_LAS2BA		(0x1C)
#define	ADL_OP_LAS3BA		(0x20)
#define	ADL_OP_EROMBA		(0x24)
#define	ADL_OP_LAS0BRD		(0x28)
#define	ADL_OP_LAS1BRD		(0x2C)
#define	ADL_OP_LAS2BRD		(0x30)
#define	ADL_OP_LAS3BRD		(0x34)
#define	ADL_OP_EROMBRD		(0x38)       
#define	ADL_OP_CS0BASE		(0x3C)
#define	ADL_OP_CS1BASE		(0x40)
#define	ADL_OP_CS2BASE		(0x44)
#define	ADL_OP_CS3BASE		(0x48)
#define	ADL_OP_LINTCSR		(0x4C)
#define	ADL_OP_CNTRL		(0x50)
#define	ADL_OP_EEPROM		(0x53)
#define	ADL_PT_CLRIRQ0		(0x80)
#define	ADL_PT_CLRIRQ1		(0x90)

/***********************************************/
/*   PCL_5022 Register Structure Type Define   */
/***********************************************/
typedef struct  cmddt 
{
	unsigned int  strcmd;  /* Start Command */
	unsigned int  stpcmd;  /* Stop Command */
	unsigned int  schgcmd; /* Speed Change Command */
	unsigned int  srescmd;
	unsigned int  outbit;
	unsigned int  concmd;
}CMD_TBL;

typedef struct  workunit 
{
	unsigned int  runadr;
	unsigned int  i_oadr;
	unsigned int  bitadr;
	unsigned int  regadr;
	unsigned int  i_obadr0;
	unsigned int  i_obadr1;
}PCL_ADR;

typedef struct
{
	U16    wCard;
	U16    initFlag;
	U16    busNo;
	U16    devFunc;
	U32    address0, address1;
	U32    size0, size1;
	U16    irqNo;
	BOOLEAN isrEnable;
	U16    reference;
	struct fasync_struct *async_queue1,  *async_queue2;
	
	unsigned char recFlag[2];
	unsigned char sendFlag[2];
	unsigned char errorFlag[2];
	unsigned char overrunFlag[2];
	unsigned char wakeupFlag[2];
	unsigned char errorPassiveFlag[2];
	unsigned char arbitrationLostFlag[2];
	unsigned char busErrorFlag[2];
	
	CAN_PACKET rBuf[2][FIFOSIZE];
	int rPtrL[2];
	int rPtrU[2];
	CAN_PACKET sBuf[2][FIFOSIZE];
	unsigned char sPtrL[2];
	unsigned char sPtrU[2];
	unsigned char sCnt[2];
	unsigned char portMode[2];
	
	spinlock_t rlock[2];
} PCI_Info;

void adl_kill_fasync(struct fasync_struct *fa, int sig);
void clear_fasync_queue( struct fasync_struct ** ppfa );
void K_clear_irq( PCI_Info* ppci_info, U8 axis_n );

int OpenCpci7841(int boardNo);
int CloseCpci7841(int boardNo);
void ConfigCanPort(int handle, int baudrate);
void SndMsg(PCI_Info* pDecExt, int port, CAN_MSG* temp_param);
void RevMsg(PCI_Info* pDecExt, int port, CAN_MSG* temp_param);
int Can7841Ioctl(int boardNo, unsigned int ioctlNum, unsigned long ioctlParam);

#endif
