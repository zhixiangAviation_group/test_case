#ifndef _SIM_UDP_FUNC_
#define _SIM_UDP_FUNC_

typedef struct UdpMessage
{
	uint64_t	ip;
	uint32_t	port;
	int32_t		socket;
	int32_t		status;
	uint32_t	buffLength;
	uint8_t		*buff;
}UDP_MESSAGE_T;

void ReleaseUdp(void);

void InitializeUdp(void);

void CloseUdpFuncImplement(UDP_MESSAGE_T *message);

void OpenUdpFuncImplement(UDP_MESSAGE_T *message);

void WriteUdpFuncImplement(UDP_MESSAGE_T *message);

void ReadUdpFuncImplement(UDP_MESSAGE_T *message);

#endif

