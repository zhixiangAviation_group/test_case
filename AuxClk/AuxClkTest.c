#include <stdio.h>
#include <vxWorks.h>
#include <sysLib.h>
#include <logLib.h>
#include <timers.h>
#include <taskLib.h>
#include "SimUdpFunc.h"

#define SEND_PORTA	(9870)
#define SEND_IP_ADDR	"192.168.0.114"

#define SEND_BUF_SIZE	(100)


int g_tskIdA = 0;
UDP_MESSAGE_T g_sendMess;

void TskScheduler(void)
{
	static UINT32  schTicks = 0; 
	
	if(0 == schTicks % 20)
	{	
		taskResume(g_tskIdA);
	
	}
	
	schTicks++;	
}

void SendMesConfig(UDP_MESSAGE_T *mes)
{
	int i;
	
	mes->port = SEND_PORTA;
	mes->ip = inet_addr(SEND_IP_ADDR);
	mes->buffLength = SEND_BUF_SIZE;
	mes->buff = malloc(SEND_BUF_SIZE);

	for(i = 0; i < SEND_BUF_SIZE;i++)
	{
		mes->buff[i] = i+10;		
	}
}

void UdpSendTask(void)
{
	while(1)
	{
		taskSuspend(g_tskIdA);
		WriteUdpFuncImplement(&g_sendMess);
		
	}
}

int MyMain(void)
{ 	
	
	InitializeUdp();

	sysAuxClkDisable();
	
	sysAuxClkRateSet(1024); 
	
	if(sysAuxClkConnect((FUNCPTR)TskScheduler, 0) == OK)
	{
		sysAuxClkEnable();
	}
	else
	{
		
		printf("sysAuxClkConnect failed!!!\n");
	}



	SendMesConfig(&g_sendMess);

	if(ERROR == (g_tskIdA = taskSpawn("UdpTsk",100,0,20480,(FUNCPTR)UdpSendTask,0,0,0,0,0,0,0,0,0,0)))
	{
		printf("creat UdpTsk failed!\n");
		reboot(2);
	}

	return 0; 
}


