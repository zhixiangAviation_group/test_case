#include <stdio.h>
#include <vxWorks.h>
#include <sysLib.h>
#include <logLib.h>
#include <timers.h>

void TskScheduler(void)
{
	static UINT32  schTicks = 0; 
	
	if(0 == schTicks % 200)
	{	
		logMsg("enter interrupt\n",0,0,0,0,0,0);
	}
	
	schTicks++;	
}

int MyMain(void)
{ 	

	sysAuxClkDisable();
	
	sysAuxClkRateSet(1024); 
	
	if(sysAuxClkConnect((FUNCPTR)TskScheduler, 0) == OK)
	{
		sysAuxClkEnable();
	}
	else
	{
		
		printf("333333333333333333error:%d\n",errno);
	}

	return 0; 
}


