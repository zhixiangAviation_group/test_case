/*
 * Rfm2gWrite_types.h
 *
 * Code generation for model "Rfm2gWrite".
 *
 * Model version              : 1.7
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 15:42:53 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_Rfm2gWrite_types_h_
#define RTW_HEADER_Rfm2gWrite_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_Rfm2gWrite_T_ P_Rfm2gWrite_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Rfm2gWrite_T RT_MODEL_Rfm2gWrite_T;

#endif                                 /* RTW_HEADER_Rfm2gWrite_types_h_ */
