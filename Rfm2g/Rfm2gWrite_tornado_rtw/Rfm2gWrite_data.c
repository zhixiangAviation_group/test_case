/*
 * Rfm2gWrite_data.c
 *
 * Code generation for model "Rfm2gWrite".
 *
 * Model version              : 1.7
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 15:42:53 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "Rfm2gWrite.h"
#include "Rfm2gWrite_private.h"

/* Block parameters (auto storage) */
P_Rfm2gWrite_T Rfm2gWrite_P = {
  /*  Computed Parameter: Read_Udp_P1_Size
   * Referenced by: '<Root>/Read_Udp'
   */
  { 1.0, 1.0 },
  30.0,                                /* Expression: dataLen
                                        * Referenced by: '<Root>/Read_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P2_Size
   * Referenced by: '<Root>/Read_Udp'
   */
  { 1.0, 1.0 },
  5858.0,                              /* Expression: locPort
                                        * Referenced by: '<Root>/Read_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P3_Size
   * Referenced by: '<Root>/Read_Udp'
   */
  { 1.0, 1.0 },
  -1.0,                                /* Expression: priod
                                        * Referenced by: '<Root>/Read_Udp'
                                        */

  /*  Computed Parameter: Write_Rfm2g_P1_Size
   * Referenced by: '<Root>/Write_Rfm2g'
   */
  { 1.0, 1.0 },
  0.0                                  /* Expression: offset
                                        * Referenced by: '<Root>/Write_Rfm2g'
                                        */
};
