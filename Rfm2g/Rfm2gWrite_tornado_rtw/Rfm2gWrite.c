/*
 * Rfm2gWrite.c
 *
 * Code generation for model "Rfm2gWrite".
 *
 * Model version              : 1.7
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 15:42:53 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "Rfm2gWrite.h"
#include "Rfm2gWrite_private.h"

/* Block signals (auto storage) */
B_Rfm2gWrite_T Rfm2gWrite_B;

/* Block states (auto storage) */
DW_Rfm2gWrite_T Rfm2gWrite_DW;

/* Real-time model */
RT_MODEL_Rfm2gWrite_T Rfm2gWrite_M_;
RT_MODEL_Rfm2gWrite_T *const Rfm2gWrite_M = &Rfm2gWrite_M_;

/* Model output function */
static void Rfm2gWrite_output(void)
{
  /* Level2 S-Function Block: '<Root>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = Rfm2gWrite_M->childSfunctions[0];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<Root>/Write_Rfm2g' (simWriteRfm2g) */
  {
    SimStruct *rts = Rfm2gWrite_M->childSfunctions[1];
    sfcnOutputs(rts, 0);
  }
}

/* Model update function */
static void Rfm2gWrite_update(void)
{
  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++Rfm2gWrite_M->Timing.clockTick0)) {
    ++Rfm2gWrite_M->Timing.clockTickH0;
  }

  Rfm2gWrite_M->Timing.t[0] = Rfm2gWrite_M->Timing.clockTick0 *
    Rfm2gWrite_M->Timing.stepSize0 + Rfm2gWrite_M->Timing.clockTickH0 *
    Rfm2gWrite_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void Rfm2gWrite_initialize(void)
{
  /* Level2 S-Function Block: '<Root>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = Rfm2gWrite_M->childSfunctions[0];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<Root>/Write_Rfm2g' (simWriteRfm2g) */
  {
    SimStruct *rts = Rfm2gWrite_M->childSfunctions[1];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }
}

/* Model terminate function */
void Rfm2gWrite_terminate(void)
{
  /* Level2 S-Function Block: '<Root>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = Rfm2gWrite_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<Root>/Write_Rfm2g' (simWriteRfm2g) */
  {
    SimStruct *rts = Rfm2gWrite_M->childSfunctions[1];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  Rfm2gWrite_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  Rfm2gWrite_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  Rfm2gWrite_initialize();
}

void MdlTerminate(void)
{
  Rfm2gWrite_terminate();
}

/* Registration function */
RT_MODEL_Rfm2gWrite_T *Rfm2gWrite(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Rfm2gWrite_M, 0,
                sizeof(RT_MODEL_Rfm2gWrite_T));
  rtsiSetSolverName(&Rfm2gWrite_M->solverInfo,"FixedStepDiscrete");
  Rfm2gWrite_M->solverInfoPtr = (&Rfm2gWrite_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = Rfm2gWrite_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    Rfm2gWrite_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    Rfm2gWrite_M->Timing.sampleTimes = (&Rfm2gWrite_M->Timing.sampleTimesArray[0]);
    Rfm2gWrite_M->Timing.offsetTimes = (&Rfm2gWrite_M->Timing.offsetTimesArray[0]);

    /* task periods */
    Rfm2gWrite_M->Timing.sampleTimes[0] = (0.002);

    /* task offsets */
    Rfm2gWrite_M->Timing.offsetTimes[0] = (0.0);
  }

  rtmSetTPtr(Rfm2gWrite_M, &Rfm2gWrite_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = Rfm2gWrite_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    Rfm2gWrite_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(Rfm2gWrite_M, -1);
  Rfm2gWrite_M->Timing.stepSize0 = 0.002;
  Rfm2gWrite_M->solverInfoPtr = (&Rfm2gWrite_M->solverInfo);
  Rfm2gWrite_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&Rfm2gWrite_M->solverInfo, 0.002);
  rtsiSetSolverMode(&Rfm2gWrite_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  Rfm2gWrite_M->ModelData.blockIO = ((void *) &Rfm2gWrite_B);
  (void) memset(((void *) &Rfm2gWrite_B), 0,
                sizeof(B_Rfm2gWrite_T));

  /* parameters */
  Rfm2gWrite_M->ModelData.defaultParam = ((real_T *)&Rfm2gWrite_P);

  /* states (dwork) */
  Rfm2gWrite_M->ModelData.dwork = ((void *) &Rfm2gWrite_DW);
  (void) memset((void *)&Rfm2gWrite_DW, 0,
                sizeof(DW_Rfm2gWrite_T));

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &Rfm2gWrite_M->NonInlinedSFcns.sfcnInfo;
    Rfm2gWrite_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(Rfm2gWrite_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &Rfm2gWrite_M->Sizes.numSampTimes);
    Rfm2gWrite_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(Rfm2gWrite_M)[0]);
    rtssSetTPtrPtr(sfcnInfo,Rfm2gWrite_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(Rfm2gWrite_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(Rfm2gWrite_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(Rfm2gWrite_M));
    rtssSetStepSizePtr(sfcnInfo, &Rfm2gWrite_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(Rfm2gWrite_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &Rfm2gWrite_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo,
      &Rfm2gWrite_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &Rfm2gWrite_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &Rfm2gWrite_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo,
      &Rfm2gWrite_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &Rfm2gWrite_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &Rfm2gWrite_M->solverInfoPtr);
  }

  Rfm2gWrite_M->Sizes.numSFcns = (2);

  /* register each child */
  {
    (void) memset((void *)&Rfm2gWrite_M->NonInlinedSFcns.childSFunctions[0], 0,
                  2*sizeof(SimStruct));
    Rfm2gWrite_M->childSfunctions =
      (&Rfm2gWrite_M->NonInlinedSFcns.childSFunctionPtrs[0]);
    Rfm2gWrite_M->childSfunctions[0] =
      (&Rfm2gWrite_M->NonInlinedSFcns.childSFunctions[0]);
    Rfm2gWrite_M->childSfunctions[1] =
      (&Rfm2gWrite_M->NonInlinedSFcns.childSFunctions[1]);

    /* Level2 S-Function Block: Rfm2gWrite/<Root>/Read_Udp (simReadUdp) */
    {
      SimStruct *rts = Rfm2gWrite_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &Rfm2gWrite_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, Rfm2gWrite_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &Rfm2gWrite_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &Rfm2gWrite_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &Rfm2gWrite_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 2);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 30);
          ssSetOutputPortSignal(rts, 0, ((uint8_T *) Rfm2gWrite_B.Read_Udp_o1));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((int32_T *) &Rfm2gWrite_B.Read_Udp_o2));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_Udp");
      ssSetPath(rts, "Rfm2gWrite/Read_Udp");
      ssSetRTModel(rts,Rfm2gWrite_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)Rfm2gWrite_P.Read_Udp_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)Rfm2gWrite_P.Read_Udp_P2_Size);
        ssSetSFcnParam(rts, 2, (mxArray*)Rfm2gWrite_P.Read_Udp_P3_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &Rfm2gWrite_DW.Read_Udp_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn0.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &Rfm2gWrite_DW.Read_Udp_PWORK);
      }

      /* registration */
      simReadUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: Rfm2gWrite/<Root>/Write_Rfm2g (simWriteRfm2g) */
    {
      SimStruct *rts = Rfm2gWrite_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &Rfm2gWrite_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, Rfm2gWrite_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &Rfm2gWrite_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &Rfm2gWrite_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &Rfm2gWrite_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, Rfm2gWrite_B.Read_Udp_o1);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 30);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_Rfm2g");
      ssSetPath(rts, "Rfm2gWrite/Write_Rfm2g");
      ssSetRTModel(rts,Rfm2gWrite_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 1);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)Rfm2gWrite_P.Write_Rfm2g_P1_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &Rfm2gWrite_DW.Write_Rfm2g_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &Rfm2gWrite_M->NonInlinedSFcns.Sfcn1.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &Rfm2gWrite_DW.Write_Rfm2g_PWORK);
      }

      /* registration */
      simWriteRfm2g(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetInputPortWidth(rts, 0, 30);
      ssSetInputPortDataType(rts, 0, SS_UINT8);
      ssSetInputPortComplexSignal(rts, 0, 0);
      ssSetInputPortFrameData(rts, 0, 0);
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  Rfm2gWrite_M->Sizes.numContStates = (0);/* Number of continuous states */
  Rfm2gWrite_M->Sizes.numY = (0);      /* Number of model outputs */
  Rfm2gWrite_M->Sizes.numU = (0);      /* Number of model inputs */
  Rfm2gWrite_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  Rfm2gWrite_M->Sizes.numSampTimes = (1);/* Number of sample times */
  Rfm2gWrite_M->Sizes.numBlocks = (2); /* Number of blocks */
  Rfm2gWrite_M->Sizes.numBlockIO = (2);/* Number of block outputs */
  Rfm2gWrite_M->Sizes.numBlockPrms = (12);/* Sum of parameter "widths" */
  return Rfm2gWrite_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
