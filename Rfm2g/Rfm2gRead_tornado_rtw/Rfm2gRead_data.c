/*
 * Rfm2gRead_data.c
 *
 * Code generation for model "Rfm2gRead".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 15:42:36 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "Rfm2gRead.h"
#include "Rfm2gRead_private.h"

/* Block parameters (auto storage) */
P_Rfm2gRead_T Rfm2gRead_P = {
  /*  Computed Parameter: Read_Rfm2g_P1_Size
   * Referenced by: '<Root>/Read_Rfm2g'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: offset
                                        * Referenced by: '<Root>/Read_Rfm2g'
                                        */

  /*  Computed Parameter: Read_Rfm2g_P2_Size
   * Referenced by: '<Root>/Read_Rfm2g'
   */
  { 1.0, 1.0 },
  30.0,                                /* Expression: dataLength
                                        * Referenced by: '<Root>/Read_Rfm2g'
                                        */

  /*  Computed Parameter: Write_Udp_P1_Size
   * Referenced by: '<Root>/Write_Udp'
   */
  { 1.0, 13.0 },

  /*  Computed Parameter: Write_Udp_P1
   * Referenced by: '<Root>/Write_Udp'
   */
  { 49.0, 57.0, 49.0, 46.0, 49.0, 54.0, 46.0, 50.0, 56.0, 46.0, 49.0, 49.0, 49.0
  },

  /*  Computed Parameter: Write_Udp_P2_Size
   * Referenced by: '<Root>/Write_Udp'
   */
  { 1.0, 1.0 },
  6868.0,                              /* Expression: port
                                        * Referenced by: '<Root>/Write_Udp'
                                        */

  /*  Computed Parameter: Write_Udp_P3_Size
   * Referenced by: '<Root>/Write_Udp'
   */
  { 1.0, 1.0 },
  -1.0                                 /* Expression: priod
                                        * Referenced by: '<Root>/Write_Udp'
                                        */
};
