/*
 * Rfm2gRead.c
 *
 * Code generation for model "Rfm2gRead".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 15:42:36 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "Rfm2gRead.h"
#include "Rfm2gRead_private.h"

/* Block signals (auto storage) */
B_Rfm2gRead_T Rfm2gRead_B;

/* Block states (auto storage) */
DW_Rfm2gRead_T Rfm2gRead_DW;

/* Real-time model */
RT_MODEL_Rfm2gRead_T Rfm2gRead_M_;
RT_MODEL_Rfm2gRead_T *const Rfm2gRead_M = &Rfm2gRead_M_;

/* Model output function */
static void Rfm2gRead_output(void)
{
  /* Level2 S-Function Block: '<Root>/Read_Rfm2g' (simReadRfm2g) */
  {
    SimStruct *rts = Rfm2gRead_M->childSfunctions[0];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<Root>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = Rfm2gRead_M->childSfunctions[1];
    sfcnOutputs(rts, 0);
  }
}

/* Model update function */
static void Rfm2gRead_update(void)
{
  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++Rfm2gRead_M->Timing.clockTick0)) {
    ++Rfm2gRead_M->Timing.clockTickH0;
  }

  Rfm2gRead_M->Timing.t[0] = Rfm2gRead_M->Timing.clockTick0 *
    Rfm2gRead_M->Timing.stepSize0 + Rfm2gRead_M->Timing.clockTickH0 *
    Rfm2gRead_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void Rfm2gRead_initialize(void)
{
  /* Level2 S-Function Block: '<Root>/Read_Rfm2g' (simReadRfm2g) */
  {
    SimStruct *rts = Rfm2gRead_M->childSfunctions[0];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<Root>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = Rfm2gRead_M->childSfunctions[1];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }
}

/* Model terminate function */
void Rfm2gRead_terminate(void)
{
  /* Level2 S-Function Block: '<Root>/Read_Rfm2g' (simReadRfm2g) */
  {
    SimStruct *rts = Rfm2gRead_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<Root>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = Rfm2gRead_M->childSfunctions[1];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  Rfm2gRead_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  Rfm2gRead_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  Rfm2gRead_initialize();
}

void MdlTerminate(void)
{
  Rfm2gRead_terminate();
}

/* Registration function */
RT_MODEL_Rfm2gRead_T *Rfm2gRead(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Rfm2gRead_M, 0,
                sizeof(RT_MODEL_Rfm2gRead_T));
  rtsiSetSolverName(&Rfm2gRead_M->solverInfo,"FixedStepDiscrete");
  Rfm2gRead_M->solverInfoPtr = (&Rfm2gRead_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = Rfm2gRead_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    Rfm2gRead_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    Rfm2gRead_M->Timing.sampleTimes = (&Rfm2gRead_M->Timing.sampleTimesArray[0]);
    Rfm2gRead_M->Timing.offsetTimes = (&Rfm2gRead_M->Timing.offsetTimesArray[0]);

    /* task periods */
    Rfm2gRead_M->Timing.sampleTimes[0] = (0.002);

    /* task offsets */
    Rfm2gRead_M->Timing.offsetTimes[0] = (0.0);
  }

  rtmSetTPtr(Rfm2gRead_M, &Rfm2gRead_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = Rfm2gRead_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    Rfm2gRead_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(Rfm2gRead_M, -1);
  Rfm2gRead_M->Timing.stepSize0 = 0.002;
  Rfm2gRead_M->solverInfoPtr = (&Rfm2gRead_M->solverInfo);
  Rfm2gRead_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&Rfm2gRead_M->solverInfo, 0.002);
  rtsiSetSolverMode(&Rfm2gRead_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  Rfm2gRead_M->ModelData.blockIO = ((void *) &Rfm2gRead_B);
  (void) memset(((void *) &Rfm2gRead_B), 0,
                sizeof(B_Rfm2gRead_T));

  /* parameters */
  Rfm2gRead_M->ModelData.defaultParam = ((real_T *)&Rfm2gRead_P);

  /* states (dwork) */
  Rfm2gRead_M->ModelData.dwork = ((void *) &Rfm2gRead_DW);
  (void) memset((void *)&Rfm2gRead_DW, 0,
                sizeof(DW_Rfm2gRead_T));

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &Rfm2gRead_M->NonInlinedSFcns.sfcnInfo;
    Rfm2gRead_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(Rfm2gRead_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &Rfm2gRead_M->Sizes.numSampTimes);
    Rfm2gRead_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(Rfm2gRead_M)[0]);
    rtssSetTPtrPtr(sfcnInfo,Rfm2gRead_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(Rfm2gRead_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(Rfm2gRead_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(Rfm2gRead_M));
    rtssSetStepSizePtr(sfcnInfo, &Rfm2gRead_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(Rfm2gRead_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &Rfm2gRead_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo,
      &Rfm2gRead_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &Rfm2gRead_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &Rfm2gRead_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo, &Rfm2gRead_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &Rfm2gRead_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &Rfm2gRead_M->solverInfoPtr);
  }

  Rfm2gRead_M->Sizes.numSFcns = (2);

  /* register each child */
  {
    (void) memset((void *)&Rfm2gRead_M->NonInlinedSFcns.childSFunctions[0], 0,
                  2*sizeof(SimStruct));
    Rfm2gRead_M->childSfunctions =
      (&Rfm2gRead_M->NonInlinedSFcns.childSFunctionPtrs[0]);
    Rfm2gRead_M->childSfunctions[0] =
      (&Rfm2gRead_M->NonInlinedSFcns.childSFunctions[0]);
    Rfm2gRead_M->childSfunctions[1] =
      (&Rfm2gRead_M->NonInlinedSFcns.childSFunctions[1]);

    /* Level2 S-Function Block: Rfm2gRead/<Root>/Read_Rfm2g (simReadRfm2g) */
    {
      SimStruct *rts = Rfm2gRead_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = Rfm2gRead_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = Rfm2gRead_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = Rfm2gRead_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &Rfm2gRead_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, Rfm2gRead_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &Rfm2gRead_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &Rfm2gRead_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &Rfm2gRead_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 1);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 30);
          ssSetOutputPortSignal(rts, 0, ((uint8_T *) Rfm2gRead_B.Read_Rfm2g));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_Rfm2g");
      ssSetPath(rts, "Rfm2gRead/Read_Rfm2g");
      ssSetRTModel(rts,Rfm2gRead_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)Rfm2gRead_P.Read_Rfm2g_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)Rfm2gRead_P.Read_Rfm2g_P2_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &Rfm2gRead_DW.Read_Rfm2g_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn0.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn0.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &Rfm2gRead_DW.Read_Rfm2g_PWORK);
      }

      /* registration */
      simReadRfm2g(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: Rfm2gRead/<Root>/Write_Udp (simWriteUdp) */
    {
      SimStruct *rts = Rfm2gRead_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = Rfm2gRead_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = Rfm2gRead_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = Rfm2gRead_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &Rfm2gRead_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, Rfm2gRead_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &Rfm2gRead_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &Rfm2gRead_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &Rfm2gRead_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, Rfm2gRead_B.Read_Rfm2g);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 30);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_Udp");
      ssSetPath(rts, "Rfm2gRead/Write_Udp");
      ssSetRTModel(rts,Rfm2gRead_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)Rfm2gRead_P.Write_Udp_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)Rfm2gRead_P.Write_Udp_P2_Size);
        ssSetSFcnParam(rts, 2, (mxArray*)Rfm2gRead_P.Write_Udp_P3_Size);
      }

      /* work vectors */
      ssSetIWork(rts, (int_T *) &Rfm2gRead_DW.Write_Udp_IWORK);
      ssSetPWork(rts, (void **) &Rfm2gRead_DW.Write_Udp_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn1.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &Rfm2gRead_M->NonInlinedSFcns.Sfcn1.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 2);

        /* IWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_INTEGER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &Rfm2gRead_DW.Write_Udp_IWORK);

        /* PWORK */
        ssSetDWorkWidth(rts, 1, 1);
        ssSetDWorkDataType(rts, 1,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 1, 0);
        ssSetDWork(rts, 1, &Rfm2gRead_DW.Write_Udp_PWORK);
      }

      /* registration */
      simWriteUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetInputPortWidth(rts, 0, 30);
      ssSetInputPortDataType(rts, 0, SS_UINT8);
      ssSetInputPortComplexSignal(rts, 0, 0);
      ssSetInputPortFrameData(rts, 0, 0);
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  Rfm2gRead_M->Sizes.numContStates = (0);/* Number of continuous states */
  Rfm2gRead_M->Sizes.numY = (0);       /* Number of model outputs */
  Rfm2gRead_M->Sizes.numU = (0);       /* Number of model inputs */
  Rfm2gRead_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  Rfm2gRead_M->Sizes.numSampTimes = (1);/* Number of sample times */
  Rfm2gRead_M->Sizes.numBlocks = (2);  /* Number of blocks */
  Rfm2gRead_M->Sizes.numBlockIO = (1); /* Number of block outputs */
  Rfm2gRead_M->Sizes.numBlockPrms = (27);/* Sum of parameter "widths" */
  return Rfm2gRead_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
