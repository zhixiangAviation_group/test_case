/*
 * Rfm2gRead_private.h
 *
 * Code generation for model "Rfm2gRead".
 *
 * Model version              : 1.9
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 15:42:36 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_Rfm2gRead_private_h_
#define RTW_HEADER_Rfm2gRead_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#endif                                 /* __RTWTYPES_H__ */

extern void simReadRfm2g(SimStruct *rts);
extern void simWriteUdp(SimStruct *rts);

#endif                                 /* RTW_HEADER_Rfm2gRead_private_h_ */
