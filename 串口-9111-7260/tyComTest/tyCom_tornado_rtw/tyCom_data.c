/*
 * tyCom_data.c
 *
 * Code generation for model "tyCom".
 *
 * Model version              : 1.55
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Mon Jan 26 08:57:32 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "tyCom.h"
#include "tyCom_private.h"

/* Block parameters (auto storage) */
P_tyCom_T tyCom_P = {
  /*  Computed Parameter: pooled1
   * Referenced by:
   *   '<S1>/Write_Tycom_Data'
   *   '<S5>/Read_Tycom_Data'
   */
  { 1.0, 1.0 },
  115200.0,                            /* Variable: TyComBaud
                                        * Referenced by:
                                        *   '<S1>/Write_Tycom_Data'
                                        *   '<S5>/Read_Tycom_Data'
                                        */

  /*  Computed Parameter: Read_Udp_P1_Size
   * Referenced by: '<S1>/Read_Udp'
   */
  { 1.0, 1.0 },
  8.0,                                 /* Expression: dataLen
                                        * Referenced by: '<S1>/Read_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P2_Size
   * Referenced by: '<S1>/Read_Udp'
   */
  { 1.0, 1.0 },
  6804.0,                              /* Expression: locPort
                                        * Referenced by: '<S1>/Read_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P3_Size
   * Referenced by: '<S1>/Read_Udp'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: priod
                                        * Referenced by: '<S1>/Read_Udp'
                                        */

  /*  Computed Parameter: Write_Tycom_Data_P1_Size
   * Referenced by: '<S1>/Write_Tycom_Data'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: tycom
                                        * Referenced by: '<S1>/Write_Tycom_Data'
                                        */

  /*  Computed Parameter: Read_9111_Data_P1_Size
   * Referenced by: '<S2>/Read_9111_Data'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: cardNum
                                        * Referenced by: '<S2>/Read_9111_Data'
                                        */

  /*  Computed Parameter: Read_9111_Data_P2_Size
   * Referenced by: '<S2>/Read_9111_Data'
   */
  { 1.0, 10.0 },

  /*  Expression: channelArr
   * Referenced by: '<S2>/Read_9111_Data'
   */
  { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 },

  /*  Computed Parameter: Write_Udp_P1_Size
   * Referenced by: '<S2>/Write_Udp'
   */
  { 1.0, 12.0 },

  /*  Computed Parameter: Write_Udp_P1
   * Referenced by: '<S2>/Write_Udp'
   */
  { 49.0, 57.0, 50.0, 46.0, 49.0, 54.0, 56.0, 46.0, 51.0, 46.0, 51.0, 52.0 },

  /*  Computed Parameter: Write_Udp_P2_Size
   * Referenced by: '<S2>/Write_Udp'
   */
  { 1.0, 1.0 },
  6801.0,                              /* Expression: port
                                        * Referenced by: '<S2>/Write_Udp'
                                        */

  /*  Computed Parameter: Write_Udp_P3_Size
   * Referenced by: '<S2>/Write_Udp'
   */
  { 1.0, 1.0 },
  1.0,                                 /* Expression: priod
                                        * Referenced by: '<S2>/Write_Udp'
                                        */

  /*  Computed Parameter: Read_7260_Data_P1_Size
   * Referenced by: '<S3>/Read_7260_Data'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: cardNum
                                        * Referenced by: '<S3>/Read_7260_Data'
                                        */

  /*  Computed Parameter: Read_7260_Data_P2_Size
   * Referenced by: '<S3>/Read_7260_Data'
   */
  { 1.0, 8.0 },

  /*  Expression: channelArr
   * Referenced by: '<S3>/Read_7260_Data'
   */
  { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0 },

  /*  Computed Parameter: Write_Udp_P1_Size_l
   * Referenced by: '<S3>/Write_Udp'
   */
  { 1.0, 12.0 },

  /*  Computed Parameter: Write_Udp_P1_i
   * Referenced by: '<S3>/Write_Udp'
   */
  { 49.0, 57.0, 50.0, 46.0, 49.0, 54.0, 56.0, 46.0, 51.0, 46.0, 51.0, 52.0 },

  /*  Computed Parameter: Write_Udp_P2_Size_o
   * Referenced by: '<S3>/Write_Udp'
   */
  { 1.0, 1.0 },
  6800.0,                              /* Expression: port
                                        * Referenced by: '<S3>/Write_Udp'
                                        */

  /*  Computed Parameter: Write_Udp_P3_Size_p
   * Referenced by: '<S3>/Write_Udp'
   */
  { 1.0, 1.0 },
  1.0,                                 /* Expression: priod
                                        * Referenced by: '<S3>/Write_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P1_Size_o
   * Referenced by: '<S4>/Read_Udp'
   */
  { 1.0, 1.0 },
  8.0,                                 /* Expression: dataLen
                                        * Referenced by: '<S4>/Read_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P2_Size_o
   * Referenced by: '<S4>/Read_Udp'
   */
  { 1.0, 1.0 },
  6803.0,                              /* Expression: locPort
                                        * Referenced by: '<S4>/Read_Udp'
                                        */

  /*  Computed Parameter: Read_Udp_P3_Size_k
   * Referenced by: '<S4>/Read_Udp'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: priod
                                        * Referenced by: '<S4>/Read_Udp'
                                        */

  /*  Computed Parameter: Write_7260_Data_P1_Size
   * Referenced by: '<S4>/Write_7260_Data'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: cardNum
                                        * Referenced by: '<S4>/Write_7260_Data'
                                        */

  /*  Computed Parameter: Write_7260_Data_P2_Size
   * Referenced by: '<S4>/Write_7260_Data'
   */
  { 1.0, 8.0 },

  /*  Expression: channelArr
   * Referenced by: '<S4>/Write_7260_Data'
   */
  { 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0 },

  /*  Computed Parameter: Read_Tycom_Data_P1_Size
   * Referenced by: '<S5>/Read_Tycom_Data'
   */
  { 1.0, 1.0 },
  0.0,                                 /* Expression: tycom
                                        * Referenced by: '<S5>/Read_Tycom_Data'
                                        */

  /*  Computed Parameter: Write_Udp_P1_Size_f
   * Referenced by: '<S5>/Write_Udp'
   */
  { 1.0, 12.0 },

  /*  Computed Parameter: Write_Udp_P1_o
   * Referenced by: '<S5>/Write_Udp'
   */
  { 49.0, 57.0, 50.0, 46.0, 49.0, 54.0, 56.0, 46.0, 51.0, 46.0, 51.0, 52.0 },

  /*  Computed Parameter: Write_Udp_P2_Size_m
   * Referenced by: '<S5>/Write_Udp'
   */
  { 1.0, 1.0 },
  6802.0,                              /* Expression: port
                                        * Referenced by: '<S5>/Write_Udp'
                                        */

  /*  Computed Parameter: Write_Udp_P3_Size_j
   * Referenced by: '<S5>/Write_Udp'
   */
  { 1.0, 1.0 },
  -1.0                                 /* Expression: priod
                                        * Referenced by: '<S5>/Write_Udp'
                                        */
};
