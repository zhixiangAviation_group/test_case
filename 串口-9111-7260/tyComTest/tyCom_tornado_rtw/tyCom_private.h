/*
 * tyCom_private.h
 *
 * Code generation for model "tyCom".
 *
 * Model version              : 1.55
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Mon Jan 26 08:57:32 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_tyCom_private_h_
#define RTW_HEADER_tyCom_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#endif                                 /* __RTWTYPES_H__ */

extern void simReadUdp(SimStruct *rts);
extern void simWriteVxTyCom(SimStruct *rts);
extern void simReadVx9111(SimStruct *rts);
extern void simWriteUdp(SimStruct *rts);
extern void simReadVx7260(SimStruct *rts);
extern void simWriteVx7260(SimStruct *rts);
extern void simReadVxTyCom(SimStruct *rts);

#endif                                 /* RTW_HEADER_tyCom_private_h_ */
