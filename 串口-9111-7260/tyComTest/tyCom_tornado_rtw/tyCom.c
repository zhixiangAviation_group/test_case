/*
 * tyCom.c
 *
 * Code generation for model "tyCom".
 *
 * Model version              : 1.55
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Mon Jan 26 08:57:32 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "tyCom.h"
#include "tyCom_private.h"

/* Block signals (auto storage) */
B_tyCom_T tyCom_B;

/* Block states (auto storage) */
DW_tyCom_T tyCom_DW;

/* Real-time model */
RT_MODEL_tyCom_T tyCom_M_;
RT_MODEL_tyCom_T *const tyCom_M = &tyCom_M_;

/* Model output function */
static void tyCom_output(void)
{
  /* Level2 S-Function Block: '<S1>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[0];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<S1>/Write_Tycom_Data' (simWriteVxTyCom) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[1];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<S2>/Read_9111_Data' (simReadVx9111) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[2];
    sfcnOutputs(rts, 0);
  }

  /* SignalConversion: '<S2>/TmpSignal ConversionAtByte PackInport1' */
  tyCom_B.TmpSignalConversionAtBytePackIn[0] = tyCom_B.Read_9111_Data_o1;
  tyCom_B.TmpSignalConversionAtBytePackIn[1] = tyCom_B.Read_9111_Data_o2;
  tyCom_B.TmpSignalConversionAtBytePackIn[2] = tyCom_B.Read_9111_Data_o3;
  tyCom_B.TmpSignalConversionAtBytePackIn[3] = tyCom_B.Read_9111_Data_o4;
  tyCom_B.TmpSignalConversionAtBytePackIn[4] = tyCom_B.Read_9111_Data_o5;
  tyCom_B.TmpSignalConversionAtBytePackIn[5] = tyCom_B.Read_9111_Data_o6;
  tyCom_B.TmpSignalConversionAtBytePackIn[6] = tyCom_B.Read_9111_Data_o7;
  tyCom_B.TmpSignalConversionAtBytePackIn[7] = tyCom_B.Read_9111_Data_o8;
  tyCom_B.TmpSignalConversionAtBytePackIn[8] = tyCom_B.Read_9111_Data_o9;
  tyCom_B.TmpSignalConversionAtBytePackIn[9] = tyCom_B.Read_9111_Data_o10;

  /* Pack: <S2>/Byte Pack */
  (void) memcpy(&tyCom_B.BytePack[0], &tyCom_B.TmpSignalConversionAtBytePackIn[0],
                80);

  /* Level2 S-Function Block: '<S2>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[3];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<S3>/Read_7260_Data' (simReadVx7260) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[4];
    sfcnOutputs(rts, 0);
  }

  /* SignalConversion: '<S3>/TmpSignal ConversionAtWrite_UdpInport1' */
  tyCom_B.TmpSignalConversionAtWrite_UdpI[0] = tyCom_B.Read_7260_Data_o1;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[1] = tyCom_B.Read_7260_Data_o2;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[2] = tyCom_B.Read_7260_Data_o3;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[3] = tyCom_B.Read_7260_Data_o4;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[4] = tyCom_B.Read_7260_Data_o5;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[5] = tyCom_B.Read_7260_Data_o6;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[6] = tyCom_B.Read_7260_Data_o7;
  tyCom_B.TmpSignalConversionAtWrite_UdpI[7] = tyCom_B.Read_7260_Data_o8;

  /* Level2 S-Function Block: '<S3>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[5];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<S4>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[6];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<S4>/Write_7260_Data' (simWriteVx7260) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[7];
    sfcnOutputs(rts, 0);
  }

  /* Level2 S-Function Block: '<S5>/Read_Tycom_Data' (simReadVxTyCom) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[8];
    sfcnOutputs(rts, 0);
  }

  /* SignalConversion: '<S5>/TmpSignal ConversionAtByte Pack1Inport1' */
  tyCom_B.TmpSignalConversionAtBytePack1I[0] = tyCom_B.Read_Tycom_Data_o1;
  tyCom_B.TmpSignalConversionAtBytePack1I[1] = tyCom_B.Read_Tycom_Data_o2;
  tyCom_B.TmpSignalConversionAtBytePack1I[2] = tyCom_B.Read_Tycom_Data_o3;
  tyCom_B.TmpSignalConversionAtBytePack1I[3] = tyCom_B.Read_Tycom_Data_o4;

  /* Pack: <S5>/Byte Pack1 */
  (void) memcpy(&tyCom_B.BytePack1[0], &tyCom_B.TmpSignalConversionAtBytePack1I
                [0],
                16);

  /* Level2 S-Function Block: '<S5>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[9];
    sfcnOutputs(rts, 0);
  }
}

/* Model update function */
static void tyCom_update(void)
{
  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++tyCom_M->Timing.clockTick0)) {
    ++tyCom_M->Timing.clockTickH0;
  }

  tyCom_M->Timing.t[0] = tyCom_M->Timing.clockTick0 * tyCom_M->Timing.stepSize0
    + tyCom_M->Timing.clockTickH0 * tyCom_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void tyCom_initialize(void)
{
  /* Level2 S-Function Block: '<S1>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[0];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S1>/Write_Tycom_Data' (simWriteVxTyCom) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[1];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S2>/Read_9111_Data' (simReadVx9111) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[2];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S2>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[3];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S3>/Read_7260_Data' (simReadVx7260) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[4];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S3>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[5];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S4>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[6];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S4>/Write_7260_Data' (simWriteVx7260) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[7];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S5>/Read_Tycom_Data' (simReadVxTyCom) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[8];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }

  /* Level2 S-Function Block: '<S5>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[9];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }
}

/* Model terminate function */
void tyCom_terminate(void)
{
  /* Level2 S-Function Block: '<S1>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S1>/Write_Tycom_Data' (simWriteVxTyCom) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/Read_9111_Data' (simReadVx9111) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S2>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[3];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S3>/Read_7260_Data' (simReadVx7260) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[4];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S3>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[5];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S4>/Read_Udp' (simReadUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[6];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S4>/Write_7260_Data' (simWriteVx7260) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[7];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S5>/Read_Tycom_Data' (simReadVxTyCom) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[8];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: '<S5>/Write_Udp' (simWriteUdp) */
  {
    SimStruct *rts = tyCom_M->childSfunctions[9];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  tyCom_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  tyCom_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  tyCom_initialize();
}

void MdlTerminate(void)
{
  tyCom_terminate();
}

/* Registration function */
RT_MODEL_tyCom_T *tyCom(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)tyCom_M, 0,
                sizeof(RT_MODEL_tyCom_T));
  rtsiSetSolverName(&tyCom_M->solverInfo,"FixedStepDiscrete");
  tyCom_M->solverInfoPtr = (&tyCom_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = tyCom_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    tyCom_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    tyCom_M->Timing.sampleTimes = (&tyCom_M->Timing.sampleTimesArray[0]);
    tyCom_M->Timing.offsetTimes = (&tyCom_M->Timing.offsetTimesArray[0]);

    /* task periods */
    tyCom_M->Timing.sampleTimes[0] = (0.01);

    /* task offsets */
    tyCom_M->Timing.offsetTimes[0] = (0.0);
  }

  rtmSetTPtr(tyCom_M, &tyCom_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = tyCom_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    tyCom_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(tyCom_M, -1);
  tyCom_M->Timing.stepSize0 = 0.01;
  tyCom_M->solverInfoPtr = (&tyCom_M->solverInfo);
  tyCom_M->Timing.stepSize = (0.01);
  rtsiSetFixedStepSize(&tyCom_M->solverInfo, 0.01);
  rtsiSetSolverMode(&tyCom_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  tyCom_M->ModelData.blockIO = ((void *) &tyCom_B);
  (void) memset(((void *) &tyCom_B), 0,
                sizeof(B_tyCom_T));

  /* parameters */
  tyCom_M->ModelData.defaultParam = ((real_T *)&tyCom_P);

  /* states (dwork) */
  tyCom_M->ModelData.dwork = ((void *) &tyCom_DW);
  (void) memset((void *)&tyCom_DW, 0,
                sizeof(DW_tyCom_T));

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &tyCom_M->NonInlinedSFcns.sfcnInfo;
    tyCom_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(tyCom_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &tyCom_M->Sizes.numSampTimes);
    tyCom_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(tyCom_M)[0]);
    rtssSetTPtrPtr(sfcnInfo,tyCom_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(tyCom_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(tyCom_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(tyCom_M));
    rtssSetStepSizePtr(sfcnInfo, &tyCom_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(tyCom_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &tyCom_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo, &tyCom_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &tyCom_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &tyCom_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo, &tyCom_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &tyCom_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &tyCom_M->solverInfoPtr);
  }

  tyCom_M->Sizes.numSFcns = (10);

  /* register each child */
  {
    (void) memset((void *)&tyCom_M->NonInlinedSFcns.childSFunctions[0], 0,
                  10*sizeof(SimStruct));
    tyCom_M->childSfunctions = (&tyCom_M->NonInlinedSFcns.childSFunctionPtrs[0]);

    {
      int_T i;
      for (i = 0; i < 10; i++) {
        tyCom_M->childSfunctions[i] = (&tyCom_M->
          NonInlinedSFcns.childSFunctions[i]);
      }
    }

    /* Level2 S-Function Block: tyCom/<S1>/Read_Udp (simReadUdp) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 2);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 8);
          ssSetOutputPortSignal(rts, 0, ((uint8_T *) tyCom_B.Read_Udp_o1));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((int32_T *) &tyCom_B.Read_Udp_o2));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_Udp");
      ssSetPath(rts, "tyCom/Pwm���/Read_Udp");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Read_Udp_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Read_Udp_P2_Size);
        ssSetSFcnParam(rts, 2, (mxArray*)tyCom_P.Read_Udp_P3_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Read_Udp_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn0.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn0.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Read_Udp_PWORK);
      }

      /* registration */
      simReadUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: tyCom/<S1>/Write_Tycom_Data (simWriteVxTyCom) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[1];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[1]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[1]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[1]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[1]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, tyCom_B.Read_Udp_o1);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 8);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_Tycom_Data");
      ssSetPath(rts, "tyCom/Pwm���/Write_Tycom_Data");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn1.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Write_Tycom_Data_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.pooled1);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Write_Tycom_Data_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn1.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn1.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Write_Tycom_Data_PWORK);
      }

      /* registration */
      simWriteVxTyCom(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: tyCom/<S2>/Read_9111_Data (simReadVx9111) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[2];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[2]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[2]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[2]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[2]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 10);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((real_T *) &tyCom_B.Read_9111_Data_o1));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((real_T *) &tyCom_B.Read_9111_Data_o2));
        }

        /* port 2 */
        {
          _ssSetOutputPortNumDimensions(rts, 2, 1);
          ssSetOutputPortWidth(rts, 2, 1);
          ssSetOutputPortSignal(rts, 2, ((real_T *) &tyCom_B.Read_9111_Data_o3));
        }

        /* port 3 */
        {
          _ssSetOutputPortNumDimensions(rts, 3, 1);
          ssSetOutputPortWidth(rts, 3, 1);
          ssSetOutputPortSignal(rts, 3, ((real_T *) &tyCom_B.Read_9111_Data_o4));
        }

        /* port 4 */
        {
          _ssSetOutputPortNumDimensions(rts, 4, 1);
          ssSetOutputPortWidth(rts, 4, 1);
          ssSetOutputPortSignal(rts, 4, ((real_T *) &tyCom_B.Read_9111_Data_o5));
        }

        /* port 5 */
        {
          _ssSetOutputPortNumDimensions(rts, 5, 1);
          ssSetOutputPortWidth(rts, 5, 1);
          ssSetOutputPortSignal(rts, 5, ((real_T *) &tyCom_B.Read_9111_Data_o6));
        }

        /* port 6 */
        {
          _ssSetOutputPortNumDimensions(rts, 6, 1);
          ssSetOutputPortWidth(rts, 6, 1);
          ssSetOutputPortSignal(rts, 6, ((real_T *) &tyCom_B.Read_9111_Data_o7));
        }

        /* port 7 */
        {
          _ssSetOutputPortNumDimensions(rts, 7, 1);
          ssSetOutputPortWidth(rts, 7, 1);
          ssSetOutputPortSignal(rts, 7, ((real_T *) &tyCom_B.Read_9111_Data_o8));
        }

        /* port 8 */
        {
          _ssSetOutputPortNumDimensions(rts, 8, 1);
          ssSetOutputPortWidth(rts, 8, 1);
          ssSetOutputPortSignal(rts, 8, ((real_T *) &tyCom_B.Read_9111_Data_o9));
        }

        /* port 9 */
        {
          _ssSetOutputPortNumDimensions(rts, 9, 1);
          ssSetOutputPortWidth(rts, 9, 1);
          ssSetOutputPortSignal(rts, 9, ((real_T *) &tyCom_B.Read_9111_Data_o10));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_9111_Data");
      ssSetPath(rts, "tyCom/ģ��������/Read_9111_Data");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn2.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Read_9111_Data_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Read_9111_Data_P2_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Read_9111_Data_PWORK[0]);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn2.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn2.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 2);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Read_9111_Data_PWORK[0]);
      }

      /* registration */
      simReadVx9111(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 1);
      _ssSetOutputPortConnected(rts, 2, 1);
      _ssSetOutputPortConnected(rts, 3, 1);
      _ssSetOutputPortConnected(rts, 4, 1);
      _ssSetOutputPortConnected(rts, 5, 1);
      _ssSetOutputPortConnected(rts, 6, 1);
      _ssSetOutputPortConnected(rts, 7, 1);
      _ssSetOutputPortConnected(rts, 8, 1);
      _ssSetOutputPortConnected(rts, 9, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 2, 0);
      _ssSetOutputPortBeingMerged(rts, 3, 0);
      _ssSetOutputPortBeingMerged(rts, 4, 0);
      _ssSetOutputPortBeingMerged(rts, 5, 0);
      _ssSetOutputPortBeingMerged(rts, 6, 0);
      _ssSetOutputPortBeingMerged(rts, 7, 0);
      _ssSetOutputPortBeingMerged(rts, 8, 0);
      _ssSetOutputPortBeingMerged(rts, 9, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: tyCom/<S2>/Write_Udp (simWriteUdp) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[3];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[3]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[3]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[3]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[3]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, tyCom_B.BytePack);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 80);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_Udp");
      ssSetPath(rts, "tyCom/ģ��������/Write_Udp");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn3.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Write_Udp_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Write_Udp_P2_Size);
        ssSetSFcnParam(rts, 2, (mxArray*)tyCom_P.Write_Udp_P3_Size);
      }

      /* work vectors */
      ssSetIWork(rts, (int_T *) &tyCom_DW.Write_Udp_IWORK);
      ssSetPWork(rts, (void **) &tyCom_DW.Write_Udp_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn3.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn3.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 2);

        /* IWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_INTEGER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Write_Udp_IWORK);

        /* PWORK */
        ssSetDWorkWidth(rts, 1, 1);
        ssSetDWorkDataType(rts, 1,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 1, 0);
        ssSetDWork(rts, 1, &tyCom_DW.Write_Udp_PWORK);
      }

      /* registration */
      simWriteUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetInputPortWidth(rts, 0, 80);
      ssSetInputPortDataType(rts, 0, SS_UINT8);
      ssSetInputPortComplexSignal(rts, 0, 0);
      ssSetInputPortFrameData(rts, 0, 0);
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: tyCom/<S3>/Read_7260_Data (simReadVx7260) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[4];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn4.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn4.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn4.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[4]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[4]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[4]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[4]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn4.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 8);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((uint8_T *) &tyCom_B.Read_7260_Data_o1));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((uint8_T *) &tyCom_B.Read_7260_Data_o2));
        }

        /* port 2 */
        {
          _ssSetOutputPortNumDimensions(rts, 2, 1);
          ssSetOutputPortWidth(rts, 2, 1);
          ssSetOutputPortSignal(rts, 2, ((uint8_T *) &tyCom_B.Read_7260_Data_o3));
        }

        /* port 3 */
        {
          _ssSetOutputPortNumDimensions(rts, 3, 1);
          ssSetOutputPortWidth(rts, 3, 1);
          ssSetOutputPortSignal(rts, 3, ((uint8_T *) &tyCom_B.Read_7260_Data_o4));
        }

        /* port 4 */
        {
          _ssSetOutputPortNumDimensions(rts, 4, 1);
          ssSetOutputPortWidth(rts, 4, 1);
          ssSetOutputPortSignal(rts, 4, ((uint8_T *) &tyCom_B.Read_7260_Data_o5));
        }

        /* port 5 */
        {
          _ssSetOutputPortNumDimensions(rts, 5, 1);
          ssSetOutputPortWidth(rts, 5, 1);
          ssSetOutputPortSignal(rts, 5, ((uint8_T *) &tyCom_B.Read_7260_Data_o6));
        }

        /* port 6 */
        {
          _ssSetOutputPortNumDimensions(rts, 6, 1);
          ssSetOutputPortWidth(rts, 6, 1);
          ssSetOutputPortSignal(rts, 6, ((uint8_T *) &tyCom_B.Read_7260_Data_o7));
        }

        /* port 7 */
        {
          _ssSetOutputPortNumDimensions(rts, 7, 1);
          ssSetOutputPortWidth(rts, 7, 1);
          ssSetOutputPortSignal(rts, 7, ((uint8_T *) &tyCom_B.Read_7260_Data_o8));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_7260_Data");
      ssSetPath(rts, "tyCom/��ɢ������/Read_7260_Data");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn4.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Read_7260_Data_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Read_7260_Data_P2_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Read_7260_Data_PWORK[0]);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn4.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn4.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 2);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Read_7260_Data_PWORK[0]);
      }

      /* registration */
      simReadVx7260(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 1);
      _ssSetOutputPortConnected(rts, 2, 1);
      _ssSetOutputPortConnected(rts, 3, 1);
      _ssSetOutputPortConnected(rts, 4, 1);
      _ssSetOutputPortConnected(rts, 5, 1);
      _ssSetOutputPortConnected(rts, 6, 1);
      _ssSetOutputPortConnected(rts, 7, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 2, 0);
      _ssSetOutputPortBeingMerged(rts, 3, 0);
      _ssSetOutputPortBeingMerged(rts, 4, 0);
      _ssSetOutputPortBeingMerged(rts, 5, 0);
      _ssSetOutputPortBeingMerged(rts, 6, 0);
      _ssSetOutputPortBeingMerged(rts, 7, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: tyCom/<S3>/Write_Udp (simWriteUdp) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[5];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn5.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn5.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn5.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[5]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[5]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[5]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[5]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn5.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, tyCom_B.TmpSignalConversionAtWrite_UdpI);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 8);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_Udp");
      ssSetPath(rts, "tyCom/��ɢ������/Write_Udp");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn5.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Write_Udp_P1_Size_l);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Write_Udp_P2_Size_o);
        ssSetSFcnParam(rts, 2, (mxArray*)tyCom_P.Write_Udp_P3_Size_p);
      }

      /* work vectors */
      ssSetIWork(rts, (int_T *) &tyCom_DW.Write_Udp_IWORK_e);
      ssSetPWork(rts, (void **) &tyCom_DW.Write_Udp_PWORK_l);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn5.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn5.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 2);

        /* IWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_INTEGER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Write_Udp_IWORK_e);

        /* PWORK */
        ssSetDWorkWidth(rts, 1, 1);
        ssSetDWorkDataType(rts, 1,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 1, 0);
        ssSetDWork(rts, 1, &tyCom_DW.Write_Udp_PWORK_l);
      }

      /* registration */
      simWriteUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetInputPortWidth(rts, 0, 8);
      ssSetInputPortDataType(rts, 0, SS_UINT8);
      ssSetInputPortComplexSignal(rts, 0, 0);
      ssSetInputPortFrameData(rts, 0, 0);
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: tyCom/<S4>/Read_Udp (simReadUdp) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[6];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn6.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn6.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn6.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[6]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[6]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[6]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[6]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn6.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 2);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 8);
          ssSetOutputPortSignal(rts, 0, ((uint8_T *) tyCom_B.Read_Udp_o1_e));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((int32_T *) &tyCom_B.Read_Udp_o2_n));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_Udp");
      ssSetPath(rts, "tyCom/��ɢ�����/Read_Udp");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn6.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Read_Udp_P1_Size_o);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Read_Udp_P2_Size_o);
        ssSetSFcnParam(rts, 2, (mxArray*)tyCom_P.Read_Udp_P3_Size_k);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Read_Udp_PWORK_f);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn6.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn6.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Read_Udp_PWORK_f);
      }

      /* registration */
      simReadUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: tyCom/<S4>/Write_7260_Data (simWriteVx7260) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[7];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn7.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn7.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn7.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[7]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[7]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[7]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[7]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn7.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, tyCom_B.Read_Udp_o1_e);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 8);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_7260_Data");
      ssSetPath(rts, "tyCom/��ɢ�����/Write_7260_Data");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn7.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Write_7260_Data_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Write_7260_Data_P2_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Write_7260_Data_PWORK[0]);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn7.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn7.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 2);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Write_7260_Data_PWORK[0]);
      }

      /* registration */
      simWriteVx7260(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }

    /* Level2 S-Function Block: tyCom/<S5>/Read_Tycom_Data (simReadVxTyCom) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[8];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn8.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn8.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn8.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[8]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[8]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[8]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[8]);
      }

      /* outputs */
      {
        ssSetPortInfoForOutputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn8.outputPortInfo[0]);
        _ssSetNumOutputPorts(rts, 4);

        /* port 0 */
        {
          _ssSetOutputPortNumDimensions(rts, 0, 1);
          ssSetOutputPortWidth(rts, 0, 1);
          ssSetOutputPortSignal(rts, 0, ((uint32_T *)
            &tyCom_B.Read_Tycom_Data_o1));
        }

        /* port 1 */
        {
          _ssSetOutputPortNumDimensions(rts, 1, 1);
          ssSetOutputPortWidth(rts, 1, 1);
          ssSetOutputPortSignal(rts, 1, ((uint32_T *)
            &tyCom_B.Read_Tycom_Data_o2));
        }

        /* port 2 */
        {
          _ssSetOutputPortNumDimensions(rts, 2, 1);
          ssSetOutputPortWidth(rts, 2, 1);
          ssSetOutputPortSignal(rts, 2, ((uint32_T *)
            &tyCom_B.Read_Tycom_Data_o3));
        }

        /* port 3 */
        {
          _ssSetOutputPortNumDimensions(rts, 3, 1);
          ssSetOutputPortWidth(rts, 3, 1);
          ssSetOutputPortSignal(rts, 3, ((uint32_T *)
            &tyCom_B.Read_Tycom_Data_o4));
        }
      }

      /* path info */
      ssSetModelName(rts, "Read_Tycom_Data");
      ssSetPath(rts, "tyCom/��������/Read_Tycom_Data");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn8.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Read_Tycom_Data_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.pooled1);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &tyCom_DW.Read_Tycom_Data_PWORK);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn8.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn8.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Read_Tycom_Data_PWORK);
      }

      /* registration */
      simReadVxTyCom(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetOutputPortConnected(rts, 0, 1);
      _ssSetOutputPortConnected(rts, 1, 1);
      _ssSetOutputPortConnected(rts, 2, 1);
      _ssSetOutputPortConnected(rts, 3, 1);
      _ssSetOutputPortBeingMerged(rts, 0, 0);
      _ssSetOutputPortBeingMerged(rts, 1, 0);
      _ssSetOutputPortBeingMerged(rts, 2, 0);
      _ssSetOutputPortBeingMerged(rts, 3, 0);

      /* Update the BufferDstPort flags for each input port */
    }

    /* Level2 S-Function Block: tyCom/<S5>/Write_Udp (simWriteUdp) */
    {
      SimStruct *rts = tyCom_M->childSfunctions[9];

      /* timing info */
      time_T *sfcnPeriod = tyCom_M->NonInlinedSFcns.Sfcn9.sfcnPeriod;
      time_T *sfcnOffset = tyCom_M->NonInlinedSFcns.Sfcn9.sfcnOffset;
      int_T *sfcnTsMap = tyCom_M->NonInlinedSFcns.Sfcn9.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &tyCom_M->NonInlinedSFcns.blkInfo2[9]);
      }

      ssSetRTWSfcnInfo(rts, tyCom_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &tyCom_M->NonInlinedSFcns.methods2[9]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &tyCom_M->NonInlinedSFcns.methods3[9]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &tyCom_M->NonInlinedSFcns.statesInfo2[9]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &tyCom_M->NonInlinedSFcns.Sfcn9.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, tyCom_B.BytePack1);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 16);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_Udp");
      ssSetPath(rts, "tyCom/��������/Write_Udp");
      ssSetRTModel(rts,tyCom_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &tyCom_M->NonInlinedSFcns.Sfcn9.params;
        ssSetSFcnParamsCount(rts, 3);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)tyCom_P.Write_Udp_P1_Size_f);
        ssSetSFcnParam(rts, 1, (mxArray*)tyCom_P.Write_Udp_P2_Size_m);
        ssSetSFcnParam(rts, 2, (mxArray*)tyCom_P.Write_Udp_P3_Size_j);
      }

      /* work vectors */
      ssSetIWork(rts, (int_T *) &tyCom_DW.Write_Udp_IWORK_p);
      ssSetPWork(rts, (void **) &tyCom_DW.Write_Udp_PWORK_j);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn9.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &tyCom_M->NonInlinedSFcns.Sfcn9.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 2);

        /* IWORK */
        ssSetDWorkWidth(rts, 0, 1);
        ssSetDWorkDataType(rts, 0,SS_INTEGER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &tyCom_DW.Write_Udp_IWORK_p);

        /* PWORK */
        ssSetDWorkWidth(rts, 1, 1);
        ssSetDWorkDataType(rts, 1,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 1, 0);
        ssSetDWork(rts, 1, &tyCom_DW.Write_Udp_PWORK_j);
      }

      /* registration */
      simWriteUdp(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.01);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetInputPortWidth(rts, 0, 16);
      ssSetInputPortDataType(rts, 0, SS_UINT8);
      ssSetInputPortComplexSignal(rts, 0, 0);
      ssSetInputPortFrameData(rts, 0, 0);
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  tyCom_M->Sizes.numContStates = (0);  /* Number of continuous states */
  tyCom_M->Sizes.numY = (0);           /* Number of model outputs */
  tyCom_M->Sizes.numU = (0);           /* Number of model inputs */
  tyCom_M->Sizes.sysDirFeedThru = (0); /* The model is not direct feedthrough */
  tyCom_M->Sizes.numSampTimes = (1);   /* Number of sample times */
  tyCom_M->Sizes.numBlocks = (15);     /* Number of blocks */
  tyCom_M->Sizes.numBlockIO = (31);    /* Number of block outputs */
  tyCom_M->Sizes.numBlockPrms = (128); /* Sum of parameter "widths" */
  return tyCom_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
