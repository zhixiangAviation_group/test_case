/* ctors and dtors arrays -- to be used by runtime system */
/*   to call static constructors and destructors          */
/*                                                        */
/* NOTE: Use a C compiler to compile this file. If you    */
/*       are using GNU C++, be sure to use compile this   */
/*       file using a GNU compiler with the               */
/*       -fdollars-in-identifiers flag.                   */


void _GLOBAL__I__Fac_tidy();

void _GLOBAL__I__ZNSt6locale7_Locimp9_MakexlocERKSt8_LocinfoiPS0_PKS_();

void _GLOBAL__I__ZNSt6localeC2ERKS_S1_i();

void _GLOBAL__I__ZNSt8ios_base4Init9_Init_cntE();

void _GLOBAL__I__ZSt7nothrow();

void _GLOBAL__I___cxa_get_globals_fast();

extern void (*_ctors[])();
void (*_ctors[])() =
    {
    _GLOBAL__I__Fac_tidy,
    _GLOBAL__I__ZNSt6locale7_Locimp9_MakexlocERKSt8_LocinfoiPS0_PKS_,
    _GLOBAL__I__ZNSt6localeC2ERKS_S1_i,
    _GLOBAL__I__ZNSt8ios_base4Init9_Init_cntE,
    _GLOBAL__I__ZSt7nothrow,
    _GLOBAL__I___cxa_get_globals_fast,
    0
    };

void _GLOBAL__D_0___gthread_once();

void _GLOBAL__D__Fac_tidy();

void _GLOBAL__D__ZNSt8ios_base4Init9_Init_cntE();

void _GLOBAL__D___cxa_get_globals_fast();

extern void (*_dtors[])();
void (*_dtors[])() =
    {
    _GLOBAL__D_0___gthread_once,
    _GLOBAL__D__Fac_tidy,
    _GLOBAL__D__ZNSt8ios_base4Init9_Init_cntE,
    _GLOBAL__D___cxa_get_globals_fast,
    0
    };
