/*
 * DAWrite_types.h
 *
 * Code generation for model "DAWrite".
 *
 * Model version              : 1.11
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 11:16:14 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_DAWrite_types_h_
#define RTW_HEADER_DAWrite_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_DAWrite_T_ P_DAWrite_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_DAWrite_T RT_MODEL_DAWrite_T;

#endif                                 /* RTW_HEADER_DAWrite_types_h_ */
