/*
 * DAWrite.c
 *
 * Code generation for model "DAWrite".
 *
 * Model version              : 1.11
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 11:16:14 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "DAWrite.h"
#include "DAWrite_private.h"

/* Block signals (auto storage) */
B_DAWrite_T DAWrite_B;

/* Block states (auto storage) */
DW_DAWrite_T DAWrite_DW;

/* Real-time model */
RT_MODEL_DAWrite_T DAWrite_M_;
RT_MODEL_DAWrite_T *const DAWrite_M = &DAWrite_M_;

/* Model output function */
static void DAWrite_output(void)
{
  /* SignalConversion: '<Root>/TmpSignal ConversionAtWrite_DAInport1' incorporates:
   *  Constant: '<Root>/Constant'
   *  Constant: '<Root>/Constant1'
   *  Constant: '<Root>/Constant2'
   */
  DAWrite_B.TmpSignalConversionAtWrite_DAIn[0] = DAWrite_P.Constant_Value;
  DAWrite_B.TmpSignalConversionAtWrite_DAIn[1] = DAWrite_P.Constant1_Value;
  DAWrite_B.TmpSignalConversionAtWrite_DAIn[2] = DAWrite_P.Constant2_Value;

  /* Level2 S-Function Block: '<Root>/Write_DA' (simWriteDa) */
  {
    SimStruct *rts = DAWrite_M->childSfunctions[0];
    sfcnOutputs(rts, 0);
  }
}

/* Model update function */
static void DAWrite_update(void)
{
  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++DAWrite_M->Timing.clockTick0)) {
    ++DAWrite_M->Timing.clockTickH0;
  }

  DAWrite_M->Timing.t[0] = DAWrite_M->Timing.clockTick0 *
    DAWrite_M->Timing.stepSize0 + DAWrite_M->Timing.clockTickH0 *
    DAWrite_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void DAWrite_initialize(void)
{
  /* Level2 S-Function Block: '<Root>/Write_DA' (simWriteDa) */
  {
    SimStruct *rts = DAWrite_M->childSfunctions[0];
    sfcnStart(rts);
    if (ssGetErrorStatus(rts) != (NULL))
      return;
  }
}

/* Model terminate function */
void DAWrite_terminate(void)
{
  /* Level2 S-Function Block: '<Root>/Write_DA' (simWriteDa) */
  {
    SimStruct *rts = DAWrite_M->childSfunctions[0];
    sfcnTerminate(rts);
  }
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  DAWrite_output();
  UNUSED_PARAMETER(tid);
}

void MdlUpdate(int_T tid)
{
  DAWrite_update();
  UNUSED_PARAMETER(tid);
}

void MdlInitializeSizes(void)
{
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  DAWrite_initialize();
}

void MdlTerminate(void)
{
  DAWrite_terminate();
}

/* Registration function */
RT_MODEL_DAWrite_T *DAWrite(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)DAWrite_M, 0,
                sizeof(RT_MODEL_DAWrite_T));
  rtsiSetSolverName(&DAWrite_M->solverInfo,"FixedStepDiscrete");
  DAWrite_M->solverInfoPtr = (&DAWrite_M->solverInfo);

  /* Initialize timing info */
  {
    int_T *mdlTsMap = DAWrite_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    DAWrite_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    DAWrite_M->Timing.sampleTimes = (&DAWrite_M->Timing.sampleTimesArray[0]);
    DAWrite_M->Timing.offsetTimes = (&DAWrite_M->Timing.offsetTimesArray[0]);

    /* task periods */
    DAWrite_M->Timing.sampleTimes[0] = (0.002);

    /* task offsets */
    DAWrite_M->Timing.offsetTimes[0] = (0.0);
  }

  rtmSetTPtr(DAWrite_M, &DAWrite_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = DAWrite_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    DAWrite_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(DAWrite_M, -1);
  DAWrite_M->Timing.stepSize0 = 0.002;
  DAWrite_M->solverInfoPtr = (&DAWrite_M->solverInfo);
  DAWrite_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&DAWrite_M->solverInfo, 0.002);
  rtsiSetSolverMode(&DAWrite_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  DAWrite_M->ModelData.blockIO = ((void *) &DAWrite_B);
  (void) memset(((void *) &DAWrite_B), 0,
                sizeof(B_DAWrite_T));

  /* parameters */
  DAWrite_M->ModelData.defaultParam = ((real_T *)&DAWrite_P);

  /* states (dwork) */
  DAWrite_M->ModelData.dwork = ((void *) &DAWrite_DW);
  (void) memset((void *)&DAWrite_DW, 0,
                sizeof(DW_DAWrite_T));

  /* child S-Function registration */
  {
    RTWSfcnInfo *sfcnInfo = &DAWrite_M->NonInlinedSFcns.sfcnInfo;
    DAWrite_M->sfcnInfo = (sfcnInfo);
    rtssSetErrorStatusPtr(sfcnInfo, (&rtmGetErrorStatus(DAWrite_M)));
    rtssSetNumRootSampTimesPtr(sfcnInfo, &DAWrite_M->Sizes.numSampTimes);
    DAWrite_M->NonInlinedSFcns.taskTimePtrs[0] = &(rtmGetTPtr(DAWrite_M)[0]);
    rtssSetTPtrPtr(sfcnInfo,DAWrite_M->NonInlinedSFcns.taskTimePtrs);
    rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(DAWrite_M));
    rtssSetTFinalPtr(sfcnInfo, &rtmGetTFinal(DAWrite_M));
    rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(DAWrite_M));
    rtssSetStepSizePtr(sfcnInfo, &DAWrite_M->Timing.stepSize);
    rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(DAWrite_M));
    rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
      &DAWrite_M->ModelData.derivCacheNeedsReset);
    rtssSetZCCacheNeedsResetPtr(sfcnInfo,
      &DAWrite_M->ModelData.zCCacheNeedsReset);
    rtssSetBlkStateChangePtr(sfcnInfo, &DAWrite_M->ModelData.blkStateChange);
    rtssSetSampleHitsPtr(sfcnInfo, &DAWrite_M->Timing.sampleHits);
    rtssSetPerTaskSampleHitsPtr(sfcnInfo, &DAWrite_M->Timing.perTaskSampleHits);
    rtssSetSimModePtr(sfcnInfo, &DAWrite_M->simMode);
    rtssSetSolverInfoPtr(sfcnInfo, &DAWrite_M->solverInfoPtr);
  }

  DAWrite_M->Sizes.numSFcns = (1);

  /* register each child */
  {
    (void) memset((void *)&DAWrite_M->NonInlinedSFcns.childSFunctions[0], 0,
                  1*sizeof(SimStruct));
    DAWrite_M->childSfunctions = (&DAWrite_M->
      NonInlinedSFcns.childSFunctionPtrs[0]);
    DAWrite_M->childSfunctions[0] = (&DAWrite_M->
      NonInlinedSFcns.childSFunctions[0]);

    /* Level2 S-Function Block: DAWrite/<Root>/Write_DA (simWriteDa) */
    {
      SimStruct *rts = DAWrite_M->childSfunctions[0];

      /* timing info */
      time_T *sfcnPeriod = DAWrite_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
      time_T *sfcnOffset = DAWrite_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
      int_T *sfcnTsMap = DAWrite_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;
      (void) memset((void*)sfcnPeriod, 0,
                    sizeof(time_T)*1);
      (void) memset((void*)sfcnOffset, 0,
                    sizeof(time_T)*1);
      ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
      ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
      ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

      /* Set up the mdlInfo pointer */
      {
        ssSetBlkInfo2Ptr(rts, &DAWrite_M->NonInlinedSFcns.blkInfo2[0]);
      }

      ssSetRTWSfcnInfo(rts, DAWrite_M->sfcnInfo);

      /* Allocate memory of model methods 2 */
      {
        ssSetModelMethods2(rts, &DAWrite_M->NonInlinedSFcns.methods2[0]);
      }

      /* Allocate memory of model methods 3 */
      {
        ssSetModelMethods3(rts, &DAWrite_M->NonInlinedSFcns.methods3[0]);
      }

      /* Allocate memory for states auxilliary information */
      {
        ssSetStatesInfo2(rts, &DAWrite_M->NonInlinedSFcns.statesInfo2[0]);
      }

      /* inputs */
      {
        _ssSetNumInputPorts(rts, 1);
        ssSetPortInfoForInputs(rts,
          &DAWrite_M->NonInlinedSFcns.Sfcn0.inputPortInfo[0]);

        /* port 0 */
        {
          ssSetInputPortRequiredContiguous(rts, 0, 1);
          ssSetInputPortSignal(rts, 0, DAWrite_B.TmpSignalConversionAtWrite_DAIn);
          _ssSetInputPortNumDimensions(rts, 0, 1);
          ssSetInputPortWidth(rts, 0, 3);
        }
      }

      /* path info */
      ssSetModelName(rts, "Write_DA");
      ssSetPath(rts, "DAWrite/Write_DA");
      ssSetRTModel(rts,DAWrite_M);
      ssSetParentSS(rts, (NULL));
      ssSetRootSS(rts, rts);
      ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

      /* parameters */
      {
        mxArray **sfcnParams = (mxArray **)
          &DAWrite_M->NonInlinedSFcns.Sfcn0.params;
        ssSetSFcnParamsCount(rts, 2);
        ssSetSFcnParamsPtr(rts, &sfcnParams[0]);
        ssSetSFcnParam(rts, 0, (mxArray*)DAWrite_P.Write_DA_P1_Size);
        ssSetSFcnParam(rts, 1, (mxArray*)DAWrite_P.Write_DA_P2_Size);
      }

      /* work vectors */
      ssSetPWork(rts, (void **) &DAWrite_DW.Write_DA_PWORK[0]);

      {
        struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
          &DAWrite_M->NonInlinedSFcns.Sfcn0.dWork;
        struct _ssDWorkAuxRecord *dWorkAuxRecord = (struct _ssDWorkAuxRecord *)
          &DAWrite_M->NonInlinedSFcns.Sfcn0.dWorkAux;
        ssSetSFcnDWork(rts, dWorkRecord);
        ssSetSFcnDWorkAux(rts, dWorkAuxRecord);
        _ssSetNumDWork(rts, 1);

        /* PWORK */
        ssSetDWorkWidth(rts, 0, 2);
        ssSetDWorkDataType(rts, 0,SS_POINTER);
        ssSetDWorkComplexSignal(rts, 0, 0);
        ssSetDWork(rts, 0, &DAWrite_DW.Write_DA_PWORK[0]);
      }

      /* registration */
      simWriteDa(rts);
      sfcnInitializeSizes(rts);
      sfcnInitializeSampleTimes(rts);

      /* adjust sample time */
      ssSetSampleTime(rts, 0, 0.002);
      ssSetOffsetTime(rts, 0, 0.0);
      sfcnTsMap[0] = 0;

      /* set compiled values of dynamic vector attributes */
      ssSetNumNonsampledZCs(rts, 0);

      /* Update connectivity flags for each port */
      _ssSetInputPortConnected(rts, 0, 1);

      /* Update the BufferDstPort flags for each input port */
      ssSetInputPortBufferDstPort(rts, 0, -1);
    }
  }

  /* Initialize Sizes */
  DAWrite_M->Sizes.numContStates = (0);/* Number of continuous states */
  DAWrite_M->Sizes.numY = (0);         /* Number of model outputs */
  DAWrite_M->Sizes.numU = (0);         /* Number of model inputs */
  DAWrite_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  DAWrite_M->Sizes.numSampTimes = (1); /* Number of sample times */
  DAWrite_M->Sizes.numBlocks = (5);    /* Number of blocks */
  DAWrite_M->Sizes.numBlockIO = (1);   /* Number of block outputs */
  DAWrite_M->Sizes.numBlockPrms = (11);/* Sum of parameter "widths" */
  return DAWrite_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/
