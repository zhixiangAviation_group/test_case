/*
 * DAWrite_data.c
 *
 * Code generation for model "DAWrite".
 *
 * Model version              : 1.11
 * Simulink Coder version : 8.6 (R2014a) 27-Dec-2013
 * C source code generated on : Sat Jan 17 11:16:14 2015
 *
 * Target selection: tornado.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "DAWrite.h"
#include "DAWrite_private.h"

/* Block parameters (auto storage) */
P_DAWrite_T DAWrite_P = {
  /*  Computed Parameter: Write_DA_P1_Size
   * Referenced by: '<Root>/Write_DA'
   */
  { 1.0, 3.0 },

  /*  Expression: channelAr
   * Referenced by: '<Root>/Write_DA'
   */
  { 0.0, 2.0, 4.0 },

  /*  Computed Parameter: Write_DA_P2_Size
   * Referenced by: '<Root>/Write_DA'
   */
  { 1.0, 1.0 },
  -1.0,                                /* Expression: time
                                        * Referenced by: '<Root>/Write_DA'
                                        */
  1.3F,                                /* Computed Parameter: Constant_Value
                                        * Referenced by: '<Root>/Constant'
                                        */
  2.7F,                                /* Computed Parameter: Constant1_Value
                                        * Referenced by: '<Root>/Constant1'
                                        */
  4.5F                                 /* Computed Parameter: Constant2_Value
                                        * Referenced by: '<Root>/Constant2'
                                        */
};
