#ifndef __HOST_CMD_H
#define __HOST_CMD_H

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

#define DM642_RAM								0x00000000
#define DM642_CE0								0x80000000
#define DM642_CE1								0x90000000
#define DM642_CE2								0xA0000000
#define DM642_CE3								0xB0000000

#define TI6205_PROG_RAM							0x00000000
#define TI6205_CE0								0x00400000
#define TI6205_CE1								0x01400000
#define TI6205_CE2								0x02000000
#define TI6205_CE3								0x03000000
#define TI6205_DATA_RAM							0x80000000  

#define DM642_HOST_CMD_RAM						DM642_CE2
#define TI6205_HOST_CMD_RAM						TI6205_CE3

#define DM642_GBLCTL_VALUE						0x924a0
#define DM642_CECTL0_VALUE						0x30f30323
#define DM642_CECTL1_VALUE						0x30f30323
#define DM642_CECTL2_VALUE						0x30f30333
#define DM642_CECTL3_VALUE						0x30f30333
#define DM642_SDCTL_VALUE						0x5744F000
#define DM642_SDTIM_VALUE						0xfff

#define TI6205_GBLCTL_VALUE						0x30d9
#define TI6205_CECTL0_VALUE						0x20d20321
#define TI6205_CECTL1_VALUE						0x20d20321
#define TI6205_CECTL2_VALUE						0x20d20331
#define TI6205_CECTL3_VALUE						0x20d20331
#define TI6205_SDCTL_VALUE						0x7446000
#define TI6205_SDTIM_VALUE						0x410


struct WT_BOARD_CONFIG
{
	unsigned short int	tx429;
	unsigned short int	rx429;
	unsigned short int	uart;
	unsigned short int	tx453;
	unsigned short int	rx453;
	unsigned short int	hdlc;
	unsigned short int	sdlc;
	unsigned short int	tx717;
	unsigned short int	rx717;
	unsigned short int	tx708;
	unsigned short int	rx708;
	unsigned short int	M_1553B;
	unsigned short int	S_1553B;
	unsigned short int	da;
	unsigned short int	ad;
	unsigned short int	can;
	unsigned short int	io_out;
	unsigned short int	io_in;
	unsigned short int	txlvds;
	unsigned short int	rxlvds;
	unsigned short int	tx422;
	unsigned short int	rx422;
	//22
	
	unsigned short int	rsv[100-22];

	//28
	unsigned int	fpga_clk0_a;
	unsigned int	fpga_clk1_a;
	unsigned int	fpga_clk0_b;
	unsigned int	fpga_clk1_b;
	unsigned int	bd_LS_No;
	unsigned int	bd_SN_No;
	unsigned char	bd_name[32];
};

struct WT_DSP_Host_Cmd
{
	unsigned int	Host_Cmd_Init_Req;
	unsigned int	HostCmd_DSP_Ready;

	unsigned int	SRAM_Test_CE2_Req;
	unsigned int	SRAM_Test_CE2;

	unsigned int	SDRAM_Test_CE2_Req;
	unsigned int	SDRAM_Test_CE2;

	unsigned int	SDRAM_Test_CE3_Req;
	unsigned int	SDRAM_Test_CE3;

	unsigned int	HostCmd_Enable;
	unsigned int	Host_Data_Offset;
	//10
	
	unsigned int	FPGA_Test_Req_A;
	unsigned int	FP_ID_A;
	unsigned int	FP_CLK0_A;
	unsigned int	FP_CLK1_A;
	unsigned int	BD_TYPE_A;

	unsigned int	FPGA_Test_Req_B;
	unsigned int	FP_ID_B;
	unsigned int	FP_CLK0_B;
	unsigned int	FP_CLK1_B;
	unsigned int	BD_TYPE_B;

	unsigned int 	DSP_INT_PCI_Req;
	unsigned int 	IRQ4_Counter;
	unsigned int	IRQ5_Counter;
	unsigned int	C_T0_Counter;
	unsigned int	C_T1_Counter;
	//25
	////////////////////////////////////
	////////////////////////////////////
	////////////////////////////////////
	unsigned int	hUser_Board;
};


#endif
