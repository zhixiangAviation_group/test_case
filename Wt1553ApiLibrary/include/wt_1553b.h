#ifndef __WT_1553B_H
#define __WT_1553B_H

	#include "busapi.h"
  
#include "wtapi_type.h"
#include "host_cmd.h"
#include "host_cmd_1553b.h"
#ifdef __cplusplus 
extern "C" {
#endif

#define		Single_Function		 0  
#define		Multi_Function		 1  
#define		None_Function		-1  

UINT __stdcall WT_1553B_FindDevice(UINT cardType,UINT Instance);
UINT __stdcall WT_1553B_OpenChannel(UINT *chnd,UINT mode,UINT devid,UINT channel);
UINT __stdcall WT_1553B_Close(UINT cardnum);
UINT __stdcall WT_1553B_API_Init(UINT cardnum,UINT device);
UINT __stdcall WT_1553B_API_InitExtended(UINT cardnum,UINT device,UINT channel);

UINT __stdcall WT_1553B_SetUserHandle(UINT cardnum,API_INT_FIFO *sIntFIFO);
UINT __stdcall WT_1553B_UnSetUserHandle(UINT cardnum,API_INT_FIFO *sIntFIFO);

UINT __stdcall WT_1553B_Set_Voltage(UINT cardnum,BT_UINT voltage,BT_UINT coupling);
UINT __stdcall WT_1553B_SetSa31(UINT cardnum,UINT sa31);
UINT __stdcall WT_1553B_GetSa31(UINT cardnum,UINT *sa31);
UINT __stdcall WT_1553B_BC_Init(UINT cardnum,WORD wRetry);
UINT __stdcall WT_1553B_BC_RetryInit(UINT cardnum, WORD *bc_retry);
UINT __stdcall WT_1553B_BC_Exit(UINT cardnum);
UINT __stdcall WT_1553B_BC_Set_MBuf(UINT cardnum,struct WT_API_BC_MBUF *bc_msg);
UINT __stdcall WT_1553B_BC_Meesage_UpData(UINT cardnum,UINT block_id,unsigned short int *bc_data);
UINT __stdcall WT_1553B_BC_Get_MBuf(UINT cardnum,struct WT_API_BC_MBUF *bc_msg);
UINT __stdcall WT_1553B_BC_mFrame_Run(UINT cardnum,UINT msgid,UINT period);
UINT __stdcall WT_1553B_BC_mFrame_Stop(UINT cardnum);
UINT __stdcall WT_1553B_BC_Async_Run(UINT cardnum,UINT Priority,UINT msgid);
UINT __stdcall WT_1553B_BC_AperiodicTest(UINT cardnum,UINT Priority);
UINT __stdcall WT_1553B_BC_Get_Status(UINT cardnum,UINT *mFrame_Status,UINT *Async_Status);
UINT __stdcall WT_1553B_BC_AutoIncrMessageData(UINT cardnum,UINT messno,UINT data_wrd,UINT start, UINT incr,UINT rate,UINT max,UINT sflag);

UINT __stdcall WT_1553B_BM_Init(UINT cardnum);
UINT __stdcall WT_1553B_BM_MessageAlloc(UINT cardnum,UINT mbuf_count,UINT *mbuf_actual,UINT enable);
UINT __stdcall WT_1553B_BM_Start(UINT cardnum);
UINT __stdcall WT_1553B_BM_Stop(UINT cardnum);
UINT __stdcall WT_1553B_BM_Get_ID_Message(UINT cardnum,UINT mbuf_id,struct BM_Message *bm_msg);
UINT __stdcall WT_1553B_BM_Get_Message(UINT cardnum,struct BM_Message *bm_msg);
UINT __stdcall WT_1553B_BM_FilterWrite(UINT cardnum,UINT rtaddr,UINT subaddr,UINT tr,API_BM_CBUF * cbuf);
UINT __stdcall WT_1553B_BM_FilterRead(UINT cardnum,UINT rtaddr,UINT subaddr,UINT tr,API_BM_CBUF * cbuf);

UINT __stdcall WT_1553B_RT_Init(UINT cardnum);
UINT __stdcall WT_1553B_RT_Start(UINT cardnum);
UINT __stdcall WT_1553B_RT_Stop(UINT cardnum);
UINT __stdcall WT_1553B_RT_AbufWrite(UINT cardnum,UINT rtaddr,struct WT_API_RT_ABUF *pAbuf);
UINT __stdcall WT_1553B_RT_AbufRead(UINT cardnum,UINT rtaddr,struct WT_API_RT_ABUF *pAbuf);
UINT __stdcall WT_1553B_RT_CbufBroadWrite(UINT cardnum,UINT subaddr,UINT tr,struct WT_API_RT_CBUFBROAD *pCbufBoard);
UINT __stdcall WT_1553B_RT_CbufBroadRead(UINT cardnum,UINT subaddr,UINT tr,struct WT_API_RT_CBUFBROAD *pCbufBoard);
UINT __stdcall WT_1553B_RT_CbufWrite(UINT cardnum,UINT rdaddr,UINT subaddr,UINT tr,UINT count,struct WT_API_RT_CBUF *pCbuf);
UINT __stdcall WT_1553B_RT_CbufRead(UINT cardnum,UINT rdaddr,UINT subaddr,UINT tr,UINT *count,struct WT_API_RT_CBUF *pCbuf);
UINT __stdcall WT_1553B_RT_MessageWrite(UINT cardnum,UINT rdaddr,UINT subaddr,UINT tr,UINT msgid,struct WT_API_RT_MBUF *pmbuf);
UINT __stdcall WT_1553B_RT_MessageRead(UINT cardnum,UINT rdaddr,UINT subaddr,UINT tr,UINT msgid,struct WT_API_RT_MBUF *pmbuf);

UINT __stdcall WT_1553B_MemoryAlloc(UINT cardnum, UINT bcount,UINT * addr);
UINT __stdcall WT_1553B_MemoryWrite(UINT cardnum, UINT addr,UINT bcount,VOID * buff);
UINT __stdcall WT_1553B_MemoryRead(UINT cardnum, UINT addr,UINT bcount,VOID * buff);

UINT __stdcall WT_1553B_Set_Mode804(UINT cardnum,UINT mode);

/*
UINT __stdcall WT_1553B_BC_611_Load_TimeZone(UINT cardnum,struct wt611_modeZoneTime *timezone);
UINT __stdcall WT_1553B_BC_Set_Run_Mode(UINT cardnum,UINT run_mode);
UINT __stdcall WT_1553B_BC_Set_SubSysMode(UINT cardnum,UINT subsys_mode);
UINT __stdcall WT_1553B_BC_mFrame_Pause(UINT cardnum);
UINT __stdcall WT_1553B_BC_mFrame_Continue(UINT cardnum);
UINT __stdcall WT_1553B_GetStatusInfo(UINT cardnum, UINT rtaddr, struct wt_1553B_StatusInfo * pStatusInfo);

//返回值 有溢出=1  无溢出=0
UINT __stdcall WT_1553B_BC_GetLastMsgId(UINT cardnum,UINT *MinorFrameId,UINT *MsgId);

UINT __stdcall WT_1553B_RT_SetVectorWord(UINT cardnum,UINT rtaddr,unsigned short VectorWord);
UINT __stdcall WT_1553B_RT_GetVectorWord(UINT cardnum,UINT rtaddr,unsigned short *VectorWord);
UINT __stdcall WT_1553B_RT_SetBITWord(UINT cardnum,UINT rtaddr,unsigned short BitWord);
UINT __stdcall WT_1553B_RT_SetTransmitData(UINT cardnum,UINT rtaddr,UINT subaddr, UINT DataLen, unsigned short Data[32]);
UINT __stdcall WT_1553B_RT_UpdateTransmitData(UINT cardnum,UINT rtaddr,UINT subaddr, UINT DataLen, unsigned short Data[32]);
UINT __stdcall WT_1553B_BC_miniFrameInfo(UINT cardnum,UINT subSysmode,struct WT_API_BC_Frame_Info *miniFrameInfo);
*/
#ifdef __cplusplus 
}
#endif

#endif