#ifndef __WTAPI_LOWLEVEL_H
#define __WTAPI_LOWLEVEL_H

#ifdef __cplusplus 
extern "C" {
#endif

#define  PCI_BAR0				    0x00
#define  PCI_BAR1					0x01
#define  PCI_BAR2                   0x02
#define  PCI_BAR3                   0x03
#define  PCI_BAR4                   0x04
#define  PCI_BAR5                   0x05

#define  nMax_Lowlevle_Handle		8

struct PCI_CONFIG_HEADER_0
{
	USHORT	VendorID;
	USHORT	DeviceID;
	USHORT	Command;
	USHORT	Status;
	UCHAR	RevisionID;
	UCHAR	ProgIf;
	UCHAR	SubClass;
	UCHAR	BaseClass;
	UCHAR	CacheLineSize;
	UCHAR	LatencyTimer;
	UCHAR	HeaderType;
	UCHAR	BIST;
	ULONG	BaseAddresses[6];
	ULONG	CardBusCISPtr;
	USHORT	SubsystemVendorID;
	USHORT	SubsystemID;
	ULONG	ROMBaseAddress;
	ULONG	Reserved2[2];
	UCHAR	InterruptLine;
	UCHAR	InterruptPin;
	UCHAR	MinimumGrant;
	UCHAR	MaximumLatency;
}; 



UINT	__stdcall Wtdriver_Open(UINT device);
void	__stdcall Wtdriver_Close(UINT device);
void	__stdcall Wtdriver_SetThreadPriority(UINT device,int nPriority,DWORD dwPriorityClass);
void	__stdcall Wtdriver_Read_PciConfig(UINT device,struct PCI_CONFIG_HEADER_0 *pci_config);
UINT	__stdcall Wtdriver_IrqEnable(UINT device);
UINT	__stdcall Wtdriver_IrqDisable(UINT device);
UINT	__stdcall Wtdriver_Set_IrqEvent(UINT device);
void	__stdcall Wtdriver_UnSet_IrqEvent(UINT device);
void	__stdcall Wtdriver_Clear_IrqCount(UINT device);
UINT	__stdcall Wtdriver_Read_IrqCount(UINT device);
void	__stdcall Wtdriver_Set_m_Ready(UINT device);
UINT	__stdcall Wtdriver_Read_m_Ready(UINT device);
UINT	__stdcall Wtdriver_SetUserHandle(UINT device,WT_UserHandle hUser);
UINT	__stdcall Wtdriver_UnSetUserHandle(UINT device,WT_UserHandle hUser);
PCHAR	__stdcall Wtdriver_GetMapAddress(UINT device,UINT PCI_BAR);
UINT	__stdcall Wtdriver_Memory_Read(UINT device,PUINT ptr,UINT ofs,UINT num,UINT PCI_BAR);
UINT	__stdcall Wtdriver_Memory_Write(UINT device,PUINT ptr,UINT ofs,UINT num,UINT PCI_BAR);
UINT	__stdcall Wtdriver_IoPort_Read(UINT device,PUINT ptr,UINT ofs,UINT num,UINT PCI_BAR);
UINT	__stdcall Wtdriver_IoPort_Write(UINT device,PUINT ptr,UINT ofs,UINT num,UINT PCI_BAR);
UINT	__stdcall Wtdriver_Get_Device_Info(UINT device,struct	WT_Device_Info *pDeviceInfo);
UINT	__stdcall Wtdriver_Set_Device_Info(UINT device,struct	WT_Device_Info *pDeviceInfo);
UINT	__stdcall Wtdriver_ReadFile(UINT device,UCHAR *buf,ULONG num);
BOOL 			  WtPci_Open(struct PCI_CONFIG_HEADER_0 *pci_config,UINT8 index);
BOOL 			  WtPci_IntHandle(int DEVICE_NUM);
BOOL 			  WtPci_Initial(UINT device);
//BOOL     		  WtPci_IntConnect(UINT inum, VOIDFUNCPTR* intFun,UINT device);
int 		 	  WtPci_IntFun(UINT device);
BOOL 			  WtPci_Close(UINT device);
BOOL 			  WtPci_CloseAll(void);
UINT 			  WT_Write32(UINT device,PUINT ptr,UINT ofs,UINT num,UINT PCI_BAR);
UINT 			  WT_Read32(UINT device,PUINT ptr,UINT ofs,UINT num,UINT PCI_BAR);
extern STATUS 	   sysIntEnablePIC(int irqNo  );
extern STATUS 	   sysIntDisablePIC(int irqNo  );
#ifdef __cplusplus 
}
#endif

#endif