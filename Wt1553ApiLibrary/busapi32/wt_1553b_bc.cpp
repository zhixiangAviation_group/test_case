#include <stdlib.h>
#include <stdio.h>
#include "../include/mergeToVx.h"/*winie*/
#include "../include/busapi.h"
#include "../include/host_cmd.h"
#include "../include/host_cmd_1553b.h"

#include "../include/wt_1553b.h"

#include "../include/wtapi_type.h"
#include "../include/wtapi_lowlevel.h"

ULONG          bc_frametime[nMaxDevice*nMax_CH1553];
BT_U16BIT      read_gap_time[32][8192];
API_BC_MBUF    local_api_mbuf[nMaxDevice*nMax_CH1553][BCMsgNum];

NOMANGLE BT_INT CCONV BusTools_BC_AperiodicRun(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   messageid,     // (i) Number of BC message which begins list
   BT_UINT   Hipriority,    // (i)  1 -> Hi Priority msgs, 0 -> Low Priority msgs
   BT_UINT   WaitFlag,      // (i)  1 -> Wait for BC to complete executing msgs
   BT_UINT   WaitTime)      // (i)  Timeout in seconds(16-bit) or milliseconds(32-bit)
{
   
   BT_UINT  set_msg_id;
   set_msg_id=messageid;
   do
   {
     if(BusTools_BC_MessageWrite(cardnum, set_msg_id,&local_api_mbuf[cardnum][set_msg_id])!=API_SUCCESS) return API_BUSTOOLS_BADCARDNUM;
	 set_msg_id=local_api_mbuf[cardnum][set_msg_id].messno_next;
   }while (set_msg_id!=0xffff ); 
   if(WT_1553B_BC_Async_Run(cardnum,Hipriority,messageid)==1)	
   {
	   return API_SUCCESS;
   }else
   {
	   return API_BUSTOOLS_BADCARDNUM;
   };
}

NOMANGLE BT_INT CCONV BusTools_BC_AperiodicTest(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   Hipriority)    // (i) 1 -> Hi Priority msgs, 0 -> Low Priority msgs
{
   if(WT_1553B_BC_AperiodicTest(cardnum,Hipriority)==0)	return API_SUCCESS;
   else													return API_BC_APERIODIC_RUNNING;
}

NOMANGLE BT_INT CCONV BusTools_BC_Init(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   bPrimaryA,     // (i) Not used
   BT_U32BIT Enable,        // (i) interrupt enables
   BT_U16BIT wRetry,        // (i) retry enables
   BT_UINT   wTimeout1,     // (i) no response time out in microseconds
   BT_UINT   wTimeout2,     // (i) late response time out in microseconds
   BT_U32BIT frame,         // (i) minor frame period, in microseconds
   BT_UINT   num_buffers)   // (i) number of BC message buffers ( 1 or 2 )
{
   if( WT_1553B_BC_Init(cardnum,wRetry)==1)
   {	   
      bc_frametime[cardnum]=frame;
	  WT_1553B_Set_Voltage(cardnum,0x7ff,0);
	  memset(local_api_mbuf,0,sizeof(API_BC_MBUF)*nMaxDevice*nMax_CH1553*BCMsgNum);
      return API_SUCCESS;
   }else
   {
      return API_BUSTOOLS_NOTINITED;
   };
}


NOMANGLE BT_INT CCONV BusTools_BC_RetryInit(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_U16BIT *bc_retry)     //
{
    WT_1553B_BC_RetryInit(cardnum,bc_retry);
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_IsRunning(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT * flag)          // (o) Returns 1 if running, 0 if not
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageAlloc(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT count)           // (i) Number of BC messages to allocate
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageGetaddr(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT messageid,       // (i) Number of BC message to compute address
   BT_U32BIT * addr)    // (o) Returned byte address of BC message
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageGetid(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_U32BIT addr,          // (i) Byte address of BC message
   BT_UINT * messageid) // (o) Number of the BC message
{
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageNoop(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT messageid,       // (i) Number of BC message to modify
   BT_UINT NoopFlag,        // (i) Flag, 1 -> Set to NOOP, 0 -> Return to msg
   BT_UINT WaitFlag)        // (i) Flag, 1 -> Wait for BC to not be executing msg,
                            //           0 -> Do not test for BC executing msg.

{
   static WORD control[32][8192];
   WT_API_BC_MBUF  bc_get_msg;
   bc_get_msg.messno = messageid;
   WT_1553B_BC_Get_MBuf(cardnum,&bc_get_msg);
   
   if(NoopFlag)
   {
	  control[cardnum][bc_get_msg.messno] = bc_get_msg.control;
	  bc_get_msg.control&= ~0x0007;
   }
   else
   {
	 bc_get_msg.control|=control[cardnum][bc_get_msg.messno];
   }
   
   WT_1553B_BC_Set_MBuf(cardnum,&bc_get_msg); 
  
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_ControlWordUpdate(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT messageid,       // (i) Number of BC message to modify
   BT_U16BIT controlWord,   // (i) New Control Word settings
   BT_UINT WaitFlag)        // (i) Flag, 1 -> Wait for BC to not be executing msg,
                            //           0 -> Do not test for BC executing msg.
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageRead(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT mblock_id,       // (i) Number of BC message
   API_BC_MBUF * api_mbuf) // (i) Pointer to buffer to receive msg
{
    struct WT_API_BC_MBUF bc_get_msg;
    bc_get_msg.messno=mblock_id;
	WT_1553B_BC_Get_MBuf(cardnum,&bc_get_msg);
	*((BT_U16BIT *)(&api_mbuf->mess_command1))=bc_get_msg.mess_cmd1;
	*((BT_U16BIT *)(&api_mbuf->mess_command2))=bc_get_msg.mess_cmd2;
	for(int i=0;i<32;i++)
	api_mbuf->data[0][i]=bc_get_msg.data[0][i];
	api_mbuf->messno =bc_get_msg.messno;
	api_mbuf->messno_next =bc_get_msg.messno_next;
	*((BT_U16BIT *)(&api_mbuf->mess_status1))=bc_get_msg.mess_status1;
	*((BT_U16BIT *)(&api_mbuf->mess_status2))=bc_get_msg.mess_status2;
	api_mbuf->gap_time =bc_get_msg.gap_time;// +10;
    api_mbuf->status=BT1553_INT_END_OF_MESS; 
	api_mbuf->control=local_api_mbuf[cardnum][mblock_id].control;  //bc_get_msg.control;//  BC_CONTROL_MESSAGE  ;
	api_mbuf->address=bc_get_msg.address;
	api_mbuf->data_mask = bc_get_msg.data_mask;
    api_mbuf->data_value = bc_get_msg.data_value;
	api_mbuf->messno_branch = bc_get_msg.messno_branch;
    api_mbuf->messno_compare = bc_get_msg.messno_compare;
    api_mbuf->spare = 0;//bc_get_msg.control_611;
	int BusA_B;
    BusA_B=bc_get_msg.status&0X3;
    if(BusA_B ==3)
    {
	  api_mbuf->status = api_mbuf->status| BT1553_INT_TWO_BUS;
    } 
    if(BusA_B ==2)
    {
		api_mbuf->status = api_mbuf->status|BT1553_INT_CHANNEL;
//		api_mbuf->control=api_mbuf->control|BC_CONTROL_CHANNELB;
    } 
	if(BusA_B ==1)
    {
		api_mbuf->status = api_mbuf->status&(~BT1553_INT_CHANNEL);
//		api_mbuf->control=api_mbuf->control|BC_CONTROL_CHANNELA;
    } 
	if((bc_get_msg.status&0X0200) !=0)
	{
       api_mbuf->status = api_mbuf->status|BT1553_INT_NO_RESP;
	}
	if ((bc_get_msg.msg_type == Non_Broadcast_RT_To_RT)||(bc_get_msg.msg_type == Broadcast_RT_To_RT))
    {
        api_mbuf->control =api_mbuf->control|BC_CONTROL_RTRTFORMAT;
		api_mbuf->status = api_mbuf->status|BT1553_INT_RT_RT_FORMAT;
	}
   	if(((bc_get_msg.status&0X0100) !=0)&&(bc_get_msg.msg_type == Non_Broadcast_RT_To_RT))
	{
        api_mbuf->status = api_mbuf->status|BT1553_INT_NO_RESP;
	}
   	if(((bc_get_msg.status&0X0100) !=0)&&(bc_get_msg.msg_type == Broadcast_RT_To_RT))
	{
        api_mbuf->status = api_mbuf->status|BT1553_INT_NO_RESP;
	}

	if ( (bc_get_msg.msg_type ==  Non_Broadcast_Mode_With_Data_Recv)
		|| (bc_get_msg.msg_type == Non_Broadcast_Mode_With_Data_Send)
		|| (bc_get_msg.msg_type == Non_Broadcast_Mode_Non_Data)
		|| (bc_get_msg.msg_type == Broadcast_Mode_With_Data_Recv)
		|| (bc_get_msg.msg_type == Broadcast_Mode_Non_Data)
		)
	{
		api_mbuf->status |= BT1553_INT_MODE_CODE;
	}
	if ( (bc_get_msg.msg_type ==  Broadcast_BC_To_RT)
			|| (bc_get_msg.msg_type == Broadcast_RT_To_RT)
			|| (bc_get_msg.msg_type == Broadcast_Mode_With_Data_Recv)
			|| (bc_get_msg.msg_type == Broadcast_Mode_Non_Data)
			)
	{
		api_mbuf->status |= BT1553_INT_BROADCAST;
	}

     return API_SUCCESS;

	//		"00000" & transout & respout1 & respout2 & bus_error & cmd1_ready & cmd2_ready & status1_ready & status2_ready & data_ready & busb_flag & busa_flag;

}

NOMANGLE BT_INT CCONV BusTools_BC_MessageReadData(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   mblock_id,     // (i) number of the BC message
   BT_U16BIT * buffer)  // (o) pointer to user's data buffer
{
     struct WT_API_BC_MBUF bc_get_msg;

    bc_get_msg.messno=mblock_id;
	WT_1553B_BC_Get_MBuf(cardnum,&bc_get_msg);
	memcpy(buffer,bc_get_msg.data,32*sizeof(unsigned short int));
    return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageUpdate(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   mblock_id,     // (i) BC Message number
   BT_U16BIT * buffer)  // (i) pointer to user's data buffer
{
	memcpy(&local_api_mbuf[cardnum][mblock_id].data[0], buffer, 32*sizeof(unsigned short int));
	return WT_1553B_BC_Meesage_UpData(cardnum,mblock_id,buffer);
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageReadDataBuffer(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   mblock_id,     // (i) number of the BC message
   BT_UINT   buffer_id,      // (i) Buffer ID 0=A 1=B
   BT_U16BIT * buffer)      // (o) pointer to user's data buffer
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageUpdateBuffer(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   mblock_id,     // (i) BC Message number
   BT_UINT   buffer_id,     // (i) BC data buffer 0=A 1=B
   BT_U16BIT * buffer)      // (i) pointer to user's data buffer
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_MessageWrite(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   mblock_id,     // (i) BC Message number
   API_BC_MBUF * api_message)  // (i) pointer to user's buffer containing msg
{
    struct WT_API_BC_MBUF bc_set_msg;
    local_api_mbuf[cardnum][mblock_id]=*api_message;

	UINT sa31;
	WT_1553B_GetSa31(cardnum,&sa31);
    bc_set_msg.control = api_message->control;
	bc_set_msg.messno_branch = api_message->messno_branch ;
    bc_set_msg.address = api_message->address;
    bc_set_msg.data_mask = api_message->data_mask ;
    bc_set_msg.data_value = api_message->data_value;
    bc_set_msg.messno_compare = api_message->messno_compare ;
    bc_set_msg.mess_cmd1 = *((BT_U16BIT *)(&api_message->mess_command1)); 
    bc_set_msg.mess_cmd2 = *((BT_U16BIT *)(&api_message->mess_command2)); 
    bc_set_msg.mess_status1 = *((BT_U16BIT *)(&api_message->mess_status1)); 
    bc_set_msg.mess_status2 = *((BT_U16BIT *)(&api_message->mess_status2)); 
	for(int i=0;i<32;i++)bc_set_msg.data[0][i]=(unsigned short int)api_message->data[0][i];
	api_message->messno=mblock_id;
	bc_set_msg.messno=api_message->messno;
	bc_set_msg.messno_next=api_message->messno_next;
	
	if(bc_set_msg.gap_time<10)
	{
		 bc_set_msg.gap_time=10;
    }
	bc_set_msg.gap_time=api_message->gap_time;

	if((api_message->control&BC_CONTROL_RTRTFORMAT)==0)
    {
      if(api_message->mess_command1.tran_rec==0)
	  {
        bc_set_msg.msg_type=Non_Broadcast_BC_To_RT;
        if(api_message->mess_command1.subaddr==0)
		{
           bc_set_msg.msg_type= Non_Broadcast_Mode_With_Data_Recv;
		}
       if((api_message->mess_command1.subaddr==31)&&(sa31==1))
		{
           bc_set_msg.msg_type= Non_Broadcast_Mode_With_Data_Recv;
		}
        if(api_message->mess_command1.rtaddr==31)
		{
          bc_set_msg.msg_type=Broadcast_BC_To_RT;
          if(api_message->mess_command1.subaddr==0)
		  {
            bc_set_msg.msg_type= Broadcast_Mode_With_Data_Recv;
		  }
          if((api_message->mess_command1.subaddr==31)&&(sa31==1))
		  {
            bc_set_msg.msg_type= Broadcast_Mode_With_Data_Recv;
		  }
		}
	  }else if(api_message->mess_command1.tran_rec==1)
	  {
         bc_set_msg.msg_type=Non_Broadcast_RT_To_BC;
         if((api_message->mess_command1.subaddr==0)||(api_message->mess_command1.subaddr==31))
		 {
           if(api_message->mess_command1.wcount>0xf)
		   {
              bc_set_msg.msg_type = Non_Broadcast_Mode_With_Data_Send;
		   }else
		   {
              bc_set_msg.msg_type = Non_Broadcast_Mode_Non_Data;
		   }
		 }
         if(api_message->mess_command1.rtaddr==31)
		 {
//        BCFormMain->BCMsgG[i].msg_type=Broadcast_BC_To_RT;
            if((api_message->mess_command1.subaddr==0)||(api_message->mess_command1.subaddr==31))
			{
               bc_set_msg.msg_type= Broadcast_Mode_Non_Data;
			}
		 }
	  }
	}else
    {
      bc_set_msg.msg_type=Non_Broadcast_RT_To_RT;
      if(api_message->mess_command1.rtaddr==31)
        bc_set_msg.msg_type=Broadcast_RT_To_RT;
    }
	//bc_set_msg.control_611=api_message->spare;
	WT_1553B_BC_Set_MBuf(cardnum,&bc_set_msg);
    return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_AutoIncrMessageData(BT_INT cardnum,BT_INT messno,BT_INT data_wrd,
                                                      BT_U16BIT start, BT_U16BIT incr, 
                                                      BT_INT rate, BT_U16BIT max, BT_INT sflag)
{
	return WT_1553B_BC_AutoIncrMessageData(cardnum,messno,data_wrd,start,incr,rate,max,sflag);
}

NOMANGLE BT_INT CCONV BusTools_BC_OneShotExecute(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT count,
   API_BC_MBUF* messages_orig,
   BT_UINT waitflag)
{
    struct WT_API_BC_MBUF bc_set_msg;
	UINT sa31;
	WT_1553B_GetSa31(cardnum,&sa31);     //获取子地址31为方式码 sa31=1，普通子地址sa31=0；

    for( BT_UINT j=0;j<count;j++)
    {
 //     local_api_mbuf[cardnum][j]=*messages_orig;

	  bc_set_msg.control = messages_orig->control;
      if(j==0)   bc_set_msg.control|=BC_CONTROL_MFRAME_BEG;     
	  bc_set_msg.messno_branch = messages_orig->messno_branch ;
      bc_set_msg.address = messages_orig->address;
      bc_set_msg.data_mask = messages_orig->data_mask ;
      bc_set_msg.data_value = messages_orig->data_value;
      bc_set_msg.messno_compare = messages_orig->messno_compare ;
      bc_set_msg.mess_cmd1 = *((BT_U16BIT *)(&messages_orig->mess_command1)); 
      bc_set_msg.mess_cmd2 = *((BT_U16BIT *)(&messages_orig->mess_command2)); 
      bc_set_msg.mess_status1 = *((BT_U16BIT *)(&messages_orig->mess_status1)); 
      bc_set_msg.mess_status2 = *((BT_U16BIT *)(&messages_orig->mess_status2)); 
	  for(int i=0;i<32;i++)bc_set_msg.data[0][i]=(unsigned short int)messages_orig->data[0][i];
	  bc_set_msg.messno=j;
	  bc_set_msg.messno_next=j+1;
	  if(j==(count-1)) 
      {	
		  bc_set_msg.messno_next=0xffff;
		  bc_set_msg.control|=BC_CONTROL_MFRAME_END;     
	  }; 
	  if(bc_set_msg.gap_time<10)
	  {
		 bc_set_msg.gap_time=10;
	  }
	  bc_set_msg.gap_time=messages_orig->gap_time-2;

	  if((messages_orig->control&BC_CONTROL_RTRTFORMAT)==0)
	  {
        if(messages_orig->mess_command1.tran_rec==0)
		{
          bc_set_msg.msg_type=Non_Broadcast_BC_To_RT;
          if(messages_orig->mess_command1.subaddr==0)
		  {
            bc_set_msg.msg_type= Non_Broadcast_Mode_With_Data_Recv;
		  }
          if((messages_orig->mess_command1.subaddr==31)&&(sa31==1))
		  {
            bc_set_msg.msg_type= Non_Broadcast_Mode_With_Data_Recv;
		  }
          if(messages_orig->mess_command1.rtaddr==31)
		  {
            bc_set_msg.msg_type=Broadcast_BC_To_RT;
            if(messages_orig->mess_command1.subaddr==0)
			{
              bc_set_msg.msg_type= Broadcast_Mode_With_Data_Recv;
			}
            if((messages_orig->mess_command1.subaddr==31)&&(sa31==1))
			{
              bc_set_msg.msg_type= Broadcast_Mode_With_Data_Recv;
			}
		  }
		}else if(messages_orig->mess_command1.tran_rec==1)
		{
         bc_set_msg.msg_type=Non_Broadcast_RT_To_BC;
         if((messages_orig->mess_command1.subaddr==0)||(messages_orig->mess_command1.subaddr==31))
		 {
           if(messages_orig->mess_command1.wcount>0xf)
		   {
              bc_set_msg.msg_type = Non_Broadcast_Mode_With_Data_Send;
		   }else
		   {
              bc_set_msg.msg_type = Non_Broadcast_Mode_Non_Data;
		   }
		 }
         if(messages_orig->mess_command1.rtaddr==31)
		 {
//        BCFormMain->BCMsgG[i].msg_type=Broadcast_BC_To_RT;
            if((messages_orig->mess_command1.subaddr==0)||(messages_orig->mess_command1.subaddr==31))
			{
               bc_set_msg.msg_type= Broadcast_Mode_Non_Data;
			}
		 }
		}
	  }else
	  {
        bc_set_msg.msg_type=Non_Broadcast_RT_To_RT;
        if(messages_orig->mess_command1.rtaddr==31)
        bc_set_msg.msg_type=Broadcast_RT_To_RT;
	  }
	//bc_set_msg.control_611=api_message->spare;
 	  WT_1553B_BC_Set_MBuf(cardnum,&bc_set_msg);
	  messages_orig++;
    }
   WT_1553B_BC_Async_Run(cardnum,0,0);	 
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_OneShotInit(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT channel,
   BT_UINT timeresp,
   BT_UINT timelate)
{
   if( WT_1553B_BC_Init(cardnum,0)==1)
   {	   
      bc_frametime[cardnum]=10000000;
	  WT_1553B_Set_Voltage(cardnum,0x7ff,0);
	  memset(local_api_mbuf,0,sizeof(API_BC_MBUF)*nMaxDevice*nMax_CH1553*BCMsgNum);
      return API_SUCCESS;
   }else
   {
      return API_BUSTOOLS_NOTINITED;
   };

	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_SetFrameRate(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_U32BIT frame_time)    // (i) New Frame Time in uSecs
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_Start(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT mblock_id)       // (i) BC Message number to start processing at
{
  WT_1553B_BC_mFrame_Run(cardnum,mblock_id,bc_frametime[cardnum]);
  return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_StartStop(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT startflag)       // (i) flag=1 to start the BC, 0 to stop it
{
    if(startflag)
	{
       WT_1553B_BC_mFrame_Run(cardnum,0,bc_frametime[cardnum]);
	}else
	{
	   WT_1553B_BC_mFrame_Stop(cardnum);
	};
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_Trigger(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_INT    trigger_mode)  // (i0 BC_TRIGGER_IMMEDIATE - BC starts immediately.
                            //  -> default              - BC starts immediately.
                            //  -> BC_TRIGGER_ONESHOT   - BC trig ext TTL source.
                            //  -> BC_TRIGGER_REPETITIVE- Each minor frame is
                            //                            started by the TTL trig.
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_Checksum1760(API_BC_MBUF *mbuf, BT_U16BIT *cksum)
{
	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_ReadNextMessage(int cardnum,BT_UINT timeout,BT_INT rt_addr,
									   BT_INT subaddress,BT_INT tr, API_BC_MBUF *pBC_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_ReadLastMessage(int cardnum,BT_INT rt_addr,
									   BT_INT subaddress,BT_INT tr, API_BC_MBUF *pBC_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BC_ReadLastMessageBlock(int cardnum,BT_INT rt_addr_mask, BT_INT subaddr_mask,
                                                       BT_INT tr, BT_UINT *mcount,API_BC_MBUF *pBC_mbuf)
{
   return API_SUCCESS;
}



