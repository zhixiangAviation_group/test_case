
{
	static struct WT_IRQ_Fifo_Info	 pINTFIFO[64];
	static API_INT_FIFO	 *pFIFO;
	static UINT i,j,k,num;
	static UINT rdidx,wridx;
	static UINT run_flag[nMaxHandle];

	if(wt_1553B[HANDLE_IDX])
	{
		num=0;

		for(i=0;i<64;i++)
		{
			rdidx=irqfifo_rdidx[HANDLE_IDX];
			wridx=wt_1553B[HANDLE_IDX]->irqfifo_wridx;
			if(wridx!=rdidx)
			{
				memcpy(&pINTFIFO[i],(const void *)&irqfifo[HANDLE_IDX][rdidx],sizeof(struct WT_IRQ_Fifo_Info));
				rdidx = (rdidx+1)&IrqMask;
				irqfifo_rdidx[HANDLE_IDX]=rdidx;
				num++;
			}
			else break;
		}

		if(num>0)
		{
			for(k=0;k<nMaxHandle;k++) run_flag[k]=0;
			for(j=0;j<num;j++)
			{
				for(k=0;k<nMaxHandle;k++)
				{
					pFIFO=UserParam[HANDLE_IDX][k];
					if(pFIFO)
					{
						switch(pINTFIFO[j].event_type)
						{
							case		EVENT_BC_MESSAGE:	
										if((pFIFO->FilterType&pINTFIFO[j].event_type)==EVENT_BC_MESSAGE)
										{
											if(pFIFO->FilterMask[pINTFIFO[j].rtaddress][pINTFIFO[j].transrec][pINTFIFO[j].subaddress])
											{
												memcpy(&pFIFO->fifo[pFIFO->head_index],&pINTFIFO[j],sizeof(struct WT_IRQ_Fifo_Info));
												pFIFO->head_index=(pFIFO->head_index+1)%MAX_FIFO_LEN;
												run_flag[k]=1;
											}
										}
										break;

							case		EVENT_RT_MESSAGE:	
										if((pFIFO->FilterType&pINTFIFO[j].event_type)==EVENT_RT_MESSAGE)
										{
											if(pFIFO->FilterMask[pINTFIFO[j].rtaddress][pINTFIFO[j].transrec][pINTFIFO[j].subaddress])
											{
												memcpy(&pFIFO->fifo[pFIFO->head_index],&pINTFIFO[j],sizeof(struct WT_IRQ_Fifo_Info));
												pFIFO->head_index=(pFIFO->head_index+1)%MAX_FIFO_LEN;
												run_flag[k]=1;
											}
										}
										break;

							case		EVENT_BM_MESSAGE:
										if((pFIFO->FilterType&pINTFIFO[j].event_type)==EVENT_BM_MESSAGE)
										{
											if(pFIFO->FilterMask[pINTFIFO[j].rtaddress][pINTFIFO[j].transrec][pINTFIFO[j].subaddress])
											{
												memcpy(&pFIFO->fifo[pFIFO->head_index],&pINTFIFO[j],sizeof(struct WT_IRQ_Fifo_Info));
												pFIFO->head_index=(pFIFO->head_index+1)%MAX_FIFO_LEN;
												run_flag[k]=1;
											}
										}
										break;

							case		EVENT_RECORDER:
										if((pFIFO->FilterType&pINTFIFO[j].event_type)==EVENT_RECORDER)
										{
											memcpy(&pFIFO->fifo[pFIFO->head_index],&pINTFIFO[j],sizeof(struct WT_IRQ_Fifo_Info));
											pFIFO->head_index=(pFIFO->head_index+1)%MAX_FIFO_LEN;
											run_flag[k]=1;
										}
										break;

							default:	break;
						}
					}
				}
			}
			for(k=0;k<nMaxHandle;k++) 
			{
				if(run_flag[k]==1)
				{
					pUserHandle[HANDLE_IDX][k](HANDLE_IDX,UserParam[HANDLE_IDX][k]);
				}
			}
		}
	}	
	return 0;
}
