
#include <stdlib.h>
#include <stdio.h>
#include "../include/mergeToVx.h"/*winie*/

#include "../include/wt_1553b.h"

#include "../include/wtapi_type.h"
#include "../include/wtapi_lowlevel.h"
#include "sxpdiv64.c"
NOMANGLE BT_INT CCONV BusTools_BM_FilterRead(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT rtaddr,
   BT_UINT subaddr,
   BT_UINT tr,
   API_BM_CBUF * api_cbuf)
{
	if(WT_1553B_BM_FilterRead(cardnum,rtaddr,subaddr,tr,api_cbuf)==WT_SUCCESS)return API_SUCCESS;
	else	  return API_BM_NOTRUNNING;
}

NOMANGLE BT_INT CCONV BusTools_BM_FilterWrite(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   rtaddr,
   BT_UINT   subaddr,
   BT_UINT   tr,
   API_BM_CBUF * api_cbuf)
{
	if(WT_1553B_BM_FilterWrite(cardnum,rtaddr,subaddr,tr,api_cbuf)==WT_SUCCESS)return API_SUCCESS;
	else	  return API_BM_NOTRUNNING;
}

NOMANGLE BT_INT CCONV BusTools_BM_Init(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT enable_a,
   BT_UINT enable_b )
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_MessageAlloc(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT   mbuf_count,
   BT_UINT   * mbuf_actual,
   BT_U32BIT enable)
{
	WT_1553B_BM_MessageAlloc(cardnum,mbuf_count,mbuf_actual,enable);
    return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_MessageGetaddr(
   BT_UINT   cardnum,           // (i) card number (0 based)
   BT_UINT mbuf_id,
   BT_U32BIT * addr)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_MessageGetid(
   BT_UINT   cardnum,           // (i) card number (0 based)
   BT_U32BIT     addr,
   BT_UINT * messageid)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_MessageRead(
   BT_UINT   cardnum,           // (i) card number (0 based)
   BT_UINT   mbuf_id,
   API_BM_MBUF * api_mbuf)
{
	struct BM_Message BMmsg;

	WT_1553B_BM_Get_ID_Message(cardnum,mbuf_id,&BMmsg);

		api_mbuf->command1.rtaddr = (BMmsg.msg.cmd1>>11)&0x1f;
		api_mbuf->command1.subaddr = (BMmsg.msg.cmd1>>5)&0x1f;
		api_mbuf->command1.tran_rec = (BMmsg.msg.cmd1>>10)&1;
		api_mbuf->command1.wcount = BMmsg.msg.cmd1&0x1f;
		api_mbuf->command2.rtaddr = (BMmsg.msg.cmd2>>11)&0x1f;
		api_mbuf->command2.subaddr = (BMmsg.msg.cmd2>>5)&0x1f;
		api_mbuf->command2.tran_rec = (BMmsg.msg.cmd2>>10)&1;
		api_mbuf->command2.wcount = BMmsg.msg.cmd2&0x1f;
		api_mbuf->response1.time=0;//(unsigned char)(2.0*BMmsg.resp_time1); 

		BMmsg.msg.tag=__div64(BMmsg.msg.tag,24);

		api_mbuf->time.microseconds = (BT_U32BIT)((BMmsg.msg.tag)&0xffffffff);
		api_mbuf->time.topuseconds = (BT_U16BIT)((BMmsg.msg.tag>>32)&0xffff);
        api_mbuf->response2.time = 0;//(unsigned char)(2.0*BMmsg.resp_time2);
		
		for(int i=0;i<32;i++)
		api_mbuf->value[i]=BMmsg.msg.data[i];
		api_mbuf->messno =BMmsg.msg.msgno ;
        api_mbuf->status1.busy =(BMmsg.msg.status1>>3)&1;   
        api_mbuf->status1.bcr=(BMmsg.msg.status1>>4)&1;   
        api_mbuf->status1.dba =(BMmsg.msg.status1>>1)&1;   
        api_mbuf->status1.me =(BMmsg.msg.status1>>10)&1;   
        api_mbuf->status1.rtaddr =(BMmsg.msg.status1>>11)&0x1f;   
        api_mbuf->status1.res =(BMmsg.msg.status1>>5)&7;   
        api_mbuf->status1.sf  =(BMmsg.msg.status1>>2)&1;   
        api_mbuf->status1.sr =(BMmsg.msg.status1>>8)&1;   
        api_mbuf->status1.tf =BMmsg.msg.status1&1;   
        api_mbuf->status1.inst = (BMmsg.msg.status1>>9)&1;

	  api_mbuf->status2.busy =(BMmsg.msg.status2>>3)&1;   
        api_mbuf->status2.bcr=(BMmsg.msg.status2>>4)&1;   
        api_mbuf->status2.dba =(BMmsg.msg.status2>>1)&1;   
        api_mbuf->status2.me =(BMmsg.msg.status2>>10)&1;   
        api_mbuf->status2.rtaddr =(BMmsg.msg.status2>>11)&0x1f;   
        api_mbuf->status2.res =(BMmsg.msg.status2>>5)&7;   
        api_mbuf->status2.sf  =(BMmsg.msg.status2>>2)&1;   
        api_mbuf->status2.sr =(BMmsg.msg.status2>>8)&1;   
        api_mbuf->status2.tf =BMmsg.msg.status2&1;   
        api_mbuf->status2.inst = (BMmsg.msg.status2>>9)&1;

		api_mbuf->int_status =BT1553_INT_END_OF_MESS;
		int BusA_B;
        BusA_B=BMmsg.msg.msgflag&0X3;
		if(BusA_B ==2)     //busB
        {
			api_mbuf->int_status =api_mbuf->int_status|BT1553_INT_CHANNEL;
        } 
		if(BusA_B ==1)      //busA
        {
			api_mbuf->int_status =api_mbuf->int_status&(~BT1553_INT_CHANNEL);
        } 
		if(BusA_B ==3)
        {
           api_mbuf->int_status=api_mbuf->int_status|BT1553_INT_TWO_BUS;
        } 
		if((BMmsg.msg.msgflag&0X0200) !=0)
		{
           api_mbuf->int_status=api_mbuf->int_status|BT1553_INT_NO_RESP;
		}
		if ((BMmsg.msg.msgtype == Non_Broadcast_RT_To_RT)||(BMmsg.msg.msgtype == Broadcast_RT_To_RT))
        {
            api_mbuf->int_status = api_mbuf->int_status|BT1553_INT_RT_RT_FORMAT;
		}
     	if(((BMmsg.msg.msgflag&0X0100) !=0)&&(BMmsg.msg.msgtype == Non_Broadcast_RT_To_RT))
		{
            api_mbuf->int_status=api_mbuf->int_status|BT1553_INT_NO_RESP;
		}
     	if(((BMmsg.msg.msgflag&0X0100) !=0)&&(BMmsg.msg.msgtype == Broadcast_RT_To_RT))
		{
            api_mbuf->int_status=api_mbuf->int_status|BT1553_INT_NO_RESP;
		}

		if ( (BMmsg.msg.msgtype ==  Non_Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Non_Broadcast_Mode_With_Data_Send)
			|| (BMmsg.msg.msgtype == Non_Broadcast_Mode_Non_Data)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_Non_Data)
			)
		{
			api_mbuf->int_status |= BT1553_INT_MODE_CODE;
		}

		if ( (BMmsg.msg.msgtype ==  Broadcast_BC_To_RT)
			|| (BMmsg.msg.msgtype == Broadcast_RT_To_RT)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_Non_Data)
			)
		{
			api_mbuf->int_status |= BT1553_INT_BROADCAST;
		}

	return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_MessageReadBlock
(
   BT_UINT   cardnum,           // (i) card number (0 based)
   API_BM_MBUF * api_mbuf,		// (i) address of caller's array of BM_MBUF's
   BT_UINT size,                // (i) number of BM_MBUF's in array
   BT_UINT curpos,              // (i) next avail location in array
   BT_UINT * ret_count)			// (o) number of BM_MBUF's transferred
{	
	struct BM_Message BMmsg;
	API_BM_MBUF * pBM;	
	BT_UINT MsgLength=0;
	BT_INT rst=API_SUCCESS;

	pBM=&api_mbuf[curpos];

	int rv = WT_1553B_BM_Get_Message(cardnum,&BMmsg);
    while(rv==WT_SUCCESS)
	{
		pBM->command1.rtaddr =(BMmsg.msg.cmd1>>11)&0x1f;
		pBM->command1.subaddr =(BMmsg.msg.cmd1>>5)&0x1f;
		pBM->command1.tran_rec =(BMmsg.msg.cmd1>>10)&1;
		pBM->command1.wcount =BMmsg.msg.cmd1&0x1f;
		pBM->command2.rtaddr =(BMmsg.msg.cmd2>>11)&0x1f;
		pBM->command2.subaddr =(BMmsg.msg.cmd2>>5)&0x1f;
		pBM->command2.tran_rec =(BMmsg.msg.cmd2>>10)&1;
		pBM->command2.wcount =BMmsg.msg.cmd2&0x1f;
		pBM->response1.time=0;//(unsigned char)(2.0*BMmsg.resp_time1); //winie

		BMmsg.msg.tag=__div64(BMmsg.msg.tag,24);//winie

		pBM->time.microseconds=(BT_U32BIT)((BMmsg.msg.tag)&0xffffffff);
		pBM->time.topuseconds=(BT_U16BIT)((BMmsg.msg.tag>>32)&0xffff);
        pBM->response2.time=0;//(unsigned char)(2.0*BMmsg.resp_time2);//winie
		
		for(int i=0;i<32;i++)pBM->value[i]=BMmsg.msg.data[i];
		pBM->messno =BMmsg.msg.msgno ;
        pBM->status1.busy =(BMmsg.msg.status1>>3)&1;   
        pBM->status1.bcr=(BMmsg.msg.status1>>4)&1;   
        pBM->status1.dba =(BMmsg.msg.status1>>1)&1;   
        pBM->status1.me =(BMmsg.msg.status1>>10)&1;   
        pBM->status1.rtaddr =(BMmsg.msg.status1>>11)&0x1f;   
        pBM->status1.res =(BMmsg.msg.status1>>5)&7;   
        pBM->status1.sf  =(BMmsg.msg.status1>>2)&1;   
        pBM->status1.sr =(BMmsg.msg.status1>>8)&1;   
        pBM->status1.tf =BMmsg.msg.status1&1;   
        pBM->status1.inst = (BMmsg.msg.status1>>9)&1;

	  pBM->status2.busy =(BMmsg.msg.status2>>3)&1;   
        pBM->status2.bcr=(BMmsg.msg.status2>>4)&1;   
        pBM->status2.dba =(BMmsg.msg.status2>>1)&1;   
        pBM->status2.me =(BMmsg.msg.status2>>10)&1;   
        pBM->status2.rtaddr =(BMmsg.msg.status2>>11)&0x1f;   
        pBM->status2.res =(BMmsg.msg.status2>>5)&7;   
        pBM->status2.sf  =(BMmsg.msg.status2>>2)&1;   
        pBM->status2.sr =(BMmsg.msg.status2>>8)&1;   
        pBM->status2.tf =BMmsg.msg.status2&1;   
        pBM->status2.inst = (BMmsg.msg.status2>>9)&1;

		pBM->int_status =BT1553_INT_END_OF_MESS;
		int BusA_B;
        BusA_B=BMmsg.msg.msgflag&0X3;
		if(BusA_B ==2)     //busB
        {
			pBM->int_status =pBM->int_status|BT1553_INT_CHANNEL;
        } 
		if(BusA_B ==1)      //busA
        {
			pBM->int_status =pBM->int_status&(~BT1553_INT_CHANNEL);
        } 
		if(BusA_B ==3)
        {
           pBM->int_status=pBM->int_status|BT1553_INT_TWO_BUS;
        } 
		if((BMmsg.msg.msgflag&0X0200) !=0)
		{
           pBM->int_status=pBM->int_status|BT1553_INT_NO_RESP;
		}
		if ((BMmsg.msg.msgtype == Non_Broadcast_RT_To_RT)||(BMmsg.msg.msgtype == Broadcast_RT_To_RT))
        {
            pBM->int_status = pBM->int_status|BT1553_INT_RT_RT_FORMAT;
		}
     	if(((BMmsg.msg.msgflag&0X0100) !=0)&&(BMmsg.msg.msgtype == Non_Broadcast_RT_To_RT))
		{
            pBM->int_status=pBM->int_status|BT1553_INT_NO_RESP;
		}
     	if(((BMmsg.msg.msgflag&0X0100) !=0)&&(BMmsg.msg.msgtype == Broadcast_RT_To_RT))
		{
            pBM->int_status=pBM->int_status|BT1553_INT_NO_RESP;
		}

		if ( (BMmsg.msg.msgtype ==  Non_Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Non_Broadcast_Mode_With_Data_Send)
			|| (BMmsg.msg.msgtype == Non_Broadcast_Mode_Non_Data)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_Non_Data)
			)
		{
			pBM->int_status |= BT1553_INT_MODE_CODE;
		}

		if ( (BMmsg.msg.msgtype ==  Broadcast_BC_To_RT)
			|| (BMmsg.msg.msgtype == Broadcast_RT_To_RT)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_Non_Data)
			)
		{
			pBM->int_status |= BT1553_INT_BROADCAST;
		}

		MsgLength++;
		if((MsgLength+curpos)==size)
		{
			pBM=api_mbuf;
			break;
		}
		else
		{
			pBM++;
		}
 		rv = WT_1553B_BM_Get_Message(cardnum,&BMmsg);
	}
	*ret_count=MsgLength;
	
	if((MsgLength+curpos)>size) rst=API_BM_WRAP_AROUND;
	
	return  rst;
}

NOMANGLE BT_INT CCONV BusTools_BM_StartStop(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT startflag)
{
	if(startflag==0)
    {
		if( WT_1553B_BM_Stop(cardnum)==WT_SUCCESS)
		{
			return API_SUCCESS;
		}
		else
		{
			return API_BM_NOTRUNNING;
		} 
	}
	else
    {
		if(WT_1553B_BM_Start(cardnum)==WT_SUCCESS)
		{
			return API_SUCCESS;
		}
		else
		{
			return API_BM_RUNNING;
		};
	}
}

NOMANGLE BT_INT CCONV BusTools_BM_TriggerWrite(
   BT_UINT   cardnum,           // (i) card number (0 based)
   API_BM_TBUF * api_tbuf)  // (i) API trigger definition buffer
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_SetRT_RT_INT(
   BT_UINT   cardnum,       // (i) card number (0 based)
   BT_UINT iflag)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_ReadNextMessage(int cardnum,BT_UINT timeout,BT_INT rt_addr,
									   BT_INT subaddress,BT_INT tr, API_BM_MBUF *pBM_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_ReadLastMessage(int cardnum,BT_INT rt_addr,
									   BT_INT subaddress,BT_INT tr, API_BM_MBUF *pBM_mbuf)
{
   return API_SUCCESS;
}

NOMANGLE BT_INT CCONV BusTools_BM_ReadLastMessageBlock(int cardnum,BT_INT rt_addr_mask,
									   BT_INT subaddr_mask,BT_INT tr, BT_UINT *mcount,API_BM_MBUF *pBM_mbuf)
{
	if ( (rt_addr_mask == -1) && (subaddr_mask == -1) && (tr == -1) )
	{
		return BusTools_BM_MessageReadBlock(cardnum, pBM_mbuf, 1024, 0, mcount);
	}
	
	struct BM_Message BMmsg;
	API_BM_MBUF *pBM;	
	BT_UINT MsgLength = 0;
	BT_INT rst = API_SUCCESS;
	BT_INT curpos = 0;
	
	pBM = &pBM_mbuf[curpos];	
	int rv = WT_1553B_BM_Get_Message(cardnum, &BMmsg);

    while(rv == WT_SUCCESS)
	{
		BT_U32BIT bit = 1;
		BT_U16BIT temp = 0;

		temp = (BMmsg.msg.cmd1 >> 11) & 0x1f;	//rt addr
		if ( !((bit << temp) & rt_addr_mask) )
		{
			rv = WT_1553B_BM_Get_Message(cardnum, &BMmsg);
			continue;
		}

		temp = (BMmsg.msg.cmd1 >> 5) & 0x1f;	//sub addr
		if ( !((bit << temp) & subaddr_mask) )
		{
			rv = WT_1553B_BM_Get_Message(cardnum, &BMmsg);
			continue;
		}

		temp = (BMmsg.msg.cmd1 >> 10) & 1;	//r-t
		if ( (temp != tr) && (tr != -1) )
		{
			rv = WT_1553B_BM_Get_Message(cardnum, &BMmsg);
			continue;
		}

		pBM->command1.rtaddr = (BMmsg.msg.cmd1 >> 11) & 0x1f;	
		pBM->command1.subaddr = (BMmsg.msg.cmd1 >> 5) & 0x1f;
		pBM->command1.tran_rec = (BMmsg.msg.cmd1 >> 10) & 1;
		pBM->command1.wcount = BMmsg.msg.cmd1 & 0x1f;
		pBM->command2.rtaddr = (BMmsg.msg.cmd2 >> 11) & 0x1f;
		pBM->command2.subaddr = (BMmsg.msg.cmd2 >> 5) & 0x1f;
		pBM->command2.tran_rec = (BMmsg.msg.cmd2 >> 10) & 1;
		pBM->command2.wcount = BMmsg.msg.cmd2 & 0x1f;
		pBM->response1.time = 0;//(unsigned char)(2.0 * BMmsg.resp_time1); 
		
		BMmsg.msg.tag = __div64(BMmsg.msg.tag,24);
		
		pBM->time.microseconds = (BT_U32BIT)( (BMmsg.msg.tag) & 0xffffffff );
		pBM->time.topuseconds = (BT_U16BIT)( (BMmsg.msg.tag >> 32) & 0xffff);
        pBM->response2.time = 0;//(unsigned char)(2.0 * BMmsg.resp_time2);
		
		for(int i = 0; i < 32; i++) pBM->value[i] = BMmsg.msg.data[i];
		pBM->messno = BMmsg.msg.msgno ;
        pBM->status1.busy = (BMmsg.msg.status1 >> 3) & 1;   
        pBM->status1.bcr= (BMmsg.msg.status1 >> 4) & 1;   
        pBM->status1.dba = (BMmsg.msg.status1 >> 1) & 1;   
        pBM->status1.me = (BMmsg.msg.status1 >> 10) & 1;   
        pBM->status1.rtaddr = (BMmsg.msg.status1 >> 11) & 0x1f;   
        pBM->status1.res = (BMmsg.msg.status1 >> 5) & 7;   
        pBM->status1.sf  = (BMmsg.msg.status1 >> 2) & 1;   
        pBM->status1.sr = (BMmsg.msg.status1 >> 8) & 1;   
        pBM->status1.tf = BMmsg.msg.status1 & 1;   
        pBM->status1.inst = (BMmsg.msg.status1 >> 9) & 1;

	  pBM->status2.busy =(BMmsg.msg.status2>>3)&1;   
        pBM->status2.bcr=(BMmsg.msg.status2>>4)&1;   
        pBM->status2.dba =(BMmsg.msg.status2>>1)&1;   
        pBM->status2.me =(BMmsg.msg.status2>>10)&1;   
        pBM->status2.rtaddr =(BMmsg.msg.status2>>11)&0x1f;   
        pBM->status2.res =(BMmsg.msg.status2>>5)&7;   
        pBM->status2.sf  =(BMmsg.msg.status2>>2)&1;   
        pBM->status2.sr =(BMmsg.msg.status2>>8)&1;   
        pBM->status2.tf =BMmsg.msg.status2&1;   
        pBM->status2.inst = (BMmsg.msg.status2>>9)&1;

		pBM->int_status = BT1553_INT_END_OF_MESS;
		int BusA_B;
        BusA_B = BMmsg.msg.msgflag & 0X3;
		if(BusA_B == 2)     //busB
        {
			pBM->int_status = pBM->int_status|BT1553_INT_CHANNEL;
        } 
		if(BusA_B == 1)      //busA
        {
			pBM->int_status = pBM->int_status & (~BT1553_INT_CHANNEL);
        } 
		if(BusA_B == 3)
        {
			pBM->int_status = pBM->int_status | BT1553_INT_TWO_BUS;
        } 
		if((BMmsg.msg.msgflag & 0X0200) != 0)
		{
			pBM->int_status = pBM->int_status | BT1553_INT_NO_RESP;
		}
		if ((BMmsg.msg.msgtype == Non_Broadcast_RT_To_RT) || (BMmsg.msg.msgtype == Broadcast_RT_To_RT))
        {
            pBM->int_status = pBM->int_status | BT1553_INT_RT_RT_FORMAT;
		}
		if(((BMmsg.msg.msgflag & 0X0100) != 0) && (BMmsg.msg.msgtype == Non_Broadcast_RT_To_RT))
		{
            pBM->int_status = pBM->int_status | BT1553_INT_NO_RESP;
		}
		if(((BMmsg.msg.msgflag & 0X0100) != 0) && (BMmsg.msg.msgtype == Broadcast_RT_To_RT))
		{
            pBM->int_status = pBM->int_status | BT1553_INT_NO_RESP;
		}
		if ( (BMmsg.msg.msgtype ==  Non_Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Non_Broadcast_Mode_With_Data_Send)
			|| (BMmsg.msg.msgtype == Non_Broadcast_Mode_Non_Data)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_Non_Data)
			)
		{
			pBM->int_status |= BT1553_INT_MODE_CODE;
		}

		if ( (BMmsg.msg.msgtype ==  Broadcast_BC_To_RT)
			|| (BMmsg.msg.msgtype == Broadcast_RT_To_RT)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_With_Data_Recv)
			|| (BMmsg.msg.msgtype == Broadcast_Mode_Non_Data)
			)
		{
			pBM->int_status |= BT1553_INT_BROADCAST;
		}

		MsgLength++;
		if((MsgLength + curpos) == 1024)
		{
			pBM = pBM_mbuf;
			*mcount=MsgLength;
			return WT_SUCCESS;
		}
		else
		{
			pBM++;
		}
		rv = WT_1553B_BM_Get_Message(cardnum,&BMmsg);
	}
	*mcount = MsgLength;
	
	if( (MsgLength+curpos) > 1024 ) rst = API_BM_WRAP_AROUND;
	
	return  rst;
}

NOMANGLE BT_INT CCONV BusTools_BM_Checksum1760(API_BM_MBUF mbuf, BT_U16BIT *cksum)
{
	return API_SUCCESS;
}




