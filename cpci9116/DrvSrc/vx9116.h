/*****************************************************************************************/
/* RTES, Co.,Ltd.                                                                        */
/*                                                                                       */
/* (c) Copyright 2014, RTES Co.,Ltd.                                                     */
/*                                                                                       */
/* All rights reserved. RTES¡¯s source code is an unpublished work and the                */
/* use of a copyright notice does not imply otherwise. This source code contains         */
/* confidential, trade secret material of RTES,Co.,Ltd. Any attempt or participation     */
/* in deciphering, decoding, reverse engineering or in any way altering the source       */
/* code is strictly prohibited, unless the prior written consent of RTES,Co.,Ltd.        */
/* is obtained.                                                                          */
/*                                                                                       */
/* Filename : VxcPCI9116.h                                                               */
/* Version:   1.0                                                                        */
/* Programmer(s):  applerain                                                             */
/* Created : 2012.11.01                                                                  */
/* Description : A driver files for cPCI 9116 BOARD                                      */
/* Modification History:     															 */
/*	2013-5-1  LJ rename macro PPC to   PPC604                                            */
/*****************************************************************************************/
#ifndef VX_CPCI9116_H
#define VX_CPCI9116_H

#define VX_9116_IO_FETCH (1)
#define VX_9116_IO_INT (0)
#define VX_9116_IO_MODE VX_9116_IO_FETCH

#include "../h/types/vxTypesOld.h"
/*#define IS_PPC*/

/*define the IO address of each register with respect to the base address*/
#define CPCI9116_SICOUNTER_OFFSET     0x00
#define CPCI9116_SI2COUNTER_OFFSET    0x04
#define CPCI9116_SCCOUNTER_OFFSET     0x08
#define CPCI9116_DIVCOUNTER_OFFSET    0x0c
#define CPCI9116_DLYCOUNTER_OFFSET    0x10
#define CPCI9116_MCOUNTER_OFFSET      0x14
#define CPCI9116_GPCT0_OFFSET         0x18
#define CPCI9116_GPCTCONTROL_OFFSET   0x20
#define CPCI9116_ADDATA_OFFSET        0x24
#define CPCI9116_CHANNELCONFIG_OFFSET 0x24
#define CPCI9116_ADFIFOSTATUS_OFFSET  0x28
#define CPCI9116_ADFIFOCONTROL_OFFSET 0x28
#define CPCI9116_DI_OFFSET            0x30
#define CPCI9116_DO_OFFSET            0x30
#define CPCI9116_ADTRIGGERMODE_OFFSET 0x34
#define CPCI9116_INTREASON_OFFSET     0x38
#define CPCI9116_INTCONTROL_OFFSET    0x38
#define	CPCI9116_INTGENERATION_OFFSET 0x2C

#define AMCC5935_INTCONTROL_OFFSET    0x38

#define CPCI9116_BOARD_MAX            8
#define CPCI9116_CHANNEL_MAX          64

#define CPCI9116_VENDERID             0x144a
#define CPCi9116_DEVID                0x9116

#define BUFFSIZE                      1024

#define FPGA_CONFIG    	   0x0100
#define FPGA_CLR_RST   	   0x0200
#define FPGA_STATUS        0x0001
#define FPGA_CONFIG_DONE   0x0002
#define FPGA_INIT_DONE     0x0004
#define FPGA_DATA_BUSY     0x0008

typedef struct
{
	#if (CPU==PPC85XX)
	unsigned long S_P2A_FIFO_TOGGLE:1;
	unsigned long S_A2P_FIFO_TOGGLE:1;
	unsigned long S_A2P_FIFO_ADVANCE_BYTE:2;
	unsigned long S_P2A_FIFO_ADVANCE_BYTE:2;
	unsigned long S_ENDIAN_CON:2;
	unsigned long S_INT_ASSERT:1;
	unsigned long Dummy3:1;
	unsigned long S_TARGET_ABORT_INT:1;
	unsigned long S_MASTER_ABORT_INT:1;
	unsigned long S_MRTC_INT:1;
	unsigned long S_MWTC_INT:1;
	unsigned long S_IMB_INT:1;
	unsigned long S_OMB_INT:1;
	unsigned long C_MRTC_IE:1;
	unsigned long C_MWTC_IE:1;
	unsigned long Dummy2:1;
	unsigned long C_IMB_IE:1;
	unsigned long C_IMB_MBX:2;
	unsigned long C_IMB_BYTE:2;
	unsigned long Dummy1:3;
	unsigned long C_OMB_IE:1;
	unsigned long C_OMB_MBX:2;
	unsigned long C_OMB_BYTE:2;

	#else

	unsigned long C_OMB_BYTE:2;
	unsigned long C_OMB_MBX:2;
	unsigned long C_OMB_IE:1;
	unsigned long Dummy1:3;
	unsigned long C_IMB_BYTE:2;
	unsigned long C_IMB_MBX:2;
	unsigned long C_IMB_IE:1;
	unsigned long Dummy2:1;
	unsigned long C_MWTC_IE:1;
	unsigned long C_MRTC_IE:1;
	unsigned long S_OMB_INT:1;
	unsigned long S_IMB_INT:1;
	unsigned long S_MWTC_INT:1;
	unsigned long S_MRTC_INT:1;
	unsigned long S_MASTER_ABORT_INT:1;
	unsigned long S_TARGET_ABORT_INT:1;
	unsigned long Dummy3:1;
	unsigned long S_INT_ASSERT:1;
	unsigned long S_ENDIAN_CON:2;
	unsigned long S_P2A_FIFO_ADVANCE_BYTE:2;
	unsigned long S_A2P_FIFO_ADVANCE_BYTE:2;
	unsigned long S_A2P_FIFO_TOGGLE:1;
	unsigned long S_P2A_FIFO_TOGGLE:1;
	#endif

} S593X_INTCSR_ST;

typedef struct
{
	float dResolution;
	float dValue;
	unsigned char uchChanNo;
}CPCI9116_ADRANGE_ST;

typedef struct
{
	UINT32 ulCtrlAddr;
    UINT32 ulBaseAddr;
	UINT32 ulFPGAAddr;
    UINT8  uchIrqNo;
	UINT8  uchInit;
}CPCI_INFO_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved:18;
	UINT32 AdGain:2;
	UINT32 AdChan:4;
	UINT32 EnableMultiplexer:4;
	UINT32 HL_Sel:1;
	UINT32 InputSignalPolar:1;
	UINT32 InputSignalEnding:1;
	UINT32 U_CMMD:1;

	#else

	UINT32 U_CMMD:1;
	UINT32 InputSignalEnding:1;
	UINT32 InputSignalPolar:1;
	UINT32 HL_Sel:1;
	UINT32 EnableMultiplexer:4;
	UINT32 AdChan:4;
	UINT32 AdGain:2;
	UINT32 reserved:18;
	#endif

}CPCI9116_AD_CONFIG_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved:23;
	UINT32 EnableDMA:1;
	UINT32 DisableScanCounter:1;
	UINT32 ClearConfigFifo:1;
	UINT32 ConfigFifoSetDone:1;
	UINT32 ClearDataFifo:1;
	UINT32 ClearTriggerStatus:1;
	UINT32 ClearScanCounter:1;
	UINT32 ClearAdOverRun:1;
	UINT32 ClearAdOverSpeed:1;

	#else

	UINT32 ClearAdOverSpeed:1;
	UINT32 ClearAdOverRun:1;
	UINT32 ClearScanCounter:1;
	UINT32 ClearTriggerStatus:1;
	UINT32 ClearDataFifo:1;
	UINT32 ConfigFifoSetDone:1;
	UINT32 ClearConfigFifo:1;
	UINT32 DisableScanCounter:1;
	UINT32 EnableDMA:1;
	UINT32 reserved:23;

	#endif

} CPCI9116_AD_CTRL_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved:19;
	UINT32 AdType:2;
	UINT32 SoftConv:1;
	UINT32 EnableACQ:1;
	UINT32 EnableMCounter:1;
	UINT32 EnableReTrig:1;
	UINT32 DLYTrigSrc:1;
	UINT32 ExternalTimeBase:1;
	UINT32 NegativeExternalTrigger:1;
	UINT32 TriggerMode:3;
	UINT32 TriggerSrc:1;

	#else

	UINT32 TriggerSrc:1;
	UINT32 TriggerMode:3;
	UINT32 NegativeExternalTrigger:1;
	UINT32 ExternalTimeBase:1;
	UINT32 DLYTrigSrc:1;
	UINT32 EnableReTrig:1;
	UINT32 EnableMCounter:1;
	UINT32 EnableACQ:1;
	UINT32 SoftConv:1;
	UINT32 AdType:2;
	UINT32 reserved:19;
	#endif

} CPCI9116_AD_TRIG_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved:24;
	UINT32 ACQEnable:1;
	UINT32 FifoOverflow:1;
	UINT32 FifoHalfFull:1;
	UINT32 FifoEmpty:1;
	UINT32 DigialTriggerHappen:1;
	UINT32 ScanCountComplete:1;
	UINT32 AdOverRun:1;
	UINT32 AdOverSpeed:1;

	#else

	UINT32 AdOverSpeed:1;
	UINT32 AdOverRun:1;
	UINT32 ScanCountComplete:1;
	UINT32 DigialTriggerHappen:1;
	UINT32 FifoEmpty:1;
	UINT32 FifoHalfFull:1;
	UINT32 FifoOverflow:1;
	UINT32 ACQEnable:1;
	UINT32 reserved:24;
	#endif

} CPCi9116_AD_STATUS_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved2:19;
	UINT32 Timer:1;
	UINT32 ScanCounterTC:1;
	UINT32 FifoHalfFull:1;
	UINT32 DigitalTrigger:1;
	UINT32 EnableEOCInt:1;
	UINT32 reserved:3;
	UINT32 ClearCPTCInt:1;
	UINT32 ClearScanCounterTCInt:1;
	UINT32 ClearFifoHalfFullInt:1;
	UINT32 ClearDigitalTrg:1;
	UINT32 ClearEOCInt:1;

	#else

	UINT32 ClearEOCInt:1;
	UINT32 ClearDigitalTrg:1;
	UINT32 ClearFifoHalfFullInt:1;
	UINT32 ClearScanCounterTCInt:1;
	UINT32 ClearCPTCInt:1;
	UINT32 reserved:3;
	UINT32 EnableEOCInt:1;
	UINT32 DigitalTrigger:1;
	UINT32 FifoHalfFull:1;
	UINT32 ScanCounterTC:1;
	UINT32 Timer:1;
	UINT32 reserved2:19;
	#endif

} CPCI9116_INT_CTRL_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved2:24;
	UINT32 EnableCounter:1;
	UINT32 UpCounter:1;
	UINT32 ExternalUpDownCtrl:1;
	UINT32 ExternalGateSrc:1;
	UINT32 ExternalClockSrc:1;
	UINT32 reserved:1;
	UINT32 CounterMode:2;

	#else

	UINT32 CounterMode:2;
	UINT32 reserved:1;
	UINT32 ExternalClockSrc:1;
	UINT32 ExternalGateSrc:1;
	UINT32 ExternalUpDownCtrl:1;
	UINT32 UpCounter:1;
	UINT32 EnableCounter:1;
	UINT32 reserved2:24;
	#endif

} CPCI9116_GP_CTRL_ST;

typedef struct
{
	#if (CPU==PPC85XX)
	UINT32 reserved:27;
	UINT32 TimerInt:1;
	UINT32 SCTCInt:1;
	UINT32 FifoHalfFullInt:1;
	UINT32 DigialTriggerInt:1;
	UINT32 EOCInt:1;

	#else

	UINT32 EOCInt:1;
	UINT32 DigialTriggerInt:1;
	UINT32 FifoHalfFullInt:1;
	UINT32 SCTCInt:1;
	UINT32 TimerInt:1;
	UINT32 reserved:27;
	#endif

} CPCI9116_INT_STATUS_ST;

typedef union
{
	unsigned long ulTemp;
	unsigned short ushTemp[2];
	unsigned char uchTemp[4];
	CPCI9116_GP_CTRL_ST stGpCtrl;
	CPCI9116_AD_CTRL_ST stAdCtrl;
	CPCi9116_AD_STATUS_ST stAdStatus;
	CPCI9116_AD_CONFIG_ST stAdConfig;
	CPCI9116_AD_TRIG_ST stAdTrig;
	CPCI9116_INT_CTRL_ST stIntCtrl;
	CPCI9116_INT_STATUS_ST stIntStatus;
}CPCI9116_REG_UNION;

typedef union
{
	unsigned long ulTemp;
	S593X_INTCSR_ST stIntcsr;
} OP_REG_ST;


int cPCI9116_Init(unsigned char uchBoardNo);
int cPCI9116_DI_Read(unsigned char uchBoardNo, unsigned char *puchData);
int cPCI9116_DI_Read_Chan(unsigned char uchBoardNo, unsigned char uchChannel, unsigned char *puchData);
int cPCI9116_DO_Write(unsigned char uchBoardNo, unsigned char uchData);
int cPCI9116_DO_Write_Chan(unsigned char uchBoardNo, unsigned char uchChannel, unsigned char uchData);
int cPCI9116_CFIFO_Set(unsigned char uchBoardNo, unsigned char uchChannel, unsigned char uchGain, char szDiff, char szUnipolar);
int cPCI9116_CFIFO_SetDone(unsigned char uchBoardNo);
int cPCI9116_CFIFO_Clear(unsigned char uchBoardNo);
int cPCI9116_ResetAD(unsigned char uchBoardNo);
int cPCI9116_Single_AD_Read(unsigned char uchBoardNo, float *pfInput);
void cPCI9116_TestSingleAD(unsigned char uchBoardNo);
int cPCI9116_Int_Config(unsigned char uchBoardNo, unsigned long ulScanInterval, unsigned long ulSampleInterval, unsigned long ulScanCount);
int cPCI9116_Start_AD(unsigned char uchBoardNo);
int cPCI9116_Stop_AD(unsigned char uchBoardNo);
int cPCI9116_GetData(unsigned char uchBoardNo, float **pdData);
int cPCI9116_GetData_Chan(unsigned char uchBoardNo, unsigned char uchChannel, float **pdData);
unsigned short Swap_Short(unsigned short ushData);
unsigned long Swap_Long(unsigned long ulData);
STATUS FPGA_Download(unsigned char uchBoardNo);
void Reset5935(unsigned char uchBoardNo);

int  cPCI9116_Int_Handle(unsigned char uchBoardNo);
int cPCI9116_Is_Init(unsigned char uchBoardNo);

#if VX_9116_IO_MODE == VX_9116_IO_FETCH
int cPCI9116_FetchData(unsigned char uchBoardNo);
#endif

#endif
