#include "../DrvSrc/vx9116.h"
#include <stdio.h>
#include <taskLib.h>

void cPCI9116Test(unsigned char uchBoardNo)
{
    int i = 0;
    float *pfData = NULL;

    cPCI9116_Init(0);
    cPCI9116_ResetAD(uchBoardNo);
    cPCI9116_CFIFO_Clear(uchBoardNo);
    cPCI9116_CFIFO_Set(uchBoardNo, 0, 0, 0, 1);
    cPCI9116_CFIFO_Set(uchBoardNo, 1, 0, 0, 1);
    cPCI9116_CFIFO_Set(uchBoardNo, 2, 0, 0, 1);

    cPCI9116_CFIFO_SetDone(uchBoardNo);
    cPCI9116_Int_Config(uchBoardNo, 1000, 100, 3);
#if 1
    while(1)
    {
		cPCI9116_Start_AD(uchBoardNo);
		taskDelay(3);
		cPCI9116_Stop_AD(uchBoardNo);
		cPCI9116_FetchData(uchBoardNo);
		cPCI9116_GetData(uchBoardNo, &pfData);
		for (i = 0; i < 9; i++)
		{
			printf("data[%d] = %f ", i, pfData[i]);
			if((i+1)%3 == 0)
			{
				printf("\n");
			}
		}
    }
#endif
}
